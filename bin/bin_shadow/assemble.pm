#!/usr/bin/perl
#
    sub logo {print"
   VHDL-ROM TO MACHINE-CODE CONVERTER 
   DLXJ Processor Experiment within the CT-PRAKTIKUM 
   Mar 2010 A.Reinsch / LS-RA
   University of Jena / Dept. of Computer Science \n\n";}
#
#  Ausgaben:
#
sub writing {print ">>> Written to file : \n   $hex_dir/bytecode.hex\n";}
sub success {print ">>> $code_lines Byte Machine-code successfully generated\n";}
sub failed {print ">>> Machine-code not generated\n";}
sub empty_vhd {print ">>> Empty assembler output file\n";}
sub empty_hex {print ">>> Empty Machine-code file\n";}
############################################################
# 
# extrahiert den Hexcode aus einer mit dem Assembler
# DLXASM generierten VHDL-Beschreibung:
#         rom-behaviour_[name].vhd
#
# Diese  Beschreibung ist das VHDL-Modell fuer ein
# ROM-Bauelement
# 
############################################################
# 
# Zielverzeichnis
# 
    $hex_dir="$ENV{DLXJ}/tmp";
    $bin_dir="$ENV{DLXJ}/bin/bin_shadow";
    # 
$vhdlfile="rom-behaviour_actual.vhd";
$codefile="bytecode.hex";
# 
# Loeschen der Ein- und Ausgabedateien, falls sie existieren
#
#stat("$hex_dir");
#system ("rm -r $hex_dir") if -e _;
#system ("mkdir $hex_dir");
stat("$hex_dir/$vhdlfile");
system ("rm $hex_dir/$vhdlfile") if -e _;
stat("$hex_dir/$codefile");
system ("rm $hex_dir/$codefile") if -e _;
#
# Assembleraufruf
#
system ("$bin_dir/dlx_asm -s '$ARGV[0]' actual");
#
# Ausgabe des Titels
#
logo;
#
# Assemblerausgabe (rom-behaviour_actual.vhd) nach tmp bewegen
# falls sie existiert
#
stat($vhdlfile);
system ( "mv `pwd`/$vhdlfile $hex_dir") if -e _;
#
# Oeffnen der VHDL Datei
#
open(VHDL,"$hex_dir/$vhdlfile") 
    or die ">>> Cannot open assembler output file. $!\n";
#
#  
$vhdl_lines = 0;
while (sysread VHDL, $buffer, 4096) 
{
    $vhdl_lines += ($buffer =~ tr/\n//);
}
if ($vhdl_lines == 0) {empty_vhd,failed} else
{
    close(VHDL);
    open(CODE,"> $hex_dir/bytecode.hex") 
	or die "Cannot open machine-code file. $!\n";

#
############################################################
#
# wiederholtes (Option g) Durchsuchen der als erster Parameter
# angegebenen Datei (rom-behaviour_[name].vhd)
# nach dem Muster:
#       X"(..)(..)(..)(..)"
# durch Klammerung wird erreicht, dass eine Aufspaltung des
# Suchergebnisses in 4 Substrings ($1 ... $4) erfolgt, die den
# 4 Byte des Befehlswortes entsprechen.
#
open(VHDL,"$hex_dir/$vhdlfile") 
    or die ">>> Cannot open assembler output file. $!\n";

    while(defined($i = <VHDL>))  
    {
 	while($i =~ /X"(..)(..)(..)(..)"/g)
	{
	    print( CODE "$1\n$2\n$3\n$4\n")
	    }
    } 
    close(CODE); 
###########################################################
    open(CODE,"$hex_dir/bytecode.hex") 
	or die "Cannot open machine-code file. $!\n";
    $code_lines = 0;
    while (sysread CODE, $buffer, 4096) 
    {
	$code_lines += ($buffer =~ tr/\n//);		    
    }
    if ($code_lines == 0) {empty_hex,failed}
###########################################################
#
# Schliessen der Dateien   
#
#
    close(CODE) 
	or die "Cannot close machine-code file. $! \n";
    success;
    writing;
}
close(VHDL);
#
###########################################################





