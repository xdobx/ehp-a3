#!/bin/sh
#
#   Simulation Start Script
#   DLXJ Processor Experiment within the CT-PRAKTIKUM 
#   Jul 12 2004 A.Reinsch / LS-RA 
#   University of Jena / Dept. of Computer Science 
#
pwddir=`pwd`
#
cd $DLXJ/sim
# 
#if test -f VirSim.log ; then
#echo
#echo ">> Simulation executable is up to date"
#else
#echo
#echo ">> Compiling DLXJ design ..."
#vcs -lca Y-2006.06-SP1 -nc -debug_all WORK.tb
#vcs -ucli -full64 -nc -q work.tb
vcs -ucli -debug_all -nc -q -full64 work.tb
#fi
######################################
#
#xrdb -quiet -merge ./xsettings
cp redi_template.txt redi.txt 
HOST_NAME=`hostname`
#
#if test "$HOST_NAME" = "isun02" ; then
if test "$HOST_NAME" = "localhost.localdomain" ; then
xterm -T "DLXJ Instructions" -bg grey -geometry 41x55+1425+0 -e tail -f redi.txt &
else
xterm -T "DLXJ Instructions" -bg grey -geometry 41x16+0+800 -e 'tail -q -f redi.txt' &
fi
#xterm -e 
#./simv -nc -debug_all -i init.in &
# echo ">> Starting Simulator ..."
#cp ./ini/sc.cfg .
#./functional -debug_all -include init.in 
#dve -session DVEfiles/session.tcl &
# dve -toolexe ./scsim -toolargs include init.in -toolargs -debug_all
#scirocco +sim+./scsim #-nc -i init.in #+cfgfile+sc.cfg 
#
# 
# sn07:
#scs -nc -verb -debug_all -executable alu_sim WORK.tb_adder
#vcs -nc -verb -debug_all -executable alu_sim WORK.tb_adder
#scs -nc -debug_all -executable alu_sim WORK.tb_adder
#
# GUI for interactive simulation session 
#
echo ">> Starting Simulator and GUI ..."
#
# interactiv Simulation Scirocco:
# -------------------------------
# sn03_06:
# scirocco +sim+./test_config -nc -i ./adder.ini +cfgfile+sc.cfg 
#
# interactiv Simulation DVE:
# -------------------------
# DVE-Preferenzfile und DVE-Verzeichnis leschen (stoert nur)
cp $DLXJ/tcl/\.synopsys_dve_prefs.tcl $HOME
if test -f $DLXJ/tcl/\.synopsys_dve_ini.tcl; then
rm $DLXJ/tcl/\.synopsys_dve_ini.tcl 
fi
if test -d $DLXJ/sim/DVEfiles; then
rm -r $DLXJ/sim/DVEfiles
fi
#dve -session $DLXJ/tcl/dve_session.tcl \
#-toolexe $DLXJ/sim/simv \
#-toolargs "-debug_all -ucli -include init.ini"
./simv -i $DLXJ/sim/init.ini -gui
#dve -full64 -toolexe ./simv -toolargs "-ucli -i $DLXJ/sim/init.ini -full64" 
if test "$HOST_NAME" = "localhost.localdomain" ; then
pkill -9 -f -x 'tail -f redi.txt'
else
pkill -9 -f -x 'tail -q -f redi.txt'
fi









