#!/bin/sh
#
pwddir=`pwd`
#
# ------------------------
xnffile='vsdlx'
ucffile='../ucf/xc3s700an'
imp_dir=$DLXJ/imp
tsim_dir=$DLXJ/sim/tsim
vhdfile='vsdlx.vhd'
sdffile='vsdlx.sdf'
vhd_tsimfile='vsdlx_time.vhd'
sdf_tsimfile='vsdlx_time.sdf'
target_part='xc3s700anfgg484-4'
# ----------------------
cd $DLXJ
#
if test -d $DLXJ/imp;  then
rm -r $DLXJ/imp;
mkdir $DLXJ/imp;
fi

cp $DLXJ/syn/expdir/$xnffile.edf $imp_dir
cd $imp_dir
#
edif2ngd -l synopsys $xnffile.edf
# ------------------------
ngdbuild -quiet -p $target_part -uc $ucffile $xnffile.ngo
# ------------------------
#map -cm speed -quiet -detail -o map.ncd $xnffile.ngd $xnffile.pcf
map -cm speed -detail -o map.ncd $xnffile.ngd $xnffile.pcf
# ------------------------
#par -w -ol 5 -xe 2 map.ncd $xnffile.ncd $xnffile.pcf # max. Frequenz
par -w map.ncd $xnffile.ncd $xnffile.pcf # min. Rechenzeit
# ------------------------
#trce -v 10 -u $xnffile.ncd $xnffile.pcf -skew -o $xnffile.twr 
# ------------------------
#ngdanno -report $xnffile.ncd map.ngm
# ------------------------
#ngd2vhdl -log $xnffile.log -r -verbose -w $xnffile.nga
# Achtung: cp weiter unten nicht vergessen !
# ------------------------
bitgen -w $xnffile.ncd 
# mit TIE: -t -n
cp $xnffile.bit /tmp
# ------------------------
cd ..
#
unset xnffile
unset ucffile
unset ise_dir
unset tsim_dir
unset vhdfile
unset sdffile
unset vhd_tsimfile
unset sdf_tsimfile
#




