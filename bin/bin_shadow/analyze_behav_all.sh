#!/bin/sh
#
pwddir=`pwd`
#
# Funktionen bei 64-Bit-System:
todo_an () 
{
    echo "       $1"; 
vhdlan -full64 -w DLX -event -no_opt -nc ../v_todo/$1
} 
##
dlx_an () 
{
    echo "       $1"; 
vhdlan -full64 -w DLX -event -no_opt -nc ../vsrc/dlx/$1 
}
##
ieee_an () 
{
    echo "       $1"; 
vhdlan -full64 -w IEEE_EXTD -event -no_opt -nc ../vsrc/ieee_extd/$1  
}
## Funktionen bei 32-Bit-System:
#todo_an () 
#{
#    echo "       $1"; 
#vhdlan -w DLX -event -no_opt -nc ../v_todo/$1
#} 
##
#dlx_an () 
#{
#    echo "       $1"; 
#vhdlan -w DLX -event -no_opt -nc ../vsrc/dlx/$1 
#}
##
#ieee_an () 
#{
#    echo "       $1"; 
#vhdlan -w IEEE_EXTD -event -no_opt -nc ../vsrc/ieee_extd/$1  
#}
##
cd $DLXJ/sim
if test -f ./VirSim.log;  then
rm ./VirSim.log
fi
#
#
# create simulation libraries:
# 
if test -d ./bsim;  then
rm -R ./bsim;
fi
mkdir ./bsim
mkdir ./bsim/blib
mkdir ./bsim/ieee_extd
echo ">> Analyzing all VHDL files ... "
#
# analyze all necessary files:
#
#    Library IEEE_EXTD
#    =================
#
ieee_an stdl1164_vector_arithmetic.vhd 
ieee_an stdl1164_vector_arithmetic-body.vhd
#
#    General
#    =======
dlx_an dlx_types.vhd  
dlx_an dlx_instructions.vhd  
dlx_an lcd.vhd
#
#   Controller
#   ==========
#
dlx_an control_types.vhd   
#
dlx_an ir_decode_1.vhd  
todo_an ir_decode_1-behaviour.vhd  
dlx_an ir_decode_2.vhd  
dlx_an ir_decode_2-behaviour.vhd  
dlx_an ir_decode_3.vhd  
todo_an ir_decode_3-behaviour.vhd  
#
dlx_an fsm.vhd  
dlx_an fsm_switch.vhd 
dlx_an fsm_switch-dataflow.vhd 
dlx_an fsm_next.vhd 
todo_an fsm_next-dataflow.vhd 
dlx_an fsm_output.vhd 
todo_an fsm_output-dataflow.vhd 
dlx_an fsm-structural.vhd 
#
dlx_an controller.vhd  
dlx_an controller-dataflow.vhd  
#
#   Datapath
#   ======== 
#
#       Latches L1 and L2 
#       -----------------
dlx_an word_latch_1.vhd 
dlx_an word_latch_1-dataflow.vhd 
#
#       Latches C (and  Display)
#       ------------------------
dlx_an word_latch_e_1.vhd 
dlx_an word_latch_e_1-dataflow.vhd 
#
#       Latches A and B 
#       ---------------
dlx_an word_latch_e_1e.vhd 
dlx_an word_latch_e_1e-dataflow.vhd 
#
#       Latches PC, MAR, MDR
#       --------------------
dlx_an word_latch_e_1e1.vhd 
dlx_an word_latch_e_1e1-dataflow.vhd 
#
#       $pre2; $pre1 $dlxMultiplexer AMUX, DMUX
#       ----------------------
dlx_an word_mux2.vhd 
dlx_an word_mux2-dataflow.vhd 
#
#       Latch IR
#       --------
dlx_an ir_e_2e1.vhd 
dlx_an ir_e_2e1-behaviour.vhd 
#
#
#       ALU (adder with fast carry)
#       --- 
dlx_an alu_core.vhd 
dlx_an alu_core-behaviour.vhd 
dlx_an alu.vhd 
dlx_an alu-structural.vhd 
#
#       Bus Constants
#       -------------
dlx_an bus_const32.vhd 
dlx_an bus_const32-dataflow.vhd 
#
#       Register File 
#       -------------
dlx_an reg_fileram.vhd 
dlx_an reg_fileram-structural.vhd 
#
#       Datapath
#       --------
dlx_an datapath.vhd 
dlx_an datapath-structural.vhd 
#
#
#    DLX Processor System
#    ====================
#  Components: Core, Memory Unit, Controller Interface, Display Interface
#
#      Core
#      ----
dlx_an core.vhd 
dlx_an core-structural.vhd 
# 
#      Memory Unit
#      -----------
dlx_an mem.vhd 
dlx_an mem-structural.vhd 
dlx_an decoder.vhd 
dlx_an decoder-dataflow.vhd 
dlx_an memory_unit.vhd 
dlx_an memory_unit-structural.vhd 
#
#      Controller Interface
#      --------------------
dlx_an byte_dff.vhd 
dlx_an byte_dff-dataflow.vhd 
dlx_an qclk2phi.vhd 
dlx_an qclk2phi-dataflow.vhd 
dlx_an uc_if.vhd 
dlx_an uc_if-structural.vhd 
#
#      Display Interface
#      -----------------
dlx_an disp_mux.vhd 
dlx_an disp_mux-behaviour.vhd 
# Virtex E:
#dlx_an display_if.vhd 
#dlx_an display_if-structural.vhd 
# Spartan-3AN
dlx_an lcd_if.vhd 
dlx_an lcd_if-dataflow.vhd 
#
#      Complete DLX System Chip
#      ------------------------
dlx_an vsdlx.vhd 
dlx_an vsdlx-structural.vhd 
#
#    Testbench
#    =========
dlx_an testbench.vhd 
dlx_an testbench-structural.vhd 
#
cd $pwddir
#
#unset dlx
#unset ieee
#unset pre1
#unset pre2
#unset ieee_options
#unset dlx_options
unset pwddir
#
