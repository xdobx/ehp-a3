;-----------------------------------------------------------------;
; Datei        : testAssemblerProgramm.asm                        ;
; Beschreibung : soll neue funktionen testeten                    ;
; Programmlauf : -1 3 2 2 3 7 7 6 12 1 6                          ;
;-----------------------------------------------------------------;

	org X"00000000" ; Programmstartadresse
	start init      ; markiert den ersten Befehl

; Immediate-Werte
	
	zero equ X"0000" ; Offset 0 
	disp equ X"0FFC" ; Offset 0x0FFC, 0x0000_0FFC Adresse Displaylatch
	ram  equ X"0FF8" ; Offset 0x0FF8, 0x0000_0FF8 Basisadresse RAM

init:
	lw.i r31, disp(r0)     ; r31 <- M[0x0000_0FFC] 
	sw.i zero(r31), r31    ; Display <- 0x2000_0000
	sub.i r1, r0, 1        ; r1 = -1 ,0xFFFFFFFF
	sw.i zero(r31), r1     ; display <- r1
	add.i r2, r1, 4        ; r2 = 3
	sw.i zero(r31), r2     ; display <- r2
	add r3, r2, r1         ; r3 = 2
	sw.i zero(r31), r3     ; display <- r3
	and.i r4, r2, 6        ; r4 = 2
	sw.i zero(r31), r4     ; Display <- r4
	and r4, r1, r2         ; r4 = 3
	sw.i zero(r31), r4     ; Display <- r4
        or.i r4, r4, 5         ; r4 = 7
	sw.i zero(r31), r4     ; display <- r4
        or r5, r3, r4          ; r5 = 7
	sw.i zero(r31), r5     ; display <- r5
	sll.i r4, r2, 1        ; r4 = 6
	sw.i zero(r31), r4     ; display <- r4
	sll r1, r2, r3         ; r1 = 12
	sw.i zero(r31), r1     ; display <- r1
	srl.i r2, r2, 1        ; r2 = 1
	sw.i zero(r31), r1     ; display <- r1
	srl r1, r1, r2         ; r1 = 6
	sw.i zero(r31), r1     ; display <- r1
	end

; TODO Implement these Operations
;	add Rd, Rs1, Rs2 // Rd <- Rs1 + Rs2
;	sub
;	and.i
;	or.i
;	sll.i
;	srl.i

