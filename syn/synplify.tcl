
# Usage: synbatch [<projectFile>] [-shell] [-log] [-tcl] [-tclcmd] 
#                 [-history] [-compile] [-enable64bit] [-impl] [-run] 
#                 [-runall] [-verbose_log] [-evalhostid] [-version] 
#                 [-licensetype] [-batch] [-identify_dir] [-help]
#
# <projectFile> ... Project to load.
# -shell .......... .
# -log ............ Specify standard out/error log file. default: stdout.log.
# -tcl ............ TCL File to run upon startup.
# -tclcmd ......... TCL Command to run upon startup.
# -history ........ Records all TCL commands and writes them to <historylog> upon exit.
# -compile ........ Run compile only.
# -enable64bit .... Run in 64 bit mode if available.
# -impl ........... Run specified implementation.
# -run ............ .
# -runall ......... Run all implementations.
# -verbose_log .... Write stdout.log in verbose mode..
# -evalhostid ..... Print evaluation HostID.
# -version ........ Print version.
# -licensetype .... license feature name to use for session.
# -batch .......... Batch mode.
# -identify_dir ... Installation directory for the Identify product.
# -help ........... Print command option information.
#
# Shell-Kommandos:
# ---------------
# add_file
# c_diff
# c_free
# c_info
# c_intersect
# c_list
# c_member
# c_print
# c_symdiff
# c_union
# define_coll
# define_collection
# expand
# find
# get_option
# impl
# partdata
# project
# set_option




#project files

#implementation: "rev_1"
#impl -add rev_1 -type fpga
#
# add_vhd.fst
#
# This script read in all the source files
# and is called from dlx.fst:
#                source add_vhd.fst
#
#    Library IEEE_EXTD
#    =================
#
add_file -lib ieee_extd -vhdl ../vsrc/ieee_extd/stdl1164_vector_arithmetic.vhd
add_file -lib ieee_extd -vhdl ../vsrc/ieee_extd/stdl1164_vector_arithmetic-body.vhd
#
#    General
#    =======
add_file -vhdl ../vsrc/dlx/dlx_types.vhd 
add_file -vhdl ../vsrc/dlx/dlx_instructions.vhd 
#
#   Controller
#   ==========
#
add_file -vhdl ../vsrc/dlx/control_types.vhd  
#
add_file -vhdl ../vsrc/dlx/ir_decode_1.vhd 
add_file -vhdl ../v_todo/ir_decode_1-behaviour.vhd 
add_file -vhdl ../vsrc/dlx/ir_decode_2.vhd 
add_file -vhdl ../vsrc/dlx/ir_decode_2-behaviour.vhd 
add_file -vhdl ../vsrc/dlx/ir_decode_3.vhd 
add_file -vhdl ../v_todo/ir_decode_3-behaviour.vhd 
#
add_file -vhdl ../vsrc/dlx/fsm.vhd 
add_file -vhdl ../vsrc/dlx/fsm_switch.vhd 
add_file -vhdl ../vsrc/dlx/fsm_switch-dataflow.vhd 
add_file -vhdl ../vsrc/dlx/fsm_next.vhd 
add_file -vhdl ../v_todo/fsm_next-dataflow.vhd 
add_file -vhdl ../vsrc/dlx/fsm_output.vhd 
add_file -vhdl ../v_todo/fsm_output-dataflow.vhd 
add_file -vhdl ../vsrc/dlx/fsm-structural.vhd 
#
add_file -vhdl ../vsrc/dlx/controller.vhd 
add_file -vhdl ../vsrc/dlx/controller-dataflow.vhd 
#
#   Datapath
#   ======== 
#
#       Latches L1 and L2 
#       -----------------
add_file -vhdl ../vsrc/dlx/word_latch_1.vhd 
add_file -vhdl ../vsrc/dlx/word_latch_1-dataflow.vhd 
#
#       Latches C (and  Display)
#       ------------------------
add_file -vhdl ../vsrc/dlx/word_latch_e_1.vhd 
add_file -vhdl ../vsrc/dlx/word_latch_e_1-dataflow.vhd 
#
#       Latches A and B 
#       ---------------
add_file -vhdl ../vsrc/dlx/word_latch_e_1e.vhd 
add_file -vhdl ../vsrc/dlx/word_latch_e_1e-dataflow.vhd 
#
#       Latches PC, MAR, MDR
#       --------------------
add_file -vhdl ../vsrc/dlx/word_latch_e_1e1.vhd 
add_file -vhdl ../vsrc/dlx/word_latch_e_1e1-dataflow.vhd 
#
#       Multiplexer AMUX, DMUX
#       ----------------------
add_file -vhdl ../vsrc/dlx/word_mux2.vhd 
add_file -vhdl ../vsrc/dlx/word_mux2-dataflow.vhd 
#
#       Latch IR
#       --------
add_file -vhdl ../vsrc/dlx/ir_e_2e1.vhd 
add_file -vhdl ../vsrc/dlx/ir_e_2e1-behaviour.vhd 
#
#
#       ALU (adder with fast carry)
#       --- 
add_file -vhdl ../vsrc/dlx/alu_core.vhd 
add_file -vhdl ../vsrc/dlx/alu_core-behaviour.vhd 
add_file -vhdl ../vsrc/dlx/alu.vhd 
add_file -vhdl ../vsrc/dlx/alu-structural.vhd 
#
#       Bus Constants
#       -------------
add_file -vhdl ../vsrc/dlx/bus_const32.vhd 
add_file -vhdl ../vsrc/dlx/bus_const32-dataflow.vhd 
#
#       Register File 
#       -------------
add_file -vhdl ../vsrc/dlx/reg_fileram.vhd 
add_file -vhdl ../vsrc/dlx/reg_fileram-structural.vhd 
#
#       Datapath
#       --------
add_file -vhdl ../vsrc/dlx/datapath.vhd 
add_file -vhdl ../vsrc/dlx/datapath-structural.vhd 
#
#
#    DLX Processor System
#    ====================
#  Components: Core, Memory Unit, Controller Interface, Display Interface
#
#      Core
#      ----
add_file -vhdl ../vsrc/dlx/core.vhd 
add_file -vhdl ../vsrc/dlx/core-structural.vhd 
# 
#      Memory Unit
#      -----------
add_file -vhdl ../vsrc/dlx/mem.vhd 
add_file -vhdl ../vsrc/dlx/mem-structural.vhd 
add_file -vhdl ../vsrc/dlx/decoder.vhd 
add_file -vhdl ../vsrc/dlx/decoder-dataflow.vhd 
add_file -vhdl ../vsrc/dlx/memory_unit.vhd 
add_file -vhdl ../vsrc/dlx/memory_unit-structural.vhd 
#
#      Controller Interface
#      --------------------
add_file -vhdl ../vsrc/dlx/byte_dff.vhd 
add_file -vhdl ../vsrc/dlx/byte_dff-dataflow.vhd 
add_file -vhdl ../vsrc/dlx/qclk2phi.vhd 
add_file -vhdl ../vsrc/dlx/qclk2phi-dataflow.vhd 
add_file -vhdl ../vsrc/dlx/uc_if.vhd 
add_file -vhdl ../vsrc/dlx/uc_if-structural.vhd 
#
#      Display Interface
#      -----------------
add_file -vhdl ../vsrc/dlx/disp_mux.vhd 
add_file -vhdl ../vsrc/dlx/disp_mux-behaviour.vhd 
add_file -vhdl ../vsrc/dlx/lcd_if.vhd 
add_file -vhdl ../vsrc/dlx/lcd_if-dataflow.vhd 
#add_file -vhdl ../vsrc/dlx/display_if.vhd 
#add_file -vhdl ../vsrc/dlx/display_if-structural.vhd 
add_file -vhdl ../vsrc/dlx/lcd.vhd 
#
#      Complete DLX System Chip
#      ------------------------
add_file -vhdl ../vsrc/dlx/vsdlx.vhd 
add_file -vhdl ../vsrc/dlx/vsdlx-structural.vhd 
#

#set TOP xc3s700an

#add_file ./vhdlsrc/lcd.vhd
#add_file ./vhdlsrc/$TOP.vhd

#device options
set_option -technology Spartan3A
set_option -part XC3S700AN
set_option -package FGG484
set_option -speed_grade -4
# set_option -part_companion ""

#compilation/mapping options
set_option -use_fsm_explorer 0

# sequential_optimization_options
set_option -symbolic_fsm_compiler 1

# Compiler Options
set_option -compiler_compatible 0
set_option -resource_sharing 1

# mapper_options
set_option -frequency auto
set_option -write_verilog 0
set_option -write_vhdl 0

# Xilinx Spartan3
set_option -run_prop_extract 1
set_option -maxfan 10000
set_option -disable_io_insertion 0
set_option -pipe 1
set_option -retiming 0
set_option -update_models_cp 0
set_option -fixgatedclocks 3
set_option -fixgeneratedclocks 3
set_option -no_sequential_opt 0

#VIF options
set_option -write_vif 1

#automatic place and route (vendor) options
set_option -write_apr_constraint 1

#set result format/file last
project -result_file "./expdir/vsdlx.edf"

#
#implementation attributes

#set_option -vlog_std v2001
set_option -project_relative_includes 1
#impl -active "rev_1"
project -run

# Command not available in batch run: "open_file"
#open_file -rtl_view
#open_file -technology_view
