-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Behavioural architecture for the instruction decoder 3
--
--  Register Fiel address selection
--  
--  file ir_decode_3-behaviour.vhd
-- -----------------------------------------------------------

ARCHITECTURE behaviour OF ir_decode_3 IS

BEGIN
  decode_3 : PROCESS(instr_in)
  BEGIN
    CASE instr_in(0 TO 5) IS       -- Opcode from IR
      WHEN op_rr_alu =>            -- |-> any RRA operation  
        rd_s2_adr_sel     <= '1';  -- |   |    -> Rd = Rd(Rtype) 
      WHEN OTHERS    =>            -- |-> other Opcode
	rd_s2_adr_sel     <= '0';  -- |        -> Rd = Rd(Itype)
    END CASE;
  END PROCESS decode_3;

END behaviour;



