-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-Experimentelle Hardwareprojekte"  
--   Mar 2010 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS Synplify
--   Target architecture:
--        XILINX Spartan-3AN  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
--------------------------------------------------------------------------
--
--------------------------------------------------------------------------
-- MOORE machine:
--
--            X >______            ________________
--                     \   ______>|output_logic    |_______> Y
--                      \ /       |________________|
--                       \        ________________
--                      / \_____>|next_state_logic|______     
--                     /________>|________________|      |
--                     |              _________          |
--                     |             |state    |         |
--                     |_____________|registers|<________|
--                                   |_________|
--
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- architecture of the finite state machine
-- (Moore machine / microcoded output style / two separated processes
-- for output_logic and for next_state_logic)
--
-- file fsm_output-dataflow.vhd
--------------------------------------------------------------------------

ARCHITECTURE dataflow OF fsm_output IS

BEGIN

  ------------------------------
  -- Output logic
  ------------------------------
  output_logic : PROCESS (current_state)
  BEGIN
    CASE current_state IS
      --------------------------
      WHEN res_state     =>
        --
        -- PC, MAR, MDR <- 0x0000_0000
        --
        s1_enab      <= s1_const;
        -- s1 <=  bus_const(out1) (0x00000000)
        --
        s2_enab      <= s2_none;
        -- s2 <= 0xZZZZZZZZ
        --
        alu_op_sel   <= alu_pass_s1;
        -- dest_bus <= stored_s1
        --
        dest_enab    <= dest_res;
        -- PC, MAR <= dest_bus
        -- MDR <= mdr_mux (dest_bus) 
        --
        const_sel    <= const_00;
        -- bus_const(out1) <= 0x00000000
        --
        rf_op_sel    <= rfop_none;
        -- A, B, RF: inputs disabled
        --
        immed_sel    <= imm_dcare;
        -- not determined
        --
        mem_ctrl     <= mem_none;
        -- IR-Reg:   input disabled,
        -- Addr-Mux: PC
        -- MDR-Mux:  dest-bus
        -- Memory:   disabled, read
        -- Error:    disabled
        --
        state_number <= res_state_no;
        --------------------------
      WHEN fetch         =>
        --
        -- fetch instruction: IR <- M(PC) 
        --
        s1_enab      <= s1_none;
        -- s1 <= 0xZZZZZZZZ
        s2_enab      <= s2_none;
        -- s2 <= 0xZZZZZZZZ
        alu_op_sel   <= alu_dcare;
        -- not determined
        dest_enab    <= dest_none;
        -- C, PC, MAR, MDR inputs diabled
        const_sel    <= const_dcare;
        -- not determined
        rf_op_sel    <= rfop_none;
        -- A, B, RF inputs disabled       
        immed_sel    <= imm_dcare;
        -- not determined
        mem_ctrl     <= mem_fetch;
        -- IR-Reg:   IR      <= Mem_data
        -- Addr-Mux: Mem_adr <= PC
        -- MDR-Mux:  MDR     <= dest-bus
        -- Memory:   enabled, read
        -- Error:    disabled
        state_number <= fetch_no;
        --------------------------
      WHEN dec_pcinc4_ab =>
        --
        -- pc <- pc + 4 / load ab from reg.file / decode instruction
        --
        s1_enab      <= s1_const;
        s2_enab      <= s2_pc;
        alu_op_sel   <= alu_s1_add_s2;
        dest_enab    <= dest_pc;
        const_sel    <= const_04;
        rf_op_sel    <= rfop_ab_rf;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_none;
        state_number <= dec_pcinc4_ab_no;
        --------------------------
      WHEN sub           =>
        --
        -- sub: c <- a - y / y : b or immed (Rtype: B, Itype: immed) 
        --
        s1_enab      <= s1_a;
        s2_enab      <= s2_y;
        alu_op_sel   <= alu_s1_sub_s2;
        dest_enab    <= dest_c;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_s16;
        mem_ctrl     <= mem_none;
        state_number <= sub_no;
-------------------------------------
        -- additional states:
-------------------------------------        
        WHEN add  =>
        --
        -- add: c <- a + b 
        --
        ------------------------
        -- available constants:
        -- s1_a, s1_mdr, s1_immed, s1_const, s1_none
        --
        -- select S1 bus source:
          s1_enab      <= s1_a ;
        -- because with this constantvector a_out_en is active
        -- so contents of register A is connected to s1-Bus
        ------------------------
        -- available constants:
        -- s2_b, s2_immed, s2_y, s2_pc, s2_mar,
        -- s2_const, s2_none
        --
        -- select S2 bus source:
          s2_enab      <= s2_y;
        -- same as in S1-Bus
        ------------------------
        -- available constants:
        -- alu_pass_s1, alu_pass_s2, alu_s1_add_s2,
        -- alu_s1_sub_s2,alu_s1_and_s2, alu_s1_or_s2,
        -- alu_sll_s1_s2, alu_srl_s1_s2, alu_dcare
        --
        -- select Alu operation:
          alu_op_sel   <= alu_s1_add_s2;
        -- because the others are not the korrekt function
        ------------------------        
        -- available constants:
        -- dest_c, dest_pc, dest_mar, dest_mdr,
        -- dest_res (reset), dest_none
        --
        -- select Dest bus destination register:
          dest_enab    <= dest_c;
        -- because we want to write to a GPR
        ------------------------
        -- available constants:
        -- const_00, const_01, const_04, const_dcare
        --
        -- select constant: 
          const_sel    <= const_dcare;
        -- empty constant
        ------------------------
        -- available constants:
        -- rfop_none, rfop_ab_rf, rfop_rf_c
        --
        -- select register file operation:
          rf_op_sel    <= rfop_none;
        -- ???
        ------------------------
        -- available constants:
        -- imm_s16, imm_s26,imm_dcare
        --
        -- select immediate (16 or 26 bit from IR):
          immed_sel    <= imm_s16;
        --
        -- ???
        ------------------------
        -- available constants:
        -- mem_none, mem_fetch, mem_ldst, mem_lw,
        -- mem_sw, mem_error
        --
        -- select memory control:
          mem_ctrl     <= mem_none;
        -- no need for memory operation
        ------------------------        
        -- available constants:
        -- (for debugging purposes,
        --  one number for each state)
        -- res_state_no, fetch_no, dec_pcinc4_ab_no,
        -- memory_no, load_w_1_no, load_w_2_no,
        -- store_w_1_no, store_w_2_no, br_eqz_no,
        -- branch_no, jump_no, sub_no, add_no, 
        -- slt_1_no, set_to_1_no, set_to_0_no,
        -- write_back_no, hlt_state_no, err_state_no
        -- and_no, or_no, sll_no, srl_no,
        --
          state_number <= add_no;
  --------------------------------------------------
        WHEN  and_1 =>
        --
        -- and: c <- a and y / y : b or immed 
        --
          s1_enab      <= s1_a;
          s2_enab      <= s2_y;
          alu_op_sel   <= alu_s1_and_s2;
          dest_enab    <= dest_c;
          const_sel    <= const_dcare;
          rf_op_sel    <= rfop_none;
          immed_sel    <= imm_s16;
          mem_ctrl     <= mem_none;
          state_number <= and_no;
        ---------------------------
        WHEN or_1 =>
        --
        -- or: c <- a or y / y : b or immed  
        --
          s1_enab      <= s1_a;
          s2_enab      <= s2_y;
          alu_op_sel   <= alu_s1_or_s2;
          dest_enab    <= dest_c;
          const_sel    <= const_dcare;
          rf_op_sel    <= rfop_none;
          immed_sel    <= imm_s16;
          mem_ctrl     <= mem_none;
          state_number <= or_no;
        ---------------------------

        WHEN sll_1 =>
        --
        -- sll: c <- a sll y / y : b or immed
        --
          s1_enab      <= s1_a;
          s2_enab      <= s2_y;
          alu_op_sel   <= alu_sll_s1_s2;
          dest_enab    <= dest_c;
          const_sel    <= const_dcare;
          rf_op_sel    <= rfop_none;
          immed_sel    <= imm_s16;
          mem_ctrl     <= mem_none;
          state_number <= sll_no;
        ----------------------------
        WHEN srl_1 =>
        --
        -- srl: c <- a srl y / y : b or immed
        --
          s1_enab      <= s1_a;
          s2_enab      <= s2_y;
          alu_op_sel   <= alu_srl_s1_s2;
          dest_enab    <= dest_c;
          const_sel    <= const_dcare;
          rf_op_sel    <= rfop_none;
          immed_sel    <= imm_s16;
          mem_ctrl     <= mem_none;
          state_number <= srl_no;
        ----------------------------

      WHEN wr_back   =>
        --
        -- write back (reg.reg.alu-instr): rf <- c 
        --
        s1_enab      <= s1_none;
        s2_enab      <= s2_none;
        alu_op_sel   <= alu_dcare;
        dest_enab    <= dest_none;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_rf_c;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_none;
        state_number <= wr_back_no;
        ---------------------------
      WHEN memory    =>
        --
        -- mar <- a + imm_s16)
        --
        s1_enab      <= s1_a;
        s2_enab      <= s2_immed;
        alu_op_sel   <= alu_s1_add_s2;
        dest_enab    <= dest_mar;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_ab_rf;
        immed_sel    <= imm_s16;
        mem_ctrl     <= mem_ldst;
        state_number <= memory_no;
        --------------------------
      WHEN load_w_1  =>
        --
        -- mdr <- mem
        --
        s1_enab      <= s1_none;
        s2_enab      <= s2_none;
        alu_op_sel   <= alu_dcare;
        dest_enab    <= dest_mdr;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_lw;
        state_number <= load_w_1_no;
        --------------------------
      WHEN load_w_2  =>
        --
        -- write back for load word : c <- mdr
        --
        s1_enab      <= s1_mdr;
        s2_enab      <= s2_none;
        alu_op_sel   <= alu_pass_s1;
        dest_enab    <= dest_c;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_none;
        state_number <= load_w_2_no;
        --------------------------
      WHEN store_w_1 =>
        --
        -- mdr <- b : move b into MDR via s2 bus
        --
        s1_enab      <= s1_none;
        s2_enab      <= s2_b;
        alu_op_sel   <= alu_pass_s2;
        dest_enab    <= dest_mdr;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_ldst;
        state_number <= store_w_1_no;
        ---------------------------
      WHEN store_w_2 =>
        --
        -- write word : mem <- mdr
        --
        s1_enab      <= s1_none;
        s2_enab      <= s2_none;
        alu_op_sel   <= alu_dcare;
        dest_enab    <= dest_none;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_sw;
        state_number <= store_w_2_no;
        ---------------------------
      WHEN br_eqz    =>
        --
        -- branch if equal zero : a - 0 = 0 ?
        --
        s1_enab      <= s1_a;
        s2_enab      <= s2_const;
        alu_op_sel   <= alu_s1_sub_s2;
        dest_enab    <= dest_none;
        const_sel    <= const_00;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_none;
        state_number <= br_eqz_no;
        --------------------------
      WHEN branch    =>
        --
        -- execute branch : temp(mar) <- pc + imm_s16
        --
        s1_enab      <= s1_immed;
        s2_enab      <= s2_pc;
        alu_op_sel   <= alu_s1_add_s2;
        dest_enab    <= dest_pc;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_s16;
        mem_ctrl     <= mem_none;
        state_number <= branch_no;
        --------------------------
      WHEN jump      =>
        --
        -- calculate jump addr: temp(mar) <- pc + imm_s26
        --
        s1_enab      <= s1_immed;
        s2_enab      <= s2_pc;
        alu_op_sel   <= alu_s1_add_s2;
        dest_enab    <= dest_pc;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_s26;
        mem_ctrl     <= mem_none;
        state_number <= jump_no;
        --------------------------
      WHEN slt       =>
        --
        -- set if s1 equal s2 
        -- none <- a sub b  
        --
        s1_enab      <= s1_a;
        s2_enab      <= s2_y;
        alu_op_sel   <= alu_s1_sub_s2;
        dest_enab    <= dest_none;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_none;
        state_number <= slt_no;
        ------------------------
      WHEN set_to_1  =>
        --
        -- c <- X"0000_0001"
        --
        s1_enab      <= s1_const;
        s2_enab      <= s2_none;
        alu_op_sel   <= alu_pass_s1;
        dest_enab    <= dest_c;
        const_sel    <= const_01;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_none;
        state_number <= set_to_1_no;
        --------------------------
      WHEN set_to_0  =>
        --
        -- c <- X"0000_0000"
        --
        s1_enab      <= s1_const;
        s2_enab      <= s2_none;
        alu_op_sel   <= alu_pass_s1;
        dest_enab    <= dest_c;
        const_sel    <= const_00;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_none;
        state_number <= set_to_0_no;
        --------------------------
      WHEN hlt_state =>
        --
        -- set all outputs to high impedance/ inouts to input 
        --
        s1_enab      <= s1_none;
        s2_enab      <= s2_none;
        alu_op_sel   <= alu_dcare;
        dest_enab    <= dest_none;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_none;
        state_number <= hlt_state_no;
        --------------------------
      WHEN err_state =>
        --
        -- set all outputs to high impedance/ inouts to input / set error  
        --
        s1_enab      <= s1_none;
        s2_enab      <= s2_none;
        alu_op_sel   <= alu_dcare;
        dest_enab    <= dest_none;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_error;
        state_number <= err_state_no;
        ------------------------
      WHEN OTHERS    =>
        --
        -- set all outputs to high impedance/ inouts to input / set error  
        --
        s1_enab      <= s1_none;
        s2_enab      <= s2_none;
        alu_op_sel   <= alu_dcare;
        dest_enab    <= dest_none;
        const_sel    <= const_dcare;
        rf_op_sel    <= rfop_none;
        immed_sel    <= imm_dcare;
        mem_ctrl     <= mem_error;
        state_number <= err_state_no;
        --------------------------        
    END CASE;
  END PROCESS output_logic;

END dataflow;




