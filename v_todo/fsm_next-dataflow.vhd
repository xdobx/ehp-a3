-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-Experimentelle Hardwareprojekte"  
--   Mar 2010 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS Synplify
--   Target architecture:
--        XILINX Spartan-3AN  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
-- MOORE machine:
--
--            X >______            ________________
--                     \   ______>|output_logic    |_______> Y
--                      \ /       |________________|
--                       \        ________________
--                      / \_____>|next_state_logic|______     
--                     /________>|________________|      | S'
--                     |              _________          |
--                     |             |state    |         |
--                   S |_____________|registers|<________|
--                                   |_________|
--
--------------------------------------------------------------
--  architecture of the finite state machine
--  (Moore machine / next_state_logic)
--
--  file fsm_next-dataflow.vhd
--------------------------------------------------------------

ARCHITECTURE dataflow OF fsm_next IS

BEGIN
  --------------------
  -- Next state logic
  -- S' = f (X,S)
  --------------------
  next_state_logic : PROCESS (current_state, reset, halt, alu_neg,
                              alu_zero, dec_1_in, dec_2_in)
  BEGIN
    CASE current_state IS
      ----------------------
      WHEN res_state     =>
        next_state   <= fetch;
        ----------------------
      WHEN fetch         =>
        --
        -- check for halt
        --
        IF halt = '1' THEN
          next_state <= hlt_state;
        ELSE
          next_state <= dec_pcinc4_ab;
        END IF;
        ----------------------
      WHEN dec_pcinc4_ab =>
        next_state   <= dec_1_in;
        ----------------------
      WHEN sub           =>
        next_state   <= wr_back;
-------------------------------------
-- additional states:
-------------------------------------
-- add:
      WHEN add           =>
        next_state   <= wr_back;
        ----------------------
-- and:
      WHEN and_1         =>
        next_state   <= wr_back;
        ----------------------
-- or:
      WHEN or_1          =>
        next_state   <= wr_back;
        ----------------------
-- sll:
      WHEN sll_1         =>
        next_state   <= wr_back;
        ----------------------
-- srl:
      WHEN srl_1         =>
        next_state   <= wr_back;
------------------------------
      WHEN wr_back       =>
        next_state   <= fetch;
        ----------------------
      WHEN memory        =>
        next_state   <= dec_2_in;
        ----------------------
      WHEN load_w_1      =>
        next_state   <= load_w_2;
        ----------------------
      WHEN load_w_2      =>
        next_state   <= wr_back;
        ----------------------
      WHEN store_w_1     =>
        next_state   <= store_w_2;
        -----------------------
      WHEN store_w_2     =>
        next_state   <= fetch;
        ----------------------
      WHEN br_eqz        =>
        --
        -- check if equal zero : a - 0 = 0 ?
        --
        IF alu_zero = '0' THEN
          next_state <= fetch;
        ELSE
          next_state <= branch;
        END IF;
        ----------------------
      WHEN branch        =>
        next_state   <= fetch;
        ----------------------
      WHEN jump          =>
        next_state   <= fetch;
        ----------------------
      WHEN slt           =>
        --
        -- check alu_neg = '1'
        --
        IF alu_neg = '1' THEN
          next_state <= set_to_1;
        ELSE
          next_state <= set_to_0;
        END IF;
        ----------------------
      WHEN set_to_1      =>
        next_state   <= wr_back;
        ----------------------
      WHEN set_to_0      =>
        next_state   <= wr_back;
        ----------------------
      WHEN hlt_state     =>
        --
        -- wait until halt = '0'
        -- 
        IF halt = '0' THEN
          next_state <= dec_pcinc4_ab;
        ELSE
          next_state <= hlt_state;
        END IF;
        ----------------------
      WHEN err_state     =>
        --
        -- wait until reset = '1'
        --  
        IF reset = '0' THEN
          next_state <= err_state;
        ELSE
          next_state <= res_state;
        END IF;
        ----------------------
      WHEN OTHERS        =>
        --
        -- wait until reset = '1'
        --  
        IF reset = '0' THEN
          next_state <= err_state;
        ELSE
          next_state <= res_state;
        END IF;
        ----------------------
    END CASE;
  END PROCESS next_state_logic;
END dataflow;





