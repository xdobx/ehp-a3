-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-Experimentelle Hardwareprojekte"  
--   Mar 2010 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS Synplify
--   Target architecture:
--        XILINX Spartan-3AN  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Behavioural architecture for the instruction decoder 1
--
--  file ir_decode_1-behaviour.vhd
-- -----------------------------------------------------------

ARCHITECTURE behaviour OF ir_decode_1 IS

BEGIN
  
  decode_1 : PROCESS(instr_in)
  BEGIN
    CASE instr_in(0 TO 5) IS       -- Opcode from IR
      WHEN op_rr_alu =>            -- |-> any RRA operation 
        CASE instr_in(26 TO 31) IS -- |   RRA Function (rr_func)
	  WHEN rr_func_nop =>      -- |   |-> no operation
	    dec1_out <= fetch;     -- |   |    -> fetch
	  WHEN rr_func_sub =>      -- |   |-> subtraction
	    dec1_out <= sub;       -- |   |    -> sub
          WHEN rr_func_slt =>      -- |   |-> set if lower than
	    dec1_out <= slt;       -- |   |    -> slt
-------------------------------------------------------------
-- additional RRA-function:           |   |
-- rr_func_add                        |   |
          WHEN rr_func_add =>      -- |   |-> addition
	    dec1_out <= add;       -- |   |    -> add
-- rr_func_and                        |   |
          WHEN rr_func_and =>      -- |   |-> logical and
	    dec1_out <= and_1;     -- |   |    -> and
-- rr_func_or                         |   |
          WHEN rr_func_or  =>      -- |   |-> logical or
	    dec1_out <= or_1;      -- |   |    -> or
-- rr_func_sll                        |   |
          WHEN rr_func_sll =>      -- |   |-> shift left logical
	    dec1_out <= sll_1;     -- |   |    -> sll
-- rr_func_srl                        |   |
          WHEN rr_func_srl =>      -- |   |-> shift right logical
	    dec1_out <= srl_1;     -- |   |    -> srl
-------------------------------------------------------------
          WHEN OTHERS      =>      -- |   |-> unkown 
	    dec1_out <= err_state; -- |        -> err_state
	END CASE;                  -- |
      WHEN op_lw_i |               -- |-> load word  OR
           op_sw_i   =>            -- |   store word
	dec1_out     <= memory;    -- |        -> memory
      WHEN op_beqz   =>            -- |-> branch if equal zero
	dec1_out     <= br_eqz;    -- |        -> br_eqz
      WHEN op_j      =>            -- |-> jump
	dec1_out     <= jump;      -- |        -> jump
      WHEN op_sub_i  =>            -- |-> sub
	dec1_out     <= sub;       -- |        -> sub
-------------------------------------------------------------
-- additional Opcode                  |
-- op_add_i                           |
      WHEN op_add_i  =>            -- |-> immed-addition
	dec1_out     <= add;       -- |        -> add
-- op_and_i                           |
      WHEN op_and_i  =>            -- |-> immed-and
	dec1_out     <= and_1;     -- |        -> and
-- op_or_i                            |
      WHEN op_or_i   =>            -- |-> immed-or
	dec1_out     <= or_1;      -- |        -> or
-- op_sll_i                           |
      WHEN op_sll_i  =>            -- |-> immed-shift left logical
	dec1_out     <= sll_1;     -- |        -> sll
-- op_srl_i                           |
      WHEN op_srl_i  =>            -- |-> immed-shift right logical
	dec1_out     <= srl_1;     -- |        -> srl
--------------------------------------------------------------
      WHEN OTHERS    =>            -- |-> unkown
        dec1_out     <= err_state; -- |        -> err_state
    END CASE;
  END PROCESS decode_1;     

END behaviour;












