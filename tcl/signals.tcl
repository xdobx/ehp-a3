
#######################################################
#   SIMULATIONSCRIPT signals.tcl
#   DLXJ Processor Experiment within the CT-PRAKTIKUM 
#   Jan. 2010 A.Reinsch / LS-RA
#   University of Jena / Dept. of Computer Science
#######################################################

# Zustaende der Steuerung:
# -----------------------
set state_no /TB/SYSTEM/COREDLX/CTRL/SMA/FOUT/STATE_NUMBER
set fsm_state /TB/FSM_STATE
 
# Signale Datenpfad global:
# ------------------------
set phi1 /TB/SYSTEM/PHI1
set phi2 /TB/SYSTEM/PHI2
set s1_bus /TB/SYSTEM/COREDLX/DPATH/S1_BUS
set s2_bus /TB/SYSTEM/COREDLX/DPATH/S2_BUS
set dest_bus TB/SYSTEM/COREDLX/DPATH/DEST_BUS
set data_in /TB/SYSTEM/COREDLX/DPATH/DATA_IN
set data_out /TB/SYSTEM/COREDLX/DPATH/DATA_OUT 
set addr_out /TB/SYSTEM/COREDLX/A_BUS

# Alu:
# ---
set alu_in1 /TB/SYSTEM/COREDLX/DPATH/ALUUNIT/ALUCORE/STORED_S1
set alu_in2 /TB/SYSTEM/COREDLX/DPATH/ALUUNIT/ALUCORE/STORED_S2
#alias -ALU_FUNC
#alias ALU_FUNC /TB/SYSTEM/COREDLX/DPATH/ALUUNIT/ALUCORE/ALU_OP
set alu_zero /TB/SYSTEM/COREDLX/DPATH/ALUUNIT/ALUCORE/ZERO
set alu_neg /TB/SYSTEM/COREDLX/DPATH/ALUUNIT/ALUCORE/NEGATIVE
  
# Reg. File:
# ---------
set rf_in /TB/SYSTEM/COREDLX/DPATH/RF/D
set rf_out1 /TB/SYSTEM/COREDLX/DPATH/RF/Q1
set rf_out2 /TB/SYSTEM/COREDLX/DPATH/RF/Q2
set rf_addr_in /TB/SYSTEM/COREDLX/DPATH/RF/ADDR_IN
set rf_addr_out1 /TB/SYSTEM/COREDLX/DPATH/RF/ADDR_OUT1
set rf_addr_out2 /TB/SYSTEM/COREDLX/DPATH/RF/ADDR_OUT2
set rf_en_in /TB/SYSTEM/COREDLX/DPATH/RF/WRITE_EN
 
# A-Latch:
# -------
set a_en_out /TB/SYSTEM/COREDLX/DPATH/A/OUT_EN
set a_en_in /TB/SYSTEM/COREDLX/DPATH/A/GATE_EN
  
# B-Latch:
# -------
set b_en_out /TB/SYSTEM/COREDLX/DPATH/B_OUT_EN
set b_en_in /TB/SYSTEM/COREDLX/DPATH/B/GATE_EN
  
# C-Latch:
# -------
set c_en_in /TB/SYSTEM/COREDLX/DPATH/C/GATE_EN

# PC:
# --
set pc_out2 /TB/SYSTEM/COREDLX/DPATH/PC/Q2
set pc_en_out1 /TB/SYSTEM/COREDLX/DPATH/PC/OUT_EN1
set pc_en_in /TB/SYSTEM/COREDLX/DPATH/PC/GATE_EN
  
# MAR:
# ---
set mar_out2 /TB/SYSTEM/COREDLX/DPATH/MAR/Q2
set mar_en_out1 /TB/SYSTEM/COREDLX/DPATH/MAR/OUT_EN1
set mar_en_in /TB/SYSTEM/COREDLX/DPATH/MAR/GATE_EN

# MDR:
# ---
set mdr_out2 /TB/SYSTEM/COREDLX/DPATH/MDR/Q2
set mdr_in /TB/SYSTEM/COREDLX/DPATH/MDR/D
set mdr_en_in /TB/SYSTEM/COREDLX/DPATH/MDR/GATE_EN
set mdr_en_out1 /TB/SYSTEM/COREDLX/DPATH/MDR/OUT_EN1
  
# AMUX:
# ----
set amux_sel_/in0_in1 /TB/SYSTEM/COREDLX/DPATH/AMUX/SEL
  
# DMUX:
# ----
set dmux_sel_/in0_in1 /TB/SYSTEM/COREDLX/DPATH/DMUX/SEL
  
# IR:
# --
set ir_out /TB/SYSTEM/COREDLX/DPATH/IR/IR_OUT
set ir_en_in /TB/SYSTEM/COREDLX/DPATH/IR/GATE_EN
set immed_en_out1 /TB/SYSTEM/COREDLX/DPATH/IR/IMMED_O1_EN
set immed_en_out2 /TB/SYSTEM/COREDLX/DPATH/IR_IMMED_O2_EN
set immed_size_/16_26 /TB/SYSTEM/COREDLX/DPATH/IR/IMMED_SIZE
set immed_sign /TB/SYSTEM/COREDLX/DPATH/IR_IMMED_SIGN
  
# CONST:
# -----
set /const_en_out1 /TB/SYSTEM/COREDLX/DPATH/CONST/OUT_EN1  
set /const_en_out2 /TB/SYSTEM/COREDLX/DPATH/CONST/OUT_EN2  
set const_sel_0-1-4 /TB/SYSTEM/COREDLX/DPATH/CONST/SEL

# Signale Prozessorkern:
# ---------------------
set halt /TB/SYSTEM/COREDLX/HALT
set reset /TB/SYSTEM/COREDLX/RESET
set enable /TB/SYSTEM/COREDLX/ENABLE
set error /TB/SYSTEM/COREDLX/ERROR
set rd_/wr /TB/SYSTEM/COREDLX/RW

# DISPLAY:
# -------
set display_out /TB/SYSTEM/SEVSEG/DISP_8DIG


# Signalverlauf aufzeichnen
# -------------------------

# Post Simulation:
#dump -vpd -o dlxj_waves.vpd $phi1 $phi2 $fsm_state $state_no $s1_bus $s2_bus $dest_bus $data_in $data_out $addr_out $display_out $immed_en_out2 $b_en_out 
#
# Interaktive Simulation:
add_wave $phi1 $phi2 $fsm_state $s1_bus $s2_bus $dest_bus $data_in $data_out $addr_out $display_out $b_en_out $immed_en_out2 $immed_sign



#
# alt (scirocco):
#dump -wave $phi1 \
#$phi2 \
#$fsm_state \
#$state_no \
#$s1_bus \
#$s2_bus \
#$dest_bus \
#$data_in \
#$data_out \
#$addr_out \
#$display_out \
#$immed_en_out2 \
#$b_en_out

#\
#/tb/starttrigger \
#$ir_out
