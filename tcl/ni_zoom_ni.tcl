set start_zoom_time [expr [gui_get_time] - 60000]
#run 100ns
#gui_sim_continue
gui_sim_send {run 700 ns}
#gui_set_active_window -window Wave.1
#gui_wv_zoom_outfull -id Wave.1
gui_sim_send {60 ns}
gui_wv_zoom_timerange -id Wave.1 $start_zoom_time [expr [gui_get_time] + 2000]
