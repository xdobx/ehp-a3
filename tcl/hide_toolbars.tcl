
#######################################################
#   SIMULATIONSCRIPT signals.tcl
#   DLXJ Processor Experiment within the CT-PRAKTIKUM 
#   Jan. 2010 A.Reinsch / LS-RA
#   University of Jena / Dept. of Computer Science
#######################################################


# ToolBar settings
gui_delete_toolbar -name {&File}
gui_delete_toolbar -name {TimeOperations}
gui_delete_toolbar -name {&Edit}
gui_delete_toolbar -name {Simulator}
gui_delete_toolbar -name {Signal}
gui_delete_toolbar -name {&Scope}
gui_delete_toolbar -name {&Trace}
gui_delete_toolbar -name {&Window}
gui_delete_toolbar -name {Zoom}
gui_delete_toolbar -name {Zoom And Pan History}
# End ToolBar settings

