
set icons_dir [getenv DLXJ]/icons
set tcl_dir [getenv DLXJ]/tcl

#gui_close_window -type Console
gui_close_window -type Wave
gui_close_window -type Source
gui_close_window -type Schematic
gui_close_window -type Data
gui_close_window -type DriverLoad
gui_close_window -type List
gui_close_window -type Memory
gui_close_window -type HSPane
gui_close_window -type DLPane
gui_close_window -type Assertion
gui_close_window -type CoverageTable
gui_close_window -type CoverageMap
##gui_close_window -type CovDensity
gui_close_window -type CovDetail
gui_close_window -type Local
gui_close_window -type Watch
##gui_close_window -type Grading
gui_close_window -type Group

# Create and position top-level windows
if {![gui_exist_window -window TopLevel.1]} {
    set TopLevel.1 [ gui_create_window -type TopLevel \
       -icon $::env(DVE)/auxx/gui/images/toolbars/dvewin.xpm] 
} else { 
    set TopLevel.1 TopLevel.1
}

gui_show_window -window ${TopLevel.1} -show_state normal -rect {{0 0} {1400 720}}
gui_set_env TOPLEVELS::TARGET_FRAME(Wave) ${TopLevel.1}
gui_close_window -type Source



source [getenv DLXJ]/tcl/signals.tcl

gui_update_layout -id Wave.1 {{left -2} {top -15} {width 1224} {height 600} {show_state maximized} {dock_state undocked} {dock_on_new_line false} {child_wave_left 286} {child_wave_right 928} {child_wave_colname 175} {child_wave_colvalue 100} {child_wave_col1 0} {child_wave_col2 1}}

gui_update_layout -id {Console.1} {{left 0} {top 0} {width 1269} {height 120} {show_state normal} {dock_state bottom} {dock_on_new_line true}}

# alte Version:
#proc next_instr {} { set start_zoom_time [expr [gui_get_time] - 50000]
#    gui_sim_send {run 700 ns}
#    gui_sim_send {run 50 ns}
#    gui_wv_zoom_timerange -id Wave.1 $start_zoom_time [expr [gui_get_time] + 2000]
#}
# neu (dve-Version: H-2013.06-SP1_Full64):
proc next_instr {} { set start_zoom_time [expr [gui_get_time -id TopLevel.1] - 50000]
    gui_sim_send {run 700 ns}
    gui_sim_send {run 50 ns}
    gui_wv_zoom_timerange -id Wave.1 $start_zoom_time [expr [gui_get_time -id TopLevel.1] + 2000]
}
proc run80 {} { gui_sim_send {run 80 ns}}

# eigenes Menue erzeugen:

gui_delete_menu -menu "&DLXJ Simulation"

#gui_create_menu -menu "&DLXJ Simulation->Simulator->Continue" 
#-tcl_cmd "tcl_source $tcl_dir/continue_zoom.tcl" 
#-icon_file "$icons_dir/icon_continue.xpm" 
#-help_string "Run complete simulation and zoom out wave window"

#gui_create_menu -menu "&DLXJ Simulation->Simulator->run 25ns" 
#-tcl_cmd "tcl_source $tcl_dir/run25_zoom.tcl" 
#-icon_file "$icons_dir/icon25ns.xpm" 
#-help_string "Run relative 25ns and zoom out wave window"

gui_create_menu -menu "&DLXJ Simulation->Simulator->Next Instruction" \
-tcl_cmd next_instr \
-icon_file "$icons_dir/icon_ni.xpm" \
-help_string "Execute next instruction and zoom in these instruction"

gui_create_menu -menu "&DLXJ Simulation->Simulator->run 80ns" \
-tcl_cmd run80 \
-icon_file "$icons_dir/icon80ns.xpm" \
-help_string "Run relative 80ns"

gui_create_menu -menu "&DLXJ Simulation->Simulator->ProcRst" \
-tcl_cmd restart -icon_file "$icons_dir/icon_restart.xpm" \
-help_string "Processor Restart"


# eigene Werkzeugleiste erzeugen:
gui_create_toolbar -name "werkzeugleiste"
#gui_create_toolbar_item -toolbar "werkzeugleiste" -menu "&DLXJ Simulation->Simulator->Continue"
#gui_create_toolbar_item -toolbar "werkzeugleiste" -menu "&DLXJ Simulation->Simulator->run 25ns"
gui_create_toolbar_item -toolbar "werkzeugleiste" -menu "&DLXJ Simulation->Simulator->Next Instruction"
gui_create_toolbar_item -toolbar "werkzeugleiste" -menu "&DLXJ Simulation->Simulator->run 80ns"
gui_create_toolbar_item -toolbar "werkzeugleiste" -menu "&DLXJ Simulation->Simulator->ProcRst"


########################################################################################################
# Experimente zum ersten Verstehen von DVE:
#         set top [gui_create_window -type TopLevel]
#         gui_create_window -type Console -parent $top -dock_state {bottom} -dock_extent 
#gui_open_window Wave
#add_wave /tb_adder
#alias run100 "run 100ns"
#gui_set_hotkey -tcl_cmd "gui_sim_run -args run 100ns" -hot_key "Alt+F12" 
#gui_set_hotkey -tcl_cmd "tcl_source run_zoom.tcl" -hot_key "F12"  -replace
#gui_delete_menu -menu "&ALU Simulation"
#gui_create_menu -menu "&ALU Simulation->Run->50ns" -tcl_cmd "tcl_source run_zoom.tcl" -icon_file "../NoName.xpm" 
#-tool_tip "short tooltip description for My Item"
#-enable_cmd "enable_my_item"
#-hot_key "Ctrl+N"
#-help_string "long status bar description for My Item"
#gui_create_toolbar -name "alu"
#gui_create_toolbar_item -toolbar "alu" -menu "&ALU Simulation->&Run->&50ns"
#gui_hide_toolbar -all
#gui_show_toolbar -toolbar "alu"
#tcl_source alu_menu.tcl
#gui_hide_toolbar -all
#gui_show_toolbar -toolbar "alu"
#alias full gui_wv_zoom_outfull -id Wave.1 # Zoom completely out in the waveform window.

# ToolBar settings
gui_hide_toolbar -toolbar {TimeOperations}
gui_hide_toolbar -toolbar {&File}
gui_hide_toolbar -toolbar {&Edit}
gui_hide_toolbar -toolbar {CopyPaste}
gui_hide_toolbar -toolbar {&Trace}
gui_hide_toolbar -toolbar {TraceInstance}
gui_hide_toolbar -toolbar {BackTrace}
gui_hide_toolbar -toolbar {&Scope}
gui_hide_toolbar -toolbar {&Window}
gui_hide_toolbar -toolbar {Signal}
#gui_hide_toolbar -toolbar {Zoom}
gui_hide_toolbar -toolbar {Zoom And Pan History}
gui_hide_toolbar -toolbar {Grid}
gui_hide_toolbar -toolbar {Simulator}
gui_hide_toolbar -toolbar {Interactive Rewind}
gui_hide_toolbar -toolbar {Testbench}
#gui_hide_toolbar -all
gui_set_toolbar_attributes -toolbar {werkzeugleiste} -dock_state top
gui_set_toolbar_attributes -toolbar {werkzeugleiste} -offset 0
gui_show_toolbar -toolbar {werkzeugleiste}

