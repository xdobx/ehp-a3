-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
--   Entity declaration of the bus multiplexer to display all
--   processor busses
-- 
--   (file disp_mux.vhd)
---------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;

ENTITY disp_mux IS
  PORT (
    
    core_addr_out          : IN  dlx_address;  -- address out to memory
    core_data_in           : IN  dlx_word;     -- data in from memory
    core_data_out          : IN  dlx_word;     -- data out to memory
    display_latch_out      : IN  dlx_word;  -- result latch out
    s1_bus_display         : IN  dlx_word;  -- s1_bus for display only
    s2_bus_display         : IN  dlx_word;  -- s2_bus for display only
    dest_bus_display       : IN  dlx_word;  -- dest_bus for display only
    ir_reg_display         : IN  dlx_word;  -- ir output  for display only
    sel                    : IN  std_logic_vector(2 DOWNTO 0);
    y                      : OUT dlx_word);     -- output

END disp_mux;

