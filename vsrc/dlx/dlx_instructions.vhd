-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
--------------------------------------------------------------------------
--
--------------------------------------------------------------------------
--   Package specification for DLX instructions
--  
--  (file dlx-instructions.vhd)
--------------------------------------------------------------------------
--
--------------------------------------------------------------------------
-- A dlx instruction is 32 bits wide. There are three instruction formats:
--
-- I-type:
--  0         5 6      10 11     15 16                            31
-- +----------------------------------------------------------------+
-- |  opcode   |   rs1   |   rd    |           immed16              |
-- +----------------------------------------------------------------+
--
-- Encodes: - loads and stores words with immediate displacement
--          - all immediates (rd <- rs1 op immediate)
--          - conditional branch instructions (rd unused)
--   
-- R-type:
--  0         5 6      10 11     15 16     20 21      25 26       31
-- +----------------------------------------------------------------+
-- |  opcode   |   rs1   |   rs2   |   rd    |        func          |
-- +----------------------------------------------------------------+
--                                           | fp_func  | rr_func   |
--                                           +----------------------+
--
-- Encodes: - register-register ALU operations (rd <- rs1 func rs2)
--            function encodes the datapath operation (add, sub, ...)
--          - loads and stores of words with register displacement
--
-- J-type:
--  0         5 6                                                 31
-- +----------------------------------------------------------------+
-- |  opcode   |                     immed26                        |
-- +----------------------------------------------------------------+
--
-- Encodes: - jump
--            
--------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;


PACKAGE dlx_instructions IS
  -- -------------------------
  -- encoding of Opcodes
  -- -------------------------
  CONSTANT op_rr_alu	: dlx_opcode := "000000"; --00

  CONSTANT op_lw_i	: dlx_opcode := "000100"; --04

  CONSTANT op_sw_i	: dlx_opcode := "001000"; --08

  CONSTANT op_j 	: dlx_opcode := "001100"; --0c

  CONSTANT op_beqz	: dlx_opcode := "010000"; --10
  --
  -- opcodes for experimental implementations only,
  -- not part of basic DLXJ instruction set :
  --
  constant op_sub_i	: dlx_opcode := "010110"; --16
  CONSTANT op_add_i	: dlx_opcode := "010100"; --14
  constant op_and_i	: dlx_opcode := "011000"; --18
  constant op_or_i	: dlx_opcode := "011001"; --19
  constant op_sll_i	: dlx_opcode := "011100"; --1c
  constant op_srl_i	: dlx_opcode := "011110"; --1e
  --
  -- -------------------------------------------------
  -- encoding of register-register ALU functions
  -- -------------------------------------------------
  CONSTANT rr_func_nop		: dlx_rr_func := "000000"; --00

  CONSTANT rr_func_sub	 	: dlx_rr_func := "000110"; --06

  CONSTANT rr_func_slt		: dlx_rr_func := "010100"; --14
  --
  -- register-register ALU functions for experimental
  -- implementations only, not part of basic DLXJ
  -- instruction set :
  -- 
  CONSTANT rr_func_add		: dlx_rr_func := "000100"; --04
  constant rr_func_and	        : dlx_rr_func := "001000"; --08
  constant rr_func_or	        : dlx_rr_func := "001001"; --09
  constant rr_func_sll	        : dlx_rr_func := "001100"; --0c
  constant rr_func_srl  	: dlx_rr_func := "001110"; --0e
  --
  -- -------------------------------------------------
END dlx_instructions;


