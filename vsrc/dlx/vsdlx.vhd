-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        Synopsys Synplify premier
--   Target architecture:
--        XILINX Spartan3an  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--
-- Entity declaration of the DLXJ Processor System (top)
--
-- V(ery)S(mall)DLX-version for Virtex FPGA
--  
-- (file vsdlx.vhd)
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
--synopsys translate_off
LIBRARY UNISIM;
USE UNISIM.ALL;
--synopsys translate_on

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
LIBRARY work;
USE work.lcd_conv.ALL;

ENTITY vsdlx IS
  PORT (
    --
    -- Clock Sources:
    ---------
    uc_clk      : IN  std_logic;  -- uc port E(5) (AVR), single clock pulse
    q_clk       : IN  std_logic;  -- crystal clock generator
    
    -- LCD Display:
    ------
    LCD_DB                      : OUT std_logic_vector(7 DOWNTO 0);
    LCD_E, LCD_RS, LCD_RW       : OUT std_logic;    

    -- UC-Ports (AVR):
    ------------
    -- AVR_A, AVR_B                : IN  std_logic_vector(7 DOWNTO 0);
    -- AVR_C, AVR_D                : OUT std_logic_vector(7 DOWNTO 0)
    uc_port_a  : OUT uc_byte;
    uc_port_b  : IN  uc_byte;          
    uc_port_c  : IN  uc_byte;        
    --
    state       : OUT fsm_state_numbers;  -- display states
                                                     -- (2 digits)
    phi1_2      : OUT std_logic_vector(1 DOWNTO 0)  -- display two-phase clock
    --display     : OUT std_logic_vector(31 DOWNTO 0); -- display data or
                                                     -- address (8 digits)
    --error       : OUT std_logic_vector(7 DOWNTO 0);  -- display error signal
                                                     -- (8 decimal points)
);
END vsdlx ;










