-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
-- 
-- -----------------------------------------------------------
--
--  Dataflow descrition of the transparent latch (32-bit)
--  
--  (file word_latch_1-dataflow.vhd)
-- -----------------------------------------------------------

ARCHITECTURE structural OF word_latch_1 IS

-- virtexe:
--   COMPONENT  LD
--     PORT (
--        D,G    : IN  std_logic;
--        Q      : OUT std_logic);    
--   END COMPONENT;

-- spartan:
COMPONENT  LDCPE
  --synopsys synthesis_off
  GENERIC (INIT : bit);
  --synopsys synthesis_on
  PORT (
    CLR,D,G,GE,PRE    : IN  std_logic;
    Q                 : OUT std_logic);    
END COMPONENT;

  CONSTANT gnd_const  : std_logic := '0';
  SIGNAL   gnd_sig    : std_logic;
  CONSTANT high_const  : std_logic := '1';
  SIGNAL   high_sig    : std_logic;

BEGIN
  gnd_sig <= gnd_const;
  high_sig <= high_const;

-- spartan:  
  word_ld:  FOR i IN 0 TO 31 GENERATE 
    latch : LDCPE
      --synopsys synthesis_off
      GENERIC MAP (INIT => '0') -- Initial value of latch ('0' or '1')
      --synopsys synthesis_on
      PORT MAP (
        D   => d(i),
        G   => gate,
        Q   => q(i),
        CLR => gnd_sig, 
        GE  => high_sig, 
        PRE => gnd_sig
        );
  END GENERATE;

-- virtexe
--   word_ld:  FOR i IN 0 TO 31 GENERATE 
--     latch : LD
--       PORT MAP (
--         D  => d(i),
--         G  => gate,
--         Q  => q(i)
--         );
--   END GENERATE;

END structural;



