-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
--  Entity declaration for the Constants
--  (file bus_const32.vhd)
--------------------------------------------------------------------------
-- 
--------------------------------------------------------------------------
-- out_en1, out_en2 : low active
--------------------------------------------------------------------------
-- select(0 to 1) : "00"  q1 = X"0000_0000" when enabled 
--                  "01"  q1 = X"0000_0001" when enabled 
--                  "10"  q1 = X"0000_0004" when enabled 
--                        q2 = X"0000_0000" when enabled 
--------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;

ENTITY bus_const32 IS

  PORT (
    q1      : OUT dlx_word;
    q2      : OUT dlx_word;
    out_en1 : IN  std_logic;
    out_en2 : IN  std_logic;
    sel     : IN  std_logic_vector(0 TO 1));
END bus_const32;
