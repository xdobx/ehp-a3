-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
--   Architecture for the ALU   
--   (file alu_structural.vhd)
-- -----------------------------------------------------------

ARCHITECTURE structural OF alu IS

  COMPONENT  word_latch_1
    PORT (d             : IN   dlx_word;
      	  q             : OUT  dlx_word;
          gate          : IN   std_logic);
  END COMPONENT;

  
  COMPONENT alu_core
   PORT (stored_s1      : IN std_logic_vector (0 TO 31);
         stored_s2      : IN std_logic_vector (0 TO 31);
         alu_op         : IN alu_func_type;
         result         : OUT std_logic_vector (0 TO 31);
         negative       : OUT std_logic;
         zero           : OUT std_logic);
  END COMPONENT;
  
  SIGNAL        stored_s1, stored_s2    : dlx_word;

BEGIN 

  l1 : word_latch_1
    PORT MAP (d         => s1,
              q         => stored_s1,
              gate      => phi1);
      
  l2 : word_latch_1
    PORT MAP (d         => s2,
              q         => stored_s2,
              gate      => phi1);

--
-- for fast carry    
--
alucore : alu_core
  PORT MAP (stored_s1      => stored_s1,
            stored_s2      => stored_s2,
            alu_op         => func,
            result         => result,
            negative       => negative,
            zero           => zero);


END structural;









