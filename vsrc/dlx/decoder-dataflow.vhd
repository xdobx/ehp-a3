-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--  
--  (file decoder-dataflow.vhd)
--------------------------------------------------------------------------
-- on-chip address space 
--
-- 0000_0000 - 0000_01FF : on-chip system rom (4K Byte)
-- 1000_0000 - 1000_01FF : on-chip system ram (4K Byte)
-- 2000_0000             : 8 digit sevensegment display 
-------------------------------------------------------------------------
ARCHITECTURE dataflow OF decoder IS

--  SIGNAL all_select     : std_logic_vector(0 TO 2);
--  ALIAS  sel_rom        : std_logic IS all_select(0);
--  ALIAS  sel_ram        : std_logic IS all_select(1);
--  ALIAS  sel_display    : std_logic IS all_select(2);
 
BEGIN

  -- decode most significant address nibble to select component
--  all_select <=
--    "100" WHEN  enable  = '1' AND a_bus_hn = "0000" ELSE
--                                        -- system rom is selected
--    "010" WHEN  enable  = '1' AND a_bus_hn = "0001" ELSE
--                                        -- system ram is selected
--    "001" WHEN  enable  = '1' AND a_bus_hn = "0010" ELSE
--                                        -- word-display is selected
--    "000" WHEN  enable  = '0' ELSE      -- nothing selected
--    "XXX";                              -- undefined inputs !
enab_rom     <= '1' WHEN  enable  = '1' AND a_bus_hn = "0000" else '0'; 
                                        -- system rom is selected
enab_ram     <= '1' WHEN  enable  = '1' AND a_bus_hn = "0001" else '0';
                                        -- system ram is selected
enab_display <= '1' WHEN  enable  = '1' AND a_bus_hn = "0010" else '0';
                                        -- word-display is selected

--  dec_enab : PROCESS (enable, a_bus_hn)
--   BEGIN
--     if
--       ((enable  = '1') and (a_bus_hn = "0000")) then
--       all_select <=  "100";
--     elsif
--       ((enable  = '1') and (a_bus_hn = "0001")) then
--       all_select <=  "010";
--     elsif
--       ((enable  = '1') and (a_bus_hn = "0010")) then
--       all_select <=  "001";
--     else
--       all_select <=  "XXX";
--     end if;
--   END PROCESS dec_enab;
  
  -- connect internal signals to ports 
--  enab_rom      <= sel_rom;
--  enab_ram      <= sel_ram;
--  enab_display  <= sel_display;

END dataflow;









