-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Entity declaration for transparent latch (32-bit)
--  gate enable, direct output
--  
--  (file word_latch_e_1.vhd)
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--  gate_en : high active
--------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

--synopsys translate_off
LIBRARY UNISIM;
USE UNISIM.ALL;
--synopsys translate_on

-- library DLX;
use WORK.dlx_types.all;

entity word_latch_e_1 is

  port (d       : IN  dlx_word;
      	q       : OUT dlx_word;
	gate_en : IN  std_logic;
        gate    : IN  std_logic);
end word_latch_e_1;




