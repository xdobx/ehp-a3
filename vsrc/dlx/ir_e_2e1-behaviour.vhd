-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
-- 
-- -----------------------------------------------------------
--  Behavioural architecture of instruction register
--
--  file ir-behaviour.vhd
--------------------------------------------------------------
--
--------------------------------------------------------------
--  latch_en    : high active
--  immed_o1_en : low activ
--  immed_o2_en : low activ
--  immed_size  : '0'-> 16 bit / '1'-> 26 bit extended
--  immed_sign  : '0'-> zero / '1'-> sign extended
-------------------------------------------------------------

ARCHITECTURE behaviour OF ir_e_2e1 IS

-- virtexe:
--   COMPONENT  LDE
--     PORT (
--        D,G,GE : IN  std_logic;
--        Q      : OUT std_logic);    
--   END COMPONENT;

-- spartan:
  COMPONENT  LDCPE
    --synopsys synthesis_off
    GENERIC (INIT : bit);
    --synopsys synthesis_on
    PORT (
      CLR,D,G,GE,PRE    : IN  std_logic;
      Q                 : OUT std_logic);    
  END COMPONENT;
    
  SIGNAL instruction : dlx_word;
  CONSTANT gnd_const: std_logic := '0';
  SIGNAL   gnd_sig      : std_logic;
  
BEGIN
  gnd_sig <= gnd_const;

-- spartan:  
  word_lde:  FOR i IN 0 TO 31 GENERATE 
    latch_e : LDCPE
      --synopsys synthesis_off
      GENERIC MAP (INIT => '0') -- Initial value of latch ('0' or '1')
      --synopsys synthesis_on
      PORT MAP (
        D   => d(i),
        G   => gate,
        Q   => instruction(i),
        CLR => gnd_sig, 
        GE  => gate_en, 
        PRE => gnd_sig 
        );
  END GENERATE;

-- virtexe:
--   word_lde:  FOR i IN 0 TO 31 GENERATE 
--     latch_e : LDE
--       PORT MAP (
--         D  => d(i),
--         G  => gate,
--         GE => gate_en,
--         Q  => instruction(i)
--         );
--   END GENERATE;

  ir_output: PROCESS (instruction, immed_o1_en, immed_o2_en, immed_size)
  BEGIN 
    if immed_o1_en = '0' then
      if immed_size = '0' then
	if immed_sign = '0' then
	  immed_out1 <= sv_zext(instruction(16 to 31), 32);
	else
	  immed_out1 <= sv_sext(instruction(16 to 31), 32);
	end if;
      else
	if immed_sign = '0' then
	  immed_out1 <= sv_zext(instruction(6 to 31), 32);
	else
	  immed_out1 <= sv_sext(instruction(6 to 31), 32);
	end if;
      end if;
    else
      immed_out1 <= (others => 'Z');
    end if;

    if immed_o2_en = '0' then
      if immed_size = '0' then
	if immed_sign = '0' then
	  immed_out2 <= sv_zext(instruction(16 to 31), 32);
	else
	  immed_out2 <= sv_sext(instruction(16 to 31), 32);
	end if;
      else
	if immed_sign = '0' then
	  immed_out2 <= sv_zext(instruction(6 to 31), 32);
	else
	  immed_out2 <= sv_sext(instruction(6 to 31), 32);
	end if;
      end if;
    else
      immed_out2 <= (others => 'Z');
    end if;
  end process ir_output;

--     IF immed_o1_en = '0' THEN
--       IF immed_size = '0' THEN
--         immed_out1 <= sv_sext(instruction(16 TO 31), 32);
--       ELSE
--         immed_out1 <= sv_sext(instruction(6 TO 31), 32);
--       END IF;
--     ELSE
--       immed_out1 <= (OTHERS => 'Z');
--     END IF;
    
--     IF immed_o2_en = '0' THEN
--       IF immed_size = '0' THEN
--         immed_out2 <= sv_sext(instruction(16 TO 31), 32);
--       ELSE
--         immed_out2 <= sv_sext(instruction(6 TO 31), 32);
--       END IF;
--     ELSE
--       immed_out2 <= (OTHERS => 'Z');
--     END IF;
--   END PROCESS ir_output;
  
  ir_out <= instruction;
    
END behaviour;











