

ARCHITECTURE dataflow OF lcd_if IS
  
   COMPONENT disp_mux
    PORT (
      core_addr_out          : IN  dlx_address;  -- address out to memory
      core_data_in           : IN  dlx_word;     -- data in from memory
      core_data_out          : IN  dlx_word;     -- data out to memory
      display_latch_out      : IN  dlx_word;     -- result latch out
      s1_bus_display         : IN  dlx_word;     -- s1_bus for display only
      s2_bus_display         : IN  dlx_word;     -- s2_bus for display only
      dest_bus_display       : IN  dlx_word;     -- dest_bus for display only
      ir_reg_display         : IN  dlx_word;     -- ir output  for display only
      sel                    : IN  std_logic_vector(2 DOWNTO 0); -- select lines
      y                      : OUT dlx_word);     -- output 
  END COMPONENT;

   SIGNAL display  : std_logic_vector(31 DOWNTO 0); 
   SIGNAL phi1_2_lcd  : std_logic_vector(3 DOWNTO 0);  -- display two-phase clock

-- LCD-Initialization:  
   SIGNAL lcd_data_sig : lcd_array :=(
-- Zeile 0 
-- Spalte    0           1           2           3 
    (to_lcd('S'),to_lcd('t'),to_lcd('a'),to_lcd('t'),   -- Position 0,0  bis 0,3
--           4           5           6           7
     to_lcd('e'),to_lcd(':'),to_lcd(' '),to_lcd(' '),   -- Position 0,4  bis 0,7
--           8           9          10          11
     to_lcd(' '),to_lcd(' '),to_lcd(' '),to_lcd(' '),   -- Position 0,8  bis 0,11
--           12          13         14          15
     to_lcd(' '),to_lcd(' '),to_lcd(' '),to_lcd(' ')),  -- Position 0,12 bis 0,15
-- Zeile 1: (Spalten siehe Zeile 0)
    (to_lcd(' '),to_lcd(' '),to_lcd(' '),to_lcd(':'),   -- Position 1,0  bis 1,3
     to_lcd(' '),to_lcd(' '),to_lcd(' '),to_lcd(' '),   -- Position 1,4  bis 1,7
     to_lcd(' '),to_lcd(' '),to_lcd(' '),to_lcd(' '),   -- Position 1,8  bis 1,11
     to_lcd(' '),to_lcd(' '),to_lcd(' '),to_lcd(' '))); -- Position 1,12 bis 1,15
--SIGNAL state    : fsm_state_numbers;

BEGIN        

  phi1_2_lcd <= "00" & phi2 & phi1;          
  lcd_data <= lcd_data_sig;

--  Display multiplexer 
  muxdisp : disp_mux  
    PORT MAP (
      core_addr_out          => a_bus,
      core_data_in           => d_bus_in,
      core_data_out          => d_bus_out,
      display_latch_out      => disp_latch_out, 
      s1_bus_display         => s1_bus,
      s2_bus_display         => s2_bus, 
      dest_bus_display       => dest_bus, 
      ir_reg_display         => instr_out,
      sel                    => disp_sel,
      y                      => display); 
  
-- Display 8 digits (address or data)
  lcd_data_sig(1, 8) <= to_lcd(display(31 DOWNTO 28));
  lcd_data_sig(1, 9) <= to_lcd(display(27 DOWNTO 24));
  lcd_data_sig(1,10) <= to_lcd(display(23 DOWNTO 20));
  lcd_data_sig(1,11) <= to_lcd(display(19 DOWNTO 16));
  lcd_data_sig(1,12) <= to_lcd(display(15 DOWNTO 12));
  lcd_data_sig(1,13) <= to_lcd(display(11 DOWNTO  8));
  lcd_data_sig(1,14) <= to_lcd(display( 7 DOWNTO  4));
  lcd_data_sig(1,15) <= to_lcd(display( 3 DOWNTO  0));
-- Display two-phase-clock:
  
--   CONSTANT res_state_no       : fsm_state_numbers := "000001"; -- state 1
--   CONSTANT fetch_no           : fsm_state_numbers := "000010"; -- state 2
--   CONSTANT dec_pcinc4_ab_no   : fsm_state_numbers := "000011"; -- state 3
--   CONSTANT memory_no          : fsm_state_numbers := "000100"; -- state 4
--   CONSTANT load_w_1_no        : fsm_state_numbers := "000101"; -- state 5
--   CONSTANT load_w_2_no        : fsm_state_numbers := "000110"; -- state 6
--   CONSTANT store_w_1_no       : fsm_state_numbers := "000111"; -- state 7
--   CONSTANT store_w_2_no       : fsm_state_numbers := "001000"; -- state 8
--   CONSTANT br_eqz_no          : fsm_state_numbers := "001001"; -- state 9
--   CONSTANT branch_no          : fsm_state_numbers := "010000"; -- state 10
--   CONSTANT jump_no            : fsm_state_numbers := "010001"; -- state 11
--   CONSTANT sub_no             : fsm_state_numbers := "010010"; -- state 12
--   CONSTANT slt_no             : fsm_state_numbers := "010011"; -- state 13
--   CONSTANT set_to_1_no        : fsm_state_numbers := "010100"; -- state 14
--   CONSTANT set_to_0_no        : fsm_state_numbers := "010101"; -- state 15
--   CONSTANT wr_back_no         : fsm_state_numbers := "010110"; -- state 16
--   CONSTANT hlt_state_no       : fsm_state_numbers := "010111"; -- state 17
--   CONSTANT err_state_no       : fsm_state_numbers := "011000"; -- state 18
--   CONSTANT add_no             : fsm_state_numbers := "011001"; -- state 19
--   CONSTANT and_no             : fsm_state_numbers := "100000"; -- state 20
--   CONSTANT or_no              : fsm_state_numbers := "100001"; -- state 21
--   CONSTANT sll_no             : fsm_state_numbers := "100010"; -- state 22
--   CONSTANT srl_no             : fsm_state_numbers := "100011"; -- state 23

  lcd_states_mux : PROCESS (state) 
  BEGIN
    CASE state IS
      WHEN "000001" =>
        lcd_data_sig(0, 7) <= to_lcd('r');
        lcd_data_sig(0, 8) <= to_lcd('e');
        lcd_data_sig(0, 9) <= to_lcd('s');
        lcd_data_sig(0,10) <= to_lcd('_');
        lcd_data_sig(0,11) <= to_lcd('s');
        lcd_data_sig(0,12) <= to_lcd('t');
        lcd_data_sig(0,13) <= to_lcd('a');
        lcd_data_sig(0,14) <= to_lcd('t');
        lcd_data_sig(0,15) <= to_lcd('e');
      WHEN "000010" =>
        lcd_data_sig(0, 7) <= to_lcd('f');
        lcd_data_sig(0, 8) <= to_lcd('e');
        lcd_data_sig(0, 9) <= to_lcd('t');
        lcd_data_sig(0,10) <= to_lcd('c');
        lcd_data_sig(0,11) <= to_lcd('h');
        lcd_data_sig(0,12) <= to_lcd(' ');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "000011" =>
        lcd_data_sig(0, 7) <= to_lcd('d');
        lcd_data_sig(0, 8) <= to_lcd('e');
        lcd_data_sig(0, 9) <= to_lcd('c');
        lcd_data_sig(0,10) <= to_lcd('_');
        lcd_data_sig(0,11) <= to_lcd('p');
        lcd_data_sig(0,12) <= to_lcd('c');
        lcd_data_sig(0,13) <= to_lcd('_');
        lcd_data_sig(0,14) <= to_lcd('a');
        lcd_data_sig(0,15) <= to_lcd('b');
      WHEN "000100" =>
        lcd_data_sig(0, 7) <= to_lcd('m');
        lcd_data_sig(0, 8) <= to_lcd('e');
        lcd_data_sig(0, 9) <= to_lcd('m');
        lcd_data_sig(0,10) <= to_lcd('o');
        lcd_data_sig(0,11) <= to_lcd('r');
        lcd_data_sig(0,12) <= to_lcd('y');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "000101" =>
        lcd_data_sig(0, 7) <= to_lcd('l');
        lcd_data_sig(0, 8) <= to_lcd('o');
        lcd_data_sig(0, 9) <= to_lcd('a');
        lcd_data_sig(0,10) <= to_lcd('d');
        lcd_data_sig(0,11) <= to_lcd('_');
        lcd_data_sig(0,12) <= to_lcd('w');
        lcd_data_sig(0,13) <= to_lcd('_');
        lcd_data_sig(0,14) <= to_lcd('1');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "000110" =>
        lcd_data_sig(0, 7) <= to_lcd('l');
        lcd_data_sig(0, 8) <= to_lcd('o');
        lcd_data_sig(0, 9) <= to_lcd('a');
        lcd_data_sig(0,10) <= to_lcd('d');
        lcd_data_sig(0,11) <= to_lcd('_');
        lcd_data_sig(0,12) <= to_lcd('w');
        lcd_data_sig(0,13) <= to_lcd('_');
        lcd_data_sig(0,14) <= to_lcd('2');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "000111" =>
        lcd_data_sig(0, 7) <= to_lcd('s');
        lcd_data_sig(0, 8) <= to_lcd('t');
        lcd_data_sig(0, 9) <= to_lcd('o');
        lcd_data_sig(0,10) <= to_lcd('r');
        lcd_data_sig(0,11) <= to_lcd('e');
        lcd_data_sig(0,12) <= to_lcd('_');
        lcd_data_sig(0,13) <= to_lcd('w');
        lcd_data_sig(0,14) <= to_lcd('_');
        lcd_data_sig(0,15) <= to_lcd('1');
      WHEN "001000" =>
        lcd_data_sig(0, 7) <= to_lcd('s');
        lcd_data_sig(0, 8) <= to_lcd('t');
        lcd_data_sig(0, 9) <= to_lcd('o');
        lcd_data_sig(0,10) <= to_lcd('r');
        lcd_data_sig(0,11) <= to_lcd('e');
        lcd_data_sig(0,12) <= to_lcd('_');
        lcd_data_sig(0,13) <= to_lcd('w');
        lcd_data_sig(0,14) <= to_lcd('_');
        lcd_data_sig(0,15) <= to_lcd('2');
      WHEN "001001" =>
        lcd_data_sig(0, 7) <= to_lcd('b');
        lcd_data_sig(0, 8) <= to_lcd('r');
        lcd_data_sig(0, 9) <= to_lcd('_');
        lcd_data_sig(0,10) <= to_lcd('e');
        lcd_data_sig(0,11) <= to_lcd('q');
        lcd_data_sig(0,12) <= to_lcd('z');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "010000" =>
        lcd_data_sig(0, 7) <= to_lcd('b');
        lcd_data_sig(0, 8) <= to_lcd('r');
        lcd_data_sig(0, 9) <= to_lcd('a');
        lcd_data_sig(0,10) <= to_lcd('n');
        lcd_data_sig(0,11) <= to_lcd('c');
        lcd_data_sig(0,12) <= to_lcd('h');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "010001" =>
        lcd_data_sig(0, 7) <= to_lcd('j');
        lcd_data_sig(0, 8) <= to_lcd('u');
        lcd_data_sig(0, 9) <= to_lcd('m');
        lcd_data_sig(0,10) <= to_lcd('p');
        lcd_data_sig(0,11) <= to_lcd(' ');
        lcd_data_sig(0,12) <= to_lcd(' ');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "010010" =>
        lcd_data_sig(0, 7) <= to_lcd('s');
        lcd_data_sig(0, 8) <= to_lcd('u');
        lcd_data_sig(0, 9) <= to_lcd('b');
        lcd_data_sig(0,10) <= to_lcd(' ');
        lcd_data_sig(0,11) <= to_lcd(' ');
        lcd_data_sig(0,12) <= to_lcd(' ');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "010011" =>
        lcd_data_sig(0, 7) <= to_lcd('s');
        lcd_data_sig(0, 8) <= to_lcd('l');
        lcd_data_sig(0, 9) <= to_lcd('t');
        lcd_data_sig(0,10) <= to_lcd(' ');
        lcd_data_sig(0,11) <= to_lcd(' ');
        lcd_data_sig(0,12) <= to_lcd(' ');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "010100" =>
        lcd_data_sig(0, 7) <= to_lcd('s');
        lcd_data_sig(0, 8) <= to_lcd('e');
        lcd_data_sig(0, 9) <= to_lcd('t');
        lcd_data_sig(0,10) <= to_lcd('_');
        lcd_data_sig(0,11) <= to_lcd('t');
        lcd_data_sig(0,12) <= to_lcd('o');
        lcd_data_sig(0,13) <= to_lcd('_');
        lcd_data_sig(0,14) <= to_lcd('1');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "010101" =>
        lcd_data_sig(0, 7) <= to_lcd('s');
        lcd_data_sig(0, 8) <= to_lcd('e');
        lcd_data_sig(0, 9) <= to_lcd('t');
        lcd_data_sig(0,10) <= to_lcd('_');
        lcd_data_sig(0,11) <= to_lcd('t');
        lcd_data_sig(0,12) <= to_lcd('o');
        lcd_data_sig(0,13) <= to_lcd('_');
        lcd_data_sig(0,14) <= to_lcd('0');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "010110" =>
        lcd_data_sig(0, 7) <= to_lcd('w');
        lcd_data_sig(0, 8) <= to_lcd('r');
        lcd_data_sig(0, 9) <= to_lcd('_');
        lcd_data_sig(0,10) <= to_lcd('b');
        lcd_data_sig(0,11) <= to_lcd('a');
        lcd_data_sig(0,12) <= to_lcd('c');
        lcd_data_sig(0,13) <= to_lcd('k');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "010111" =>
        lcd_data_sig(0, 7) <= to_lcd('h');
        lcd_data_sig(0, 8) <= to_lcd('l');
        lcd_data_sig(0, 9) <= to_lcd('t');
        lcd_data_sig(0,10) <= to_lcd('_');
        lcd_data_sig(0,11) <= to_lcd('s');
        lcd_data_sig(0,12) <= to_lcd('t');
        lcd_data_sig(0,13) <= to_lcd('a');
        lcd_data_sig(0,14) <= to_lcd('t');
        lcd_data_sig(0,15) <= to_lcd('e');
      WHEN "011000" =>
        lcd_data_sig(0, 7) <= to_lcd('!');
        lcd_data_sig(0, 8) <= to_lcd(' ');
        lcd_data_sig(0, 9) <= to_lcd('e');
        lcd_data_sig(0,10) <= to_lcd('r');
        lcd_data_sig(0,11) <= to_lcd('r');
        lcd_data_sig(0,12) <= to_lcd('o');
        lcd_data_sig(0,13) <= to_lcd('r');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd('!');
      WHEN "011001" =>
        lcd_data_sig(0, 7) <= to_lcd('a');
        lcd_data_sig(0, 8) <= to_lcd('d');
        lcd_data_sig(0, 9) <= to_lcd('d');
        lcd_data_sig(0,10) <= to_lcd(' ');
        lcd_data_sig(0,11) <= to_lcd(' ');
        lcd_data_sig(0,12) <= to_lcd(' ');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "100000" =>
        lcd_data_sig(0, 7) <= to_lcd('a');
        lcd_data_sig(0, 8) <= to_lcd('n');
        lcd_data_sig(0, 9) <= to_lcd('d');
        lcd_data_sig(0,10) <= to_lcd('_');
        lcd_data_sig(0,11) <= to_lcd('1');
        lcd_data_sig(0,12) <= to_lcd(' ');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "100001" =>
        lcd_data_sig(0, 7) <= to_lcd('o');
        lcd_data_sig(0, 8) <= to_lcd('r');
        lcd_data_sig(0, 9) <= to_lcd('_');
        lcd_data_sig(0,10) <= to_lcd('1');
        lcd_data_sig(0,11) <= to_lcd(' ');
        lcd_data_sig(0,12) <= to_lcd(' ');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "100010" =>
        lcd_data_sig(0, 7) <= to_lcd('s');
        lcd_data_sig(0, 8) <= to_lcd('l');
        lcd_data_sig(0, 9) <= to_lcd('l');
        lcd_data_sig(0,10) <= to_lcd('_');
        lcd_data_sig(0,11) <= to_lcd('1');
        lcd_data_sig(0,12) <= to_lcd(' ');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN "100011" =>
        lcd_data_sig(0, 7) <= to_lcd('s');
        lcd_data_sig(0, 8) <= to_lcd('r');
        lcd_data_sig(0, 9) <= to_lcd('l');
        lcd_data_sig(0,10) <= to_lcd('_');
        lcd_data_sig(0,11) <= to_lcd('1');
        lcd_data_sig(0,12) <= to_lcd(' ');
        lcd_data_sig(0,13) <= to_lcd(' ');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd(' ');
      WHEN OTHERS  =>
        lcd_data_sig(0, 7) <= to_lcd('!');
        lcd_data_sig(0, 8) <= to_lcd(' ');
        lcd_data_sig(0, 9) <= to_lcd('e');
        lcd_data_sig(0,10) <= to_lcd('r');
        lcd_data_sig(0,11) <= to_lcd('r');
        lcd_data_sig(0,12) <= to_lcd('o');
        lcd_data_sig(0,13) <= to_lcd('r');
        lcd_data_sig(0,14) <= to_lcd(' ');
        lcd_data_sig(0,15) <= to_lcd('!');
    END CASE;
  END PROCESS;

  lcd_disp_source : PROCESS (disp_sel)
   BEGIN
     CASE disp_sel IS
-- display_latch_out:
       WHEN "000"  => --lcd_data_sig(1,0) <= to_lcd(' ');
                      lcd_data_sig(1,3) <= to_lcd('d');
                      lcd_data_sig(1,4) <= to_lcd('r');
                      lcd_data_sig(1,5) <= to_lcd(':');
-- core_data_out
       WHEN "001"  => --lcd_data_sig(1,0) <= to_lcd(' ');
                      lcd_data_sig(1,3) <= to_lcd('d');
                      lcd_data_sig(1,4) <= to_lcd('o'); 
                      lcd_data_sig(1,5) <= to_lcd(':');
-- core_data_in:               
       WHEN "010"  => --lcd_data_sig(1,0) <= to_lcd(' ');
                      lcd_data_sig(1,3) <= to_lcd('d');
                      lcd_data_sig(1,4) <= to_lcd('i'); 
                      lcd_data_sig(1,5) <= to_lcd(':');
-- core_addr_out      
       WHEN "011"  => --lcd_data_sig(1,0) <= to_lcd(' ');
                      lcd_data_sig(1,3) <= to_lcd('a');
                      lcd_data_sig(1,4) <= to_lcd('o'); 
                      lcd_data_sig(1,5) <= to_lcd(':');
-- s1_bus_display
       WHEN "100"  => --lcd_data_sig(1,0) <= to_lcd(' ');
                      lcd_data_sig(1,3) <= to_lcd('s');
                      lcd_data_sig(1,4) <= to_lcd('1'); 
                      lcd_data_sig(1,5) <= to_lcd(':');
-- s2_bus_display            
       WHEN "101"  => --lcd_data_sig(1,0) <= to_lcd(' ');
                      lcd_data_sig(1,3) <= to_lcd('s');
                      lcd_data_sig(1,4) <= to_lcd('2'); 
                      lcd_data_sig(1,5) <= to_lcd(':');
-- dest_bus_display     
       WHEN "110"  => --lcd_data_sig(1,0) <= to_lcd(' ');
                      lcd_data_sig(1,3) <= to_lcd('d');
                      lcd_data_sig(1,4) <= to_lcd('b'); 
                      lcd_data_sig(1,5) <= to_lcd(':');
-- ir_reg_display         
       WHEN OTHERS => --lcd_data_sig(1,0) <= to_lcd(' ');
                      lcd_data_sig(1,3) <= to_lcd('i');
                      lcd_data_sig(1,4) <= to_lcd('r');                           
                      lcd_data_sig(1,5) <= to_lcd(':');
     END CASE;
   END PROCESS lcd_disp_source;

END dataflow;
