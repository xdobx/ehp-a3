-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Entity declaration for the register file (32 32-bit-reg.)
--  one input, two outputs 
--
--  (file reg_fileram.vhd)
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  write_en : low active
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

--synopsys translate_off
LIBRARY UNISIM;
USE UNISIM.ALL;
--Library UNISIM;
--use UNISIM.vcomponents.all;

--synopsys translate_on

USE WORK.dlx_types.ALL;

ENTITY reg_fileram IS

 PORT (
   addr_out1  : IN  dlx_reg_addr;
   q1         : OUT dlx_word;
   addr_out2  : IN  dlx_reg_addr;
   q2         : OUT dlx_word;
   addr_in    : IN  dlx_reg_addr;
   d          : IN  dlx_word;
   phi2       : IN  std_logic; 
   write_en   : IN  std_logic);

END reg_fileram;










