-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
-- MOORE machine:
--
--            X >______            ________________
--                     \   ______>|output_logic    |_______> Y
--                      \ /       |________________|
--                       \        ________________
--                      / \_____>|next_state_logic|______     
--                     /________>|________________|      |
--                     |              _________          |
--                     |             |state    |         |
--                     |_____________|registers|<________|
--                                   |_________|
--
-- ------------------------------------------------------------
--  architecture of the finite state machine
--  (Moore machine / microcoded output style / two separated processes
--  for output_logic and next_state_logic)
--
--  file fsm_switch-dataflow.vhd
---------------------------------------------------------------

ARCHITECTURE dataflow OF fsm_switch IS

BEGIN

  -------------------------------------------------------------
  -- State switch
  -------------------------------------------------------------
  state_switch : PROCESS(phi1, reset)
  BEGIN
    --
    -- switch to next state with rising edge of phi1
    --
    IF (phi1'event AND phi1 = '1') THEN
      IF reset = '1' THEN
	current_state <= res_state;
      ELSE
	current_state <= next_state;
      END IF;
    END IF;
  END PROCESS state_switch;  
END dataflow;
