-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--  Structural architecture of the core of the DLXJ processor 
--  (only signed instructions)
--  
--  (file core-structural.vhd)
--------------------------------------------------------------------------

ARCHITECTURE structural OF core IS

  COMPONENT controller
    PORT (
      phi1       : IN  std_logic;                  -- two phase clock in
      phi2       : IN  std_logic;                  -- two phase clock in
      reset      : IN  std_logic;                  -- reset input
      halt       : IN  std_logic;                  -- halt input
      instr_in   : IN  dlx_word;                   -- instruction input
      alu_zero   : IN  std_logic;                  -- alu zero bit
      alu_neg    : IN  std_logic;                  -- alu negative bit
      --
      -- reg. address outputs
      --
      rs1_out    : OUT dlx_reg_addr;
      rs2_out    : OUT dlx_reg_addr;	  
      rd_out     : OUT dlx_reg_addr;	  
      --
      -- control outputs
      --
      s1_enab    : OUT std_logic_vector(0 TO 3);   -- select s1 source
      s2_enab    : OUT std_logic_vector(0 TO 4);   -- select s2_source
      dest_enab  : OUT std_logic_vector(0 TO 3);   -- select destination
      alu_op_sel : OUT alu_func_type;              -- alu operation
      const_sel  : OUT std_logic_vector(0 TO 1);   -- select const for s1
      rf_op_sel  : OUT std_logic_vector(0 TO 1);   -- sel. reg file operation
      immed_sel  : OUT std_logic_vector(0 TO 1);   -- select immediate from ir
      mem_ctrl   : OUT std_logic_vector(0 TO 5);   -- memory control lines
      state_number : OUT fsm_state_numbers         -- state numbers
      );
  END COMPONENT;

  COMPONENT datapath 
    PORT (
      uc_rf_data_out2   : OUT dlx_word;         -- for uc read only
      s1_bus            : OUT dlx_word;         -- s1_bus for display only
      s2_bus            : OUT dlx_word;         -- s2_bus for display only
      dest_bus          : OUT dlx_word;         -- dest_bus for display only
      data_in           : IN  dlx_word;         -- data in from pads
      data_out          : OUT dlx_word;         -- data_out to pads
      addr_out          : OUT dlx_word;         -- addr. out to pads
      instr_out         : OUT dlx_word;         -- instr. reg. content
      alu_zero          : OUT std_logic;                     
      alu_neg           : OUT std_logic;
      --
      -- register file addr. inputs
      --
      rf_addr_out1      : IN dlx_reg_addr;
      rf_addr_out2      : IN dlx_reg_addr;
      rf_addr_in        : IN dlx_reg_addr;
      --
      -- control inputs
      --
      a_latch_en        : IN std_logic;         
      a_out_en          : IN std_logic;         
      b_latch_en        : IN std_logic;         
      b_out_en          : IN std_logic;         
      c_latch_en        : IN std_logic;         
      rf_wr_en          : IN std_logic;         
      
      mar_latch_en      : IN std_logic;         
      mar_out1_en       : IN std_logic;         
      mdr_latch_en      : IN std_logic;         
      mdr_out1_en       : IN std_logic;         
      
      pc_latch_en       : IN std_logic;         
      pc_out_en         : IN std_logic;         

      ir_latch_en       : IN std_logic;         
      ir_immed_o1_en    : IN std_logic;         
      ir_immed_o2_en    : IN std_logic;         
      ir_immed_size     : IN std_logic;         
      ir_immed_sign     : IN std_logic;         

      alu_func          : IN alu_func_type;
      a_mux_sel         : IN std_logic;         
      d_mux_sel         : IN std_logic;         
      const_o1_en       : IN std_logic;         
      const_o2_en       : IN std_logic;         
      const_sel         : IN std_logic_vector(0 TO 1); 
      phi1              : IN std_logic;
      phi2              : IN std_logic);        

  END COMPONENT;

  --
  -- signals to connect
  --
  SIGNAL dp_instr_out     : dlx_word; 
  SIGNAL dp_alu_zero      : std_logic;                     
  SIGNAL dp_alu_neg       : std_logic;
  SIGNAL c_s1_enab        : std_logic_vector(0 TO 3);  -- select s1 source
  SIGNAL c_s2_enab        : std_logic_vector(0 TO 4);  -- select s2_source
  SIGNAL c_dest_enab      : std_logic_vector(0 TO 3);  -- select destination
  SIGNAL c_alu_op_sel     : alu_func_type;             -- alu operation
  SIGNAL c_const_sel      : std_logic_vector(0 TO 1);  -- select const for s1
  SIGNAL c_rf_op_sel      : std_logic_vector(0 TO 1);  -- sel. reg file operation
  SIGNAL c_immed_sel      : std_logic_vector(0 TO 1);  -- select immed. from ir
  SIGNAL c_mem_ctrl       : std_logic_vector(0 TO 5);  -- memory control lines
  SIGNAL c_rs1_out        : dlx_reg_addr;
  SIGNAL c_rs2_out        : dlx_reg_addr;  
  SIGNAL c_rd_out         : dlx_reg_addr;
  SIGNAL rf_addr_out2_sig : dlx_reg_addr;
  SIGNAL uc_rf_data_out2_sig  : dlx_word;         -- for uc read only
  SIGNAL a_mux_sel_sig    : std_logic; 
BEGIN        
  
  ctrl : controller
    PORT MAP (
      phi1              => phi1,
      phi2              => phi2,
      halt              => halt,
      reset             => reset,
      instr_in          => dp_instr_out,
      alu_zero          => dp_alu_zero,
      alu_neg           => dp_alu_neg,
      rs1_out           => c_rs1_out,
      rs2_out           => c_rs2_out,
      rd_out            => c_rd_out,
      s1_enab           => c_s1_enab,
      s2_enab           => c_s2_enab,
      dest_enab         => c_dest_enab,
      alu_op_sel        => c_alu_op_sel,
      const_sel         => c_const_sel,
      rf_op_sel         => c_rf_op_sel,
      immed_sel         => c_immed_sel,
      mem_ctrl          => c_mem_ctrl,
      state_number      => state_number
      );
  
  dpath : datapath
    PORT MAP (
      uc_rf_data_out2   => uc_rf_data_out2_sig, -- for uc read only
      s1_bus            => s1_disp,             -- s1_bus for display only
      s2_bus            => s2_disp,             -- s2_bus for display only
      dest_bus          => dest_disp,           -- dest_bus for display only
      data_in           => d_bus_in,
      data_out          => d_bus_out,
      addr_out          => a_bus,
      instr_out         => dp_instr_out,
      alu_zero          => dp_alu_zero,
      alu_neg           => dp_alu_neg,
      --
      -- reg. file addr. inputs
      --
      rf_addr_out1      => c_rs1_out,
      rf_addr_out2      => rf_addr_out2_sig,
      rf_addr_in        => c_rd_out,
      --
      -- s1_enab(0 to 3) : select source for s1 bus
      -- |a|mdr|immed|consta| (b/pc/mar not connected to s1 bus)
      --
      a_out_en          => c_s1_enab(0),
      mdr_out1_en       => c_s1_enab(1),
      ir_immed_o1_en    => c_s1_enab(2),
      const_o1_en       => c_s1_enab(3),
      --
      -- s2_enab(0 to 4) : select source for s2 bus
      -- |b|immed|pc|mar|constb|  (a/mdr/ not connected to s2 bus)
      --
      b_out_en          => c_s2_enab(0),
      ir_immed_o2_en    => c_s2_enab(1),  
      pc_out_en         => c_s2_enab(2),
      mar_out1_en       => c_s2_enab(3),
      const_o2_en       => c_s2_enab(4),       
      --	      
      -- dest_enab(0 to 3) : select destination from dest bus
      -- |c|pc|mar|mdr|
      --
      c_latch_en        => c_dest_enab(0),
      pc_latch_en       => c_dest_enab(1),
      mar_latch_en      => c_dest_enab(2),
      mdr_latch_en      => c_dest_enab(3),
      --
      -- alu_func : select alu operation
      --
      alu_func          => c_alu_op_sel,
      --
      -- const_sel (select constant for s2 bus)
      --
      const_sel         => c_const_sel,
      --
      -- immed_sel : select immediate type 
      --
      ir_immed_size     => c_immed_sel(0),
      ir_immed_sign     => c_immed_sel(1),
      --
      -- rf_op_sel(0 to 1) : register-file operation
      -- |ab<-rf|rf<-c|mux_sel|set31||
      --
      a_latch_en        => c_rf_op_sel(0),
      b_latch_en        => c_rf_op_sel(0),
      rf_wr_en          => c_rf_op_sel(1), 
      --
      -- mem_ctrl(0 to 5) : all control signal for memory interface etc.
      -- |ir_en|a_mux|d_mux|enable(0 to 3)|rw|error|
      --
      ir_latch_en       => c_mem_ctrl(0),
      a_mux_sel         => a_mux_sel_sig,
      d_mux_sel         => c_mem_ctrl(2),
      -- two phase clock
      phi1              => phi1,
      phi2              => phi2
      );
  
  -- for register dump only:
  a_mux_sel_sig <= c_mem_ctrl(1) WHEN uc_a_mux_sel(1) = '0' ELSE uc_a_mux_sel(0);

  --
  -- connect memory control lines and error line
  --
  enable        <= c_mem_ctrl(3);
  rw            <= c_mem_ctrl(4);
  error         <= c_mem_ctrl(5);
  --
  -- instruction display
  --
  instr_disp    <= dp_instr_out; -- for display only

  -- for RF dump only:
  
     -- RF-address multiplexor:

  rf_addr_out2_sig <= c_rs2_out WHEN uc_rf_addr_out2_sel = '0' ELSE uc_rf_addr_out2;
  uc_rf_data_out2  <= uc_rf_data_out2_sig;

END structural;



























