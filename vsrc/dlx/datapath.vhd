-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--  Entity declaration for the datapath
--
--  (file datapath.vhd)
--------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;

ENTITY datapath IS
  PORT (
    uc_rf_data_out2     : OUT dlx_word;         -- for uc read only
    s1_bus              : OUT dlx_word;         -- s1_bus for display only
    s2_bus              : OUT dlx_word;         -- s2_bus for display only
    dest_bus            : OUT dlx_word;         -- dest_bus for display only
    data_in             : IN  dlx_word;         -- data in from pads
    data_out            : OUT dlx_word;         -- data_out to pads
    addr_out            : OUT dlx_word;         -- addr. out to pads
    instr_out           : OUT dlx_word;         -- instr. reg. content
    alu_zero            : OUT std_logic;
    alu_neg             : OUT std_logic;
--    mar_adr_msb         : OUT std_logic;        --  msb of mar
    --
    -- register file addr. inputs
    --
    rf_addr_out1        : IN dlx_reg_addr;
    rf_addr_out2        : IN dlx_reg_addr;
    rf_addr_in          : IN dlx_reg_addr;
    --
    -- control inputs
    --
    a_latch_en          : IN std_logic;          -- high active
    a_out_en            : IN std_logic;          -- low  active
    b_latch_en          : IN std_logic;          -- high active
    b_out_en            : IN std_logic;          -- low  active
    c_latch_en          : IN std_logic;          -- high active
    rf_wr_en            : IN std_logic;          -- low  active
    mar_latch_en        : IN std_logic;          -- high active
    mar_out1_en         : IN std_logic;          -- low  active
    mdr_latch_en        : IN std_logic;          -- high active
    mdr_out1_en         : IN std_logic;          -- low  active
    pc_latch_en         : IN std_logic;          -- high active
    pc_out_en           : IN std_logic;          -- low  active
    ir_latch_en         : IN std_logic;          -- high active
    ir_immed_o1_en      : IN std_logic;          -- low  active
    ir_immed_o2_en      : IN std_logic;          -- low  active
    ir_immed_size       : IN std_logic;          -- '0'-> 16 bit/'1'-> 26 bit
    ir_immed_sign       : IN std_logic;          -- '0'-> zero /'1'-> sign
    alu_func            : IN alu_func_type;
    a_mux_sel           : IN std_logic;          -- '0' : pc->out/
                                                 -- '1' : mar->out
    d_mux_sel           : IN std_logic;          -- '0' : dest->mdr/
                                                 -- '1' : mem->mdr
    const_o1_en         : IN std_logic;          -- enable constant for s1
                                                 -- (low act.)
    const_o2_en         : IN std_logic;          -- enable constant for s2
                                                 -- (low act.)
    const_sel           : IN std_logic_vector(0 TO 1); -- select constant
    --
    -- clock input
    --
   phi1                 : IN std_logic;
   phi2                 : IN std_logic);         -- global clock, write data
                                                 -- into reg_fileram
END datapath;






