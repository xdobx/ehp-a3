-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  
--  (file testbench.vhd)
-- -----------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;
USE IEEE.std_logic_textio.ALL;
USE std.textio.ALL;

LIBRARY IEEE_EXTD;
USE     IEEE_EXTD.stdl1164_vector_arithmetic.all;

USE WORK.dlx_types.all;
USE WORK.control_types.all;

ENTITY tb IS
    GENERIC (
      t_ph_init_clk     : time := 100 ns;
      t_del_sio_clk     : time := 25 ns;
      t_ph_q_clk        : time := 10 ns
--      t_enab_q_clk      : time := 87 ns
--      t_ph_init_clk     : time := 200 ns;
--      t_del_sio_clk     : time := 40 ns;
--      t_ph_q_clk        : time := 25 ns;
--      t_enab_q_clk      : time := 87 ns
      );
END tb ;




