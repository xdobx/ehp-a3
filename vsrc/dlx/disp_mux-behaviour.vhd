-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Architecture of the bus multiplexer to display all
--  processor busses
--  
--  (file disp_mux-behaviour.vhd)
-- -----------------------------------------------------------

ARCHITECTURE behaviour OF disp_mux IS
  
BEGIN

-- ----------------------------------------------
--     with use of combinatorial logic (slow):
-- ---------------------------------------------- 

--   disp_source : PROCESS (sel, display_latch_out, core_data_out,
--                             core_data_in, core_addr_out, s1_bus_display,
--                             s2_bus_display, dest_bus_display, ir_reg_display)
   disp_source : PROCESS (sel)
   BEGIN
     CASE sel IS
       WHEN "000"       => y <= display_latch_out;
       WHEN "001"       => y <= core_data_out;
       WHEN "010"       => y <= core_data_in;
       WHEN "011"       => y <= core_addr_out;
  
       WHEN "100"       => y <= s1_bus_display;
       WHEN "101"       => y <= s2_bus_display;
       WHEN "110"       => y <= dest_bus_display;
       WHEN OTHERS      => y <= ir_reg_display;                
     END CASE;
   END PROCESS disp_source;


-- ----------------------------------------------
-- with use of three-state buffers (fast):
-- ----------------------------------------------
  
--   y <= display_latch_out WHEN sel = "000" ELSE (OTHERS => 'Z');
--   y <= core_data_out     WHEN sel = "001" ELSE (OTHERS => 'Z');
--   y <= core_data_in      WHEN sel = "010" ELSE (OTHERS => 'Z');
--   y <= core_addr_out     WHEN sel = "011" ELSE (OTHERS => 'Z');
  
--   y <= s1_bus_display    WHEN sel = "100" ELSE (OTHERS => 'Z');
--   y <= s2_bus_display    WHEN sel = "101" ELSE (OTHERS => 'Z');
--   y <= dest_bus_display  WHEN sel = "110" ELSE (OTHERS => 'Z');
--   y <= ir_reg_display    WHEN sel = "111" ELSE (OTHERS => 'Z');
  
END behaviour;
