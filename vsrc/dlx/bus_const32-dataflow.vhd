-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
--   Dataflow architecture for 32 bit bus constant
--  (file bus_const32-dataflow.vhd)
--------------------------------------------------------------

ARCHITECTURE dataflow OF bus_const32 IS 

  SIGNAL tmp1 : dlx_word;
  SIGNAL tmp2 : dlx_word;

BEGIN

  tmp1 <= "00000000000000000000000000000000" WHEN sel = "00" ELSE   -- 0
          -- used in states reset_1, set_to_0
          "00000000000000000000000000000001" WHEN sel = "01" ELSE   -- 1
          -- used in state set_to_1, 
          "00000000000000000000000000000100";                       -- 4
	  -- used in state decode_pcincr_ab,

  tmp2 <= "00000000000000000000000000000000";   -- 0
          -- used in state br_eqz

  q1 <= tmp1 WHEN out_en1 = '0' ELSE
        "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
  q2 <= tmp2 WHEN out_en2 = '0' ELSE
        "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";

END dataflow;




