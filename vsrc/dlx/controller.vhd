-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
--  Entity declaration for controller
--  (file controller.vhd)
--------------------------------------------------------------
  --
  -- source for s1 bus (low activ signals)
  -- |a|mdr|immed|consta| (b, pc, mar not connected to s1 bus)
  --
  -- source for s2 bus (low activ signals)
  -- |b|immed|pc|mar|constb|  (a, mdr not connected to s2 bus)
  --
  -- destination from dest bus (low activ signals)
  -- |c|pc|mar|mdr|
  --
  -- select constant 
  -- |sel_const(0:1)|
  --
  -- select immediate
  -- |size_imm|
  --
  -- register file operation
  -- (ab<-rf, rf<-c)
  -- |ab_rf|rf_c|
  --
  -- alu operation
  --
  -- memory control signals (ir_en : low active)
  -- |ir_en|addr_mux|mdr_mux|enable|r/w|error|
  --
--------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;

USE WORK.dlx_instructions.ALL;
USE WORK.control_types.ALL;

ENTITY controller IS  
  PORT (phi1, phi2      : IN std_logic;  -- two phase clock in
	reset           : IN std_logic;  -- reset input
	halt            : IN std_logic;  -- halt input
	instr_in        : IN dlx_word;   -- instruction input
	alu_zero        : IN std_logic;  -- alu zero bit
	alu_neg         : IN std_logic;  -- alu negative bit
        --
	-- reg. address outputs
	--
	rs1_out         : OUT dlx_reg_addr;
	rs2_out         : OUT dlx_reg_addr;	  
	rd_out          : OUT dlx_reg_addr;	  
        --
	-- control outputs
	--
	s1_enab      : OUT std_logic_vector(0 TO 3);    -- select s1 source
	s2_enab      : OUT std_logic_vector(0 TO 4);    -- select s2_source
	dest_enab    : OUT std_logic_vector(0 TO 3);    -- select destination
	alu_op_sel   : OUT alu_func_type;               -- alu operation
	const_sel    : OUT std_logic_vector(0 TO 1);    -- select const for s1
	rf_op_sel    : OUT std_logic_vector(0 TO 1);    -- select reg file op
	immed_sel    : OUT std_logic_vector(0 TO 1);    -- select immed from ir
	mem_ctrl     : OUT std_logic_vector(0 TO 5);    -- memory control lines
        state_number : OUT   fsm_state_numbers          -- state numbers
        );
END controller;





