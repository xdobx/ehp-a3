-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Entity declaration for the next state logic
--
--  file fsm_next.vhd
--------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;

ENTITY fsm_next IS 
  PORT (
    reset         : IN 	std_logic;      -- reset input
    halt          : IN 	std_logic;      -- halt input
    alu_zero	  : IN 	std_logic;	-- alu zero bit
    alu_neg	  : IN 	std_logic;	-- alu negative bit
    dec_1_in	  : IN 	fsm_states;	-- input from ir_decode_1
    dec_2_in	  : IN 	fsm_states;	-- input from ir_decode_2
    current_state : IN	fsm_states;	-- the current state
    next_state    : OUT	fsm_states);	-- the next state

END fsm_next;











