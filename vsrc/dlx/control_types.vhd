-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Mar 2010 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS Synplify
--   Target architecture:
--        XILINX Spartan-3AN
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--  Package for the data types and microcode used in the
--  controller and the alu core 
--  
--  (file control_types.vhd)
-- -----------------------------------------------------------

LIBRARY IEEE;
LIBRARY SYNOPSYS;
USE IEEE.std_logic_1164.ALL;
--USE SYNOPSYS.attributes.ALL;
--library synplify;
--use synplify.attributes.all;

PACKAGE control_types IS
  
  TYPE alu_func_type  IS
    (alu_pass_s1,
     alu_pass_s2,
     alu_s1_add_s2,  -- used in states dec_pcinc4_ab and add ! 
     alu_s1_sub_s2,
     alu_s1_and_s2,
     alu_s1_or_s2,
     alu_sll_s1_s2,
     alu_srl_s1_s2,
     alu_dcare);
  ATTRIBUTE enum_encoding : string;
  ATTRIBUTE enum_encoding OF alu_func_type :
    TYPE IS ("000 001 010 011 100 101 110 111 000");

  TYPE fsm_states IS (
    res_state,
    fetch,
    dec_pcinc4_ab,
    memory,
    load_w_1,
    load_w_2,
    store_w_1,
    store_w_2,
    br_eqz,
    branch,
    jump,
    sub,
    slt,
    set_to_1,
    set_to_0,
    wr_back,
    hlt_state,
    err_state,
    add,    --add !
    and_1,
    or_1,
    sll_1,
    srl_1
   );
    
  SUBTYPE  fsm_state_numbers IS std_logic_vector(5 DOWNTO 0);

  CONSTANT res_state_no         : fsm_state_numbers := "000001"; -- state 1
  CONSTANT fetch_no             : fsm_state_numbers := "000010"; -- state 2
  CONSTANT dec_pcinc4_ab_no     : fsm_state_numbers := "000011"; -- state 3
  CONSTANT memory_no            : fsm_state_numbers := "000100"; -- state 4
  CONSTANT load_w_1_no          : fsm_state_numbers := "000101"; -- state 5
  CONSTANT load_w_2_no          : fsm_state_numbers := "000110"; -- state 6
  CONSTANT store_w_1_no         : fsm_state_numbers := "000111"; -- state 7
  CONSTANT store_w_2_no         : fsm_state_numbers := "001000"; -- state 8
  CONSTANT br_eqz_no            : fsm_state_numbers := "001001"; -- state 9
  CONSTANT branch_no            : fsm_state_numbers := "010000"; -- state 10
  CONSTANT jump_no              : fsm_state_numbers := "010001"; -- state 11
  CONSTANT sub_no               : fsm_state_numbers := "010010"; -- state 12
  CONSTANT slt_no               : fsm_state_numbers := "010011"; -- state 13
  CONSTANT set_to_1_no          : fsm_state_numbers := "010100"; -- state 14
  CONSTANT set_to_0_no          : fsm_state_numbers := "010101"; -- state 15
  CONSTANT wr_back_no           : fsm_state_numbers := "010110"; -- state 16
  CONSTANT hlt_state_no         : fsm_state_numbers := "010111"; -- state 17
  CONSTANT err_state_no         : fsm_state_numbers := "011000"; -- state 18
  CONSTANT add_no               : fsm_state_numbers := "011001"; -- state 19
  CONSTANT and_no               : fsm_state_numbers := "100000"; -- state 20
  CONSTANT or_no                : fsm_state_numbers := "100001"; -- state 21
  CONSTANT sll_no               : fsm_state_numbers := "100010"; -- state 22
  CONSTANT srl_no               : fsm_state_numbers := "100011"; -- state 23
  
  
-- source for s1 bus (low activ signals)
-- |a|mdr|immed|bus_const| (b/pc/mar not connected to s1 bus)
--                                                   0123
  CONSTANT s1_a       : std_logic_vector(0 TO 3) := "0111";
  CONSTANT s1_mdr     : std_logic_vector(0 TO 3) := "1011";
  CONSTANT s1_immed   : std_logic_vector(0 TO 3) := "1101";
  CONSTANT s1_const   : std_logic_vector(0 TO 3) := "1110";
  CONSTANT s1_none    : std_logic_vector(0 TO 3) := "1111";-- don't care not
  --       a_out_en -> A (RF-latch) output enable ___||||  -- allowed (only
  --                                                  |||  -- one driver!)
  --    mdr_out1_en -> MDR output 1 enable to s1 _____|||
  --                                                   ||
  -- ir_immed_o1_en -> IR output 1 enable to s1 _______||
  --                                                    |
  --    const_o1_en -> bus_const output 1 enable to s1 _|
  --------------------------------------------------------
  --
  -- source for s2 bus
  -- |b|immed|Y|pc|mar|bus_const|
  -- (a/mdr not connected to s2 bus)
  -- Y : b or immed, depends on instruction type
  -- all output enable signals are low active,
  -- activated with phi1 positive edge (fsm_next)
-- s2_enab:                                         012345
  CONSTANT s2_b      : std_logic_vector(0 TO 5) := "011111";  
  CONSTANT s2_immed  : std_logic_vector(0 TO 5) := "101111";  
  CONSTANT s2_Y      : std_logic_vector(0 TO 5) := "110111";  
  CONSTANT s2_pc     : std_logic_vector(0 TO 5) := "111011";
  CONSTANT s2_mar    : std_logic_vector(0 TO 5) := "111101";
  CONSTANT s2_const  : std_logic_vector(0 TO 5) := "111110";
  CONSTANT s2_none   : std_logic_vector(0 TO 5) := "111111"; -- don't care not 
  --       b_out_en -> B output enable to s2 _______||||||   -- allowed (only
  --                                                 |||||   -- one driver!)
  -- ir_immed_o2_en -> IR output 2 enable to s2 _____|||||
  --                                                  ||||
  -- b_out_en or ir_immed_o2_en (dep. on instr.type) _||||
  --                                                   |||
  --       pc_out_en -> PC output 1 enable to s2 ______|||
  --                                                    ||
  --     mar_out1_en -> MAR output 1 enable to s2 ______||
  --                                                     |
  --     const_o2_en -> bus_const output 2 enable to s2 _|
  ---------------------------------------------------------
  --
  -- destination from dest bus
  -- all input enable signals are high active
  -- all inputs are gated with phi2 high
  -- dest_enab:                                      0123 
  CONSTANT dest_c     : std_logic_vector(0 TO 3) := "1000";
  CONSTANT dest_pc    : std_logic_vector(0 TO 3) := "0100";
  CONSTANT dest_mar   : std_logic_vector(0 TO 3) := "0010";
  CONSTANT dest_mdr   : std_logic_vector(0 TO 3) := "0001";
  CONSTANT dest_res   : std_logic_vector(0 TO 3) := "0111"; -- don't care not
  CONSTANT dest_none  : std_logic_vector(0 TO 3) := "0000"; -- allowed
  --     c_latch_en -> C enable (RF-input latch C)___||||
  --                                                  |||
  --    pc_latch_en -> PC enable _____________________|||
  --                                                   ||
  --   mar_latch_en -> MAR enable _____________________||
  --                                                    |
  --   mdr_latch_en -> MDR enable ______________________|
  ---------------------------------------------------------
  --
  -- select constant 
  -- const_sel:                                       01
  CONSTANT const_00    : std_logic_vector(0 TO 1) := "00";
  CONSTANT const_01    : std_logic_vector(0 TO 1) := "01";
  CONSTANT const_04    : std_logic_vector(0 TO 1) := "10";
  CONSTANT const_dcare : std_logic_vector(0 TO 1) := "--";
  --        const_sel -> "00": tmp1 <= 0x00000000     ||
  --                     "01": tmp1 <= 0x00000001     ||
  --                     "10": tmp1 <= 0x00000004_____||
  -- bus constant function:
  --                           tmp2 <= 0x00000000                        
  --  q1 <= tmp1 WHEN out_en1 = '0' ELSE 0xZZZZZZZZ
  --  q2 <= tmp2 WHEN out_en2 = '0' ELSE 0xZZZZZZZZ
  --------------------------------------------------------
  --
  -- select immediate
  -- immed_sel:
  constant imm_s16   : std_logic_vector(0 to 1) := "01";
  constant imm_u16   : std_logic_vector(0 to 1) := "00";
  constant imm_s26   : std_logic_vector(0 to 1) := "11";
  constant imm_u26   : std_logic_vector(0 to 1) := "10";
  constant imm_dcare : std_logic_vector(0 to 1) := "--";
  -- ir_immed_size -> '0': 16 bit                   ||
  --                  '1': 26 bit        ___________||
  -- ir_immed_sign -> '0': zero extended             |
  --                  '1': sign extended ____________|
  ------------------------------------------------------
  --
  -- register file operation
  -- rf_op_sel:                                       01
  CONSTANT rfop_none   : std_logic_vector(0 TO 1) := "01";
  CONSTANT rfop_ab_rf  : std_logic_vector(0 TO 1) := "11";
  CONSTANT rfop_rf_c   : std_logic_vector(0 TO 1) := "00";
  --     a_latch_en -> A latch enable, high active    ||
  --                   (RF-output latch A)            ||
  --                   gated with phi2 high           ||
  --     b_latch_en -> B latch enable, high active,   ||
  --                   (RF-output latch B)            ||
  --                   gated with phi2 high __________||
  --                                                   |
  --       rf_wr_en -> RF write enable, low active     |
  --                   clocked with phi2 neg. edge ____|
  --------------------------------------------------------
  --
  -- memory control signals 
  -- mem_ctrl:                                      012345
  CONSTANT mem_none  : std_logic_vector(0 TO 5) := "000010";
  CONSTANT mem_fetch : std_logic_vector(0 TO 5) := "100110";
  CONSTANT mem_ldst  : std_logic_vector(0 TO 5) := "010000";
  CONSTANT mem_lw    : std_logic_vector(0 TO 5) := "011110";
  CONSTANT mem_sw    : std_logic_vector(0 TO 5) := "010100";
  CONSTANT mem_error : std_logic_vector(0 TO 5) := "000011";  
  --   ir_latch_en -> IR latch enable, high active  ||||||
  --                  gated with phi2 high    ______||||||
  --                                                 |||||
  --  addr_mux_sel -> '0': Memory address <= PC      |||||
  --                  '1': Memory address <= MAR ____|||||
  --                                                  ||||
  --   mdr_mux_sel -> '0': MDR <= Dest. bus           ||||
  --                  '1': MDR <= Memory _____________||||
  --                                                   |||
  --        enable -> Memory access, high active       |||
  --                  clocked with phi1 pos. edge _____|||
  --                                                    ||
  --            rw -> '0': Memory write                 ||
  --                  '1': Memory read _________________||
  --                                                     |
  --         error -> error pad signal, high active _____|
  
END control_types;

