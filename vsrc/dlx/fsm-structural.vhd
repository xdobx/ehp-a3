-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
-- MOORE machine:
--
--            X >______            ________________
--                     \   ______>|output_logic    |_______> Y
--                      \ /       |________________|
--                       \        ________________
--                      / \_____>|next_state_logic|______     
--                     /________>|________________|      |
--                     |              _________          |
--                     |             |state    |         |
--                     |_____________|registers|<________|
--                                   |_________|
--
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  architecture of the finite state machine
--  (Moore machine / microcoded output style / two separated processes
--  for output_logic and next_state_logic)
--
--  file fsm-structural.vhd
-- ------------------------------------------------------------

ARCHITECTURE structural OF fsm IS

  COMPONENT fsm_switch 
    PORT (
      phi1		: IN  std_logic;	-- clock input
      reset	        : IN  std_logic;	-- reset input
      next_state        : IN  fsm_states;	-- the internal next state
      current_state     : OUT fsm_states);	-- the current state
  END COMPONENT;
    
  COMPONENT fsm_next 
    PORT (
      reset             : IN  std_logic;	-- reset input
      halt              : IN  std_logic;	-- halt input
      alu_zero          : IN  std_logic;	-- alu zero bit
      alu_neg           : IN  std_logic;	-- alu negative bit
      dec_1_in          : IN  fsm_states;	-- input from instr.dec.1
      dec_2_in          : IN  fsm_states;	-- input from instr.dec.2
      current_state     : IN  fsm_states;	-- the current state
      next_state        : OUT fsm_states);      -- the next state
  END COMPONENT;

  COMPONENT fsm_output 
    PORT (
      current_state : IN fsm_states;		 -- the current state
      s1_enab	 : OUT std_logic_vector(0 TO 3); -- select s1 source
      s2_enab	 : OUT std_logic_vector(0 TO 5); -- select s2_source
      dest_enab	 : OUT std_logic_vector(0 TO 3); -- select destination
      alu_op_sel : OUT alu_func_type;            -- alu operation
      const_sel	 : OUT std_logic_vector(0 TO 1); -- select const for s1
      rf_op_sel	 : OUT std_logic_vector(0 TO 1); -- select reg file op.
      immed_sel	 : OUT std_logic_vector(0 TO 1); -- select immed. from ir
      mem_ctrl   : OUT std_logic_vector(0 TO 5); -- mem. control lines
      state_number : OUT   fsm_state_numbers);   -- state numbers
  END COMPONENT;

  SIGNAL current_state_in       : fsm_states;
  SIGNAL next_state_in          : fsm_states;

BEGIN

 fswi : fsm_switch 
  PORT MAP (
    phi1                => phi1,
    reset               => reset,
    next_state          => next_state_in,
    current_state       => current_state_in);

  fnext : fsm_next 
  PORT MAP (
    reset               => reset,
    halt		=> halt,
    alu_zero            => alu_zero,
    alu_neg		=> alu_neg,
    dec_1_in            => dec_1_in,
    dec_2_in            => dec_2_in,
    current_state	=> current_state_in,
    next_state          => next_state_in);

  fout : fsm_output 
  PORT MAP (
    current_state	=> current_state_in,
    s1_enab		=> s1_enab,
    s2_enab		=> s2_enab,
    dest_enab           => dest_enab,
    alu_op_sel          => alu_op_sel,
    const_sel           => const_sel,
    rf_op_sel           => rf_op_sel,
    immed_sel           => immed_sel,
    mem_ctrl            => mem_ctrl,
    state_number        => state_number);

END structural;












