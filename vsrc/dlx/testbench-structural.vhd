-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--
--  (file testbench-structural.vhd)
-- -----------------------------------------------------------

ARCHITECTURE structural OF tb IS

-- Typdeklaration der Initialisierungszustaende:  
  TYPE init_states IS (
    -- Prozessor-RESET:
    reset_proc,
    -- Prozessor starten:
    run_proc,
    -- Steuerung des System-Speicher-Zugriffs (SelectRAM Port B):
    rom_write_enable,   -- Schreibfreigabe ROM
    rom_read_enable,    -- Lesefreigabe ROM
    ram_write_enable,   -- Schreibfreigabe RAM
    ram_read_enable,    -- Lesefreigabe RAM
    memory_disable,     -- Systemspeicher sperren
    -- Adresse fuer die Displayadresse in Adressregister:
    disp_addr_reg_low,  -- Low-Byte 
    disp_addr_reg_high, -- High-Byte
    -- Displayadresse in den System-ROM schreiben:
    disp_addr_0,        -- Byte 0 
    disp_addr_1,        -- Byte 1 
    disp_addr_2,        -- Byte 2
    disp_addr_3,        -- Byte 3 
    -- Adresse fuer die RAM-Basisadresse in Adressregister:
    rambase_addr_reg_low,  -- Low-Byte 
    rambase_addr_reg_high, -- High-Byte
    -- Daten fuer die Basisadresse in den System-ROM schreiben:
    rambase_addr_0,        -- Byte 0 
    rambase_addr_1,        -- Byte 1 
    rambase_addr_2,        -- Byte 2
    rambase_addr_3,        -- Byte 3
    -- Einschreiben Fehlercode in den System-ROM:
    -- (fuehrt zu DLX-Fehler, wenn kein Maschinencode existiert
    error_addr_0,        -- Byte 0 
    error_addr_1,        -- Byte 1 
    error_addr_2,        -- Byte 2 
    error_addr_3,        -- Byte 3 
    -- Initialisierung des Adressregisters:
    init_addr_reg_low,  -- Low-Byte 
    init_addr_reg_high, -- High-Byte
    -- Aktualisierung des Adressregisters:
    load_addr_reg_low,  -- Low-Byte 
    load_addr_reg_high, -- High-Byte
    -- Programmcode in den System-ROM schreiben:
    load_rom,
    -- Auswahl der Taktquelle:
    enab_sio_clk,       -- Interface
    enab_q_clk,         -- Quarz-Taktgenerator
    -- Auswahl der Anzeigequelle:
    display_source,
    -- zusaetzlicher Start- und Stopzustand:
    start_init,
    stop_init,
    -- Wartezustand
    wait_one_state, -- mit Port-Ausgaben
    nop_state, -- ohne Port-Ausgaben
    bus_uc_reg_init, -- Registerinitialisierung uC-Buslesezugriffe
    reg_uc_reg_init -- Registerinitialisierung uC-Registerlesezugriffe
    );
  
   -----------------------
-- Steuerregister-Adressen
   -----------------------
  -- Adressregister fuer System-Speicher-Adresse (SelectRAM Port B):
  -- Register fuer LOW-Teil:
  CONSTANT a_reg_low_we        : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"30"));
  -- Register fuer HIGH-Teil:
  CONSTANT a_reg_high_we       : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"31"));
  -- Register fuer Schreibsteuerung des System-Speichers (SelectRAM Port B):
  CONSTANT mem_reg_en          : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"40"));
  -- Register fuer RESET und HALT:   
  CONSTANT res_hlt_reg_en      : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"50"));
  -- Register fuer die Auswahl der Taktquelle:
  CONSTANT core_clk_sel_reg_en : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"60"));
  -- Register fuer die Auswahl der Anzeigequelle:
  CONSTANT disp_sel_reg_en     : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"70"));
  -- Register fuer die Auswahl des vom uC lesbaren Busses:
  CONSTANT bus_ucrd_sel_reg_en : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"80"));
  -- Register fuer die Auswahl der vom uC lesbaren Register:
  CONSTANT reg_uc_reg_en       : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"a0"));

   --------------------  
-- Steuerregister-Werte
   --------------------
  -- Prozessor-RESET, -HALT:
  CONSTANT proc_reset            : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"01"));
  CONSTANT proc_halt             : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"02"));
  CONSTANT not_res_not_hlt  : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"00"));
  
  -- Schreiben des System-ROM (SelectRAM Port B)
  --    freigeben:
  CONSTANT rom_wr_enable    : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"01"));
  -- Lesen des System-ROM (SelectRAM Port B)
  --    freigeben:
  CONSTANT rom_rd_enable    : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"02"));
  -- Schreiben des System-RAM (SelectRAM Port B)
  --    freigeben:
  CONSTANT ram_wr_enable    : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"10"));
  -- Lesen des System-RAM (SelectRAM Port B)
  --    freigeben:
  CONSTANT ram_rd_enable    : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"20"));

  -- Speicherzugriff sperren:
  CONSTANT mem_disable   : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"00"));

-- Adressregister-Werte:
  --    Initialisierung: 
  CONSTANT a_reg_init       : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"00"));
  --    fuer Displayadresse:
  --            Low-Teil der Adresse:
  CONSTANT a_reg_l_disp     : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"FF"));
  --            High-Teil der Adresse:
  CONSTANT a_reg_h_disp     : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"03"));
  --    fuer Basisadresse des RAM:
  --            Low-Teil der Adresse:
  CONSTANT a_reg_l_rambase     : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"FE"));
  --            High-Teil der Adresse:
  CONSTANT a_reg_h_rambase     : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"03"));
  -- Speicher-Inhalt (Adresse 3ff), der die Displayadresse festlegt:
  --    Byte 0,1,2:
  CONSTANT disp_012         : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"00"));
  --    Byte 3:
  CONSTANT disp_3           : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"20"));
  -- Speicher-Inhalt (Adresse 3fe) fuer die RAM-Basis-Adresse:
  --    Byte 0,1,2:
  CONSTANT rambase_012         : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"00"));
  --    Byte 3:
  CONSTANT rambase_3           : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"10"));
  CONSTANT error_code_0123     : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"ff"));
  -- Position der Datenbytes bei Schreibvorgaengen auf den System-ROM:
  --    Byte 0:
  CONSTANT data_pos_0       : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"10"));
  --    Byte 1:
  CONSTANT data_pos_1       : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"11"));
  --    Byte 2:
  CONSTANT data_pos_2       : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"12"));
  --    Byte 3:
  CONSTANT data_pos_3       : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"13"));
  -- Auswahl der Taktquelle:
  --    Interface:
  CONSTANT core_none_clk     : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"00"));
  --    Quarz-Taktgenerator
  CONSTANT core_q_clk       : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"01"));
  -- Auswahl der Anzeigequelle:
  --    Anzeige-Register:
  CONSTANT disp_display_latch : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"00"));
  --    Core-Datenausgang:
  CONSTANT disp_core_data_out : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"01"));
  --    Core-Dateneingang:
  CONSTANT disp_core_data_in  : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"02"));
  --    Core-Adressausgang:
  CONSTANT disp_core_addr_out : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"03"));
  --    S1 Bus: 
  CONSTANT disp_s1_bus : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"04"));
  --    S2 Bus:
  CONSTANT disp_s2_bus  : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"05"));
  --    Dest Bus: 
  CONSTANT disp_dest_bus         : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"06"));
  --    IR Register Ausgang:
  CONSTANT disp_ir_reg          : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"07"));
  -- Auswahl der Bussignale nach Port A:
  -- da in der Simulation nicht verwendet,
  -- wird das Display Latch initialisiert
  CONSTANT bus_ucrd_display_latch : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"00"));
  -- Register dump:
  -- da in der Simulation nicht verwendet,
  -- wird mit "not_reg_dump" initialisiert
  CONSTANT not_reg_dump : std_logic_vector(7 DOWNTO 0)
    := To_StdLogicVector(bit_vector'(X"00"));

     -------------------
  -- Signaldeklarationen
     -------------------
  -- loest initialisierungsroutine aus:
  SIGNAL init               : std_logic := '0';
  -- Initialisierungstakt:
  SIGNAL init_clk           : std_logic := '0';
  -- Quarzgeneratortakt:
  SIGNAL q_clk              : std_logic := '0';
  -- Signale der Initialisierungsroutine fuer den Prozessor:
  SIGNAL sio_port_b         : std_logic_vector(7 DOWNTO 0);
  SIGNAL sio_port_c         : std_logic_vector(7 DOWNTO 0);
  SIGNAL sio_clk            : std_logic;
  -- Momentanzustand der Initialisierungsroutine:
  SIGNAL current_init_state : init_states;
  -- Folgezustand der Initialisierungsroutine:
  SIGNAL next_init_state    : init_states := start_init;
  -- aktuelle ROM-Adressen:
  SIGNAL addr_l             : std_logic_vector(7 DOWNTO 0) := "00000000";
  SIGNAL addr_h             : std_logic_vector(7 DOWNTO 0) := "00000000";
  -- Gueltigkeitssignal fuer z.B. externe Speicher
  SIGNAL starttrigger       : std_logic_vector(3 DOWNTO 0) := To_StdLogicVector(bit_vector'(X"0"));
  SIGNAL state   : fsm_state_numbers; 
  SIGNAL phi1_2  : std_logic_vector(1 DOWNTO 0);  
  SIGNAL fsm_state : fsm_states;  

  ----------------------
-- Komponentendeklaration
   ----------------------
  COMPONENT vsdlx
      PORT (
    --
    -- Clock Sources:
    ---------
    uc_clk      : IN  std_logic;  -- uc port E(5) (AVR), single clock pulse
    q_clk       : IN  std_logic;  -- crystal clock generator
    
    -- LCD Display:
    ------
    LCD_DB                      : OUT std_logic_vector(7 DOWNTO 0);
    LCD_E, LCD_RS, LCD_RW       : OUT std_logic;    

    -- UC-Ports (AVR):
    ------------
    -- AVR_A, AVR_B                : IN  std_logic_vector(7 DOWNTO 0);
    -- AVR_C, AVR_D                : OUT std_logic_vector(7 DOWNTO 0)
    uc_port_a  : OUT uc_byte;
    uc_port_b  : IN  uc_byte;          
    uc_port_c  : IN  uc_byte;        

    --
    state       : OUT fsm_state_numbers;  -- display states
                                                     -- (2 digits)
    phi1_2      : OUT std_logic_vector(1 DOWNTO 0));  -- display two-phase clock
    --display     : OUT std_logic_vector(31 DOWNTO 0); -- display data or
                                                     -- address (8 digits)
    --error       : OUT std_logic_vector(7 DOWNTO 0);  -- display error signal
                                                     -- (8 decimal points)
  END COMPONENT;

   ---------------------  
-- Initialisierungsphase
   ---------------------  
BEGIN
  -- Taktgenerator fuer Initialisierungsroutine:
  init_clk      <= NOT init_clk AFTER t_ph_init_clk;
  -- Quarz-Taktgenerator:
  q_clk         <= NOT q_clk AFTER t_ph_q_clk;
  -- Zustandsschalter:
  init_state_switch : PROCESS(init_clk)
  BEGIN
    IF (init_clk'event AND init_clk = '1') THEN
      current_init_state <= next_init_state;
    END IF;
  END PROCESS;
  -- Zustaende und Ausgaben:
  init_sequencer : PROCESS
   VARIABLE l_in      : line;
   VARIABLE l_out     : line;
   VARIABLE l_text    : line;
   VARIABLE code_hex  : std_logic_vector(7 DOWNTO 0);
   FILE bytecode      : text IS IN "../tmp/bytecode.hex";
--   FILE redirect      : text IS OUT "../sim/redi.txt";
   FILE redirect      : text IS OUT "../sim/redi.txt";
   VARIABLE bool      : boolean;
   VARIABLE i         : integer := 0;
   VARIABLE line_no   : integer := 1;
   VARIABLE i_message : integer := 1;
 BEGIN  
    WAIT ON init_clk,current_init_state, next_init_state;
--    WAIT ON init_clk;
    -- Startzustand:
    IF  current_init_state = start_init THEN 
      sio_port_c        <= "00000000";
      sio_port_b        <= "00000000";
      next_init_state   <= enab_sio_clk;
      init              <= '1';
    END IF;

    -- Interfacetakt fuer Core sperren:  
    IF  current_init_state  = enab_sio_clk THEN 
      sio_port_c        <= core_clk_sel_reg_en;
      sio_port_b        <= core_none_clk;
      next_init_state   <= reset_proc;
    END IF;

    -- Prozessor ruecksetzen:  
    IF  current_init_state  = reset_proc THEN 
      sio_port_c        <= res_hlt_reg_en;
      sio_port_b        <= proc_reset;
      next_init_state   <= bus_uc_reg_init;
    END IF;

    -- Register fuer uC-Debugging initialisieren:  
    IF  current_init_state  = bus_uc_reg_init THEN 
      sio_port_c        <= bus_ucrd_sel_reg_en;
      sio_port_b        <= bus_ucrd_display_latch;
      next_init_state   <= reg_uc_reg_init;
    END IF;
    IF  current_init_state  = reg_uc_reg_init THEN 
      sio_port_c        <= reg_uc_reg_en;
      sio_port_b        <= not_reg_dump;
      next_init_state   <= display_source;
    END IF;

-- Anzeigequelle auswaehlen:  
    IF  current_init_state  = display_source THEN 
      sio_port_c        <= disp_sel_reg_en;
      -- Anzeigequelle:
      sio_port_b        <= disp_display_latch;
--      sio_port_b        <= disp_core_data_out;
--      sio_port_b        <= disp_core_data_in;
--      sio_port_b        <= disp_core_addr_out;
--      sio_port_b        <= disp_s1_bus;
--      sio_port_b        <= disp_s2_bus;
--      sio_port_b        <= disp_dest_bus;
--      sio_port_b        <= disp_ir_reg;
      next_init_state   <= rom_write_enable;
    END IF;
    -- System-ROM Schreibfreigabe:
    IF current_init_state =  rom_write_enable THEN
      sio_port_c        <= mem_reg_en;
      sio_port_b        <= rom_wr_enable;
      next_init_state   <= disp_addr_reg_low;
    END IF;
    -- Adressregister fuer Displayadresse setzen:
    --  Low-Byte:
    IF current_init_state = disp_addr_reg_low THEN
      sio_port_c        <= a_reg_low_we;
      sio_port_b        <= a_reg_l_disp;
      next_init_state   <= disp_addr_reg_high;
    END IF;
    -- Adressregister fuer Displayadresse setzen:
    --  High-Byte:
    IF current_init_state =  disp_addr_reg_high THEN
      sio_port_c        <= a_reg_high_we;
      sio_port_b        <= a_reg_h_disp;
      next_init_state   <= disp_addr_0;
    END IF;
    -- Einschreiben der Displayadresse in den System-ROM:
    --    Datenbyte 0:
    IF current_init_state =  disp_addr_0 THEN
      sio_port_c        <= data_pos_0;
      sio_port_b        <= disp_012;
      next_init_state   <= disp_addr_1;
    END IF;
    --    Datenbyte 1:
    IF current_init_state =  disp_addr_1 THEN
      sio_port_c        <= data_pos_1;
      sio_port_b        <= disp_012;
      next_init_state   <= disp_addr_2;
    END IF;
    --    Datenbyte 2:
    IF current_init_state =  disp_addr_2 THEN
      sio_port_c        <= data_pos_2;
      sio_port_b        <= disp_012;
      next_init_state   <= disp_addr_3;
    END IF;
    --    Datenbyte 3:
    IF current_init_state =  disp_addr_3 THEN
      sio_port_c        <= data_pos_3;
      sio_port_b        <= disp_3;
      next_init_state   <= rambase_addr_reg_low;
    END IF;

    -- Adressregister fuer RAM-Basisadresse setzen:
    --  Low-Byte:
    IF current_init_state = rambase_addr_reg_low THEN
      sio_port_c        <= a_reg_low_we;
      sio_port_b        <= a_reg_l_rambase;
      next_init_state   <= rambase_addr_reg_high;
    END IF;
    -- Adressregister fuer RAM-Basisadresse setzen:
    --  High-Byte:
    IF current_init_state =  rambase_addr_reg_high THEN
      sio_port_c        <= a_reg_high_we;
      sio_port_b        <= a_reg_h_rambase;
      next_init_state   <= rambase_addr_0;
    END IF;
    
    -- Einschreiben RAM-Basisadresse in den System-ROM:
    --    Datenbyte 0:
    IF current_init_state =  rambase_addr_0 THEN
      sio_port_c        <= data_pos_0;
      sio_port_b        <= rambase_012;
      next_init_state   <= rambase_addr_1;
    END IF;
    --    Datenbyte 1:
    IF current_init_state =  rambase_addr_1 THEN
      sio_port_c        <= data_pos_1;
      sio_port_b        <= rambase_012;
      next_init_state   <= rambase_addr_2;
    END IF;
    --    Datenbyte 2:
    IF current_init_state =  rambase_addr_2 THEN
      sio_port_c        <= data_pos_2;
      sio_port_b        <= rambase_012;
      next_init_state   <= rambase_addr_3;
    END IF;
    --    Datenbyte 3:
    IF current_init_state =  rambase_addr_3 THEN
      sio_port_c        <= data_pos_3;
      sio_port_b        <= rambase_3;
      next_init_state   <= init_addr_reg_low;
    END IF;    
-- Startadresse fuer Einschreiben des Maschinencodes
    -- in den System-ROM - Low-Teil:
    IF current_init_state =  init_addr_reg_low THEN
      sio_port_c        <= a_reg_low_we;
      sio_port_b        <= a_reg_init;
      next_init_state <= init_addr_reg_high;
    END IF;    
    -- Startadresse fuer Einschreiben des Maschinencodes
    -- in den System-ROM - High-Teil:
    IF current_init_state =  init_addr_reg_high THEN
      sio_port_c        <= a_reg_high_we;
      sio_port_b        <= a_reg_init;
      next_init_state   <= error_addr_0;
    END IF;
-- Einschreiben Fehlercode in den System-ROM:
    -- (fuehrt zu DLX-Fehler, wenn kein Maschinencode existiert
    --    Datenbyte 0:
    IF current_init_state = error_addr_0 THEN
      sio_port_c        <= data_pos_0;
      sio_port_b        <= error_code_0123;
      next_init_state   <= error_addr_1;
    END IF;
    --    Datenbyte 1:
    IF current_init_state =  error_addr_1 THEN
      sio_port_c        <= data_pos_1;
      sio_port_b        <= error_code_0123;
      next_init_state   <= error_addr_2;
    END IF;
    --    Datenbyte 2:
    IF current_init_state =  error_addr_2 THEN
      sio_port_c        <= data_pos_2;
      sio_port_b        <= error_code_0123;
      next_init_state   <= error_addr_3;
    END IF;
    --    Datenbyte 3:
    IF current_init_state =  error_addr_3 THEN
      sio_port_c        <= data_pos_3;
      sio_port_b        <= error_code_0123;
      next_init_state   <= load_rom;
    END IF;    
-- Einschreiben des Maschinencodes in den System-ROM:
    IF current_init_state = load_rom THEN    
     WHILE (endfile(bytecode) = false) LOOP
        WAIT UNTIL (init_clk'event AND init_clk = '1');
        READLINE (bytecode, l_in);
        hread (l_in, code_hex, bool);
        ASSERT bool REPORT "Error reading file!";
        IF ((i=0) AND (line_no =1)) THEN  
      write(l_out, string'(" "));
      writeline(output, l_out);         
      write(l_out, string'(" >>>Loading Machine-code:"));
      writeline(output, l_out);         
      write(l_out, string'(" "));
      writeline(output, l_out);         
      write(l_out, string'("    Address     Code  "));
      writeline(output, l_out);         
      write(l_out, string'("    --------    --------"));
      writeline(output, l_out);         
        END IF;       
        IF (i=0) THEN  
      write(l_out, string'("    "));         
      hwrite(l_out, To_StdLogicVector(bit_vector'(X"0000")));
      hwrite(l_out, integer_to_sv(((sv_to_integer(addr_h & addr_l)  )*4),16));
      -- hwrite(l_out, addr_l);         
      write(l_out, string'("    "));         
        END IF;       
        hwrite(l_out, code_hex);
        sio_port_b <= code_hex;
        IF (I = 0) THEN 
          sio_port_c <= data_pos_3;
          i := (i + 1);        
        ELSIF (i = 1) THEN 
          sio_port_c <= data_pos_2;
          i := (i + 1);              
        ELSIF (i = 2) THEN 
          sio_port_c <= data_pos_1;
          i := (i + 1);              
        ELSIF (i = 3) THEN 
          sio_port_c <= data_pos_0;
          i := 0;
          writeline(output, l_out);         
         addr_l <= addr_l + '1';
          WAIT UNTIL (init_clk'event AND init_clk = '1');
          sio_port_c        <= a_reg_low_we;
          sio_port_b        <= addr_l;
          IF (addr_l = "11111111") THEN
            addr_h <= addr_h + '1';
            WAIT UNTIL (init_clk'event AND init_clk = '1');
            sio_port_c        <= a_reg_high_we;
            sio_port_b        <= addr_h;          
          END IF;
        END IF;
        line_no := line_no +1;
        IF (endfile(bytecode) = true) THEN
          write(l_out, string'(" "));
          writeline(output, l_out);
          write(l_out, string'(">>> Machine-code loaded ! "));
          writeline(output, l_out);
          write(l_out, string'(" "));
          writeline(output, l_out);
        END IF;
     END LOOP;
        next_init_state <= enab_q_clk;
    END IF;
-- Quarztakt freigeben:  
    IF  current_init_state  = enab_q_clk THEN 
       sio_port_c        <= core_clk_sel_reg_en;
       sio_port_b        <= core_q_clk;
-- Versuch, die RTL-Simulation in Gang zu bringen
--       WAIT FOR t_enab_q_clk;
               IF ((line_no = 1) AND (i_message = 1)) THEN 
          write(l_out, string'(" "));
          writeline(output, l_out);
          write(l_out, string'(">>> Nonexisting machine-code file ! "));
          writeline(output, l_out);
          write(l_out, string'("     ----------------------------- "));
          writeline(output, l_out);
          write(l_out, string'(" "));
          writeline(output, l_out);
          i_message := (i_message + 1);        
        END IF;
       next_init_state   <= wait_one_state;
     END IF;

  IF  current_init_state  = wait_one_state THEN
    sio_port_c        <= res_hlt_reg_en;
    sio_port_b        <= proc_reset;
    next_init_state   <= run_proc;
  END IF;
    
    -- Prozessor starten:  
    IF  current_init_state  = run_proc THEN 
      sio_port_c        <= res_hlt_reg_en;
      sio_port_b        <= not_res_not_hlt;
      starttrigger <= To_StdLogicVector(bit_vector'(X"A"));
      starttrigger <= "1010";
      next_init_state   <= stop_init;
    END IF;
    -- Initialisierung beendet:
    IF current_init_state = stop_init THEN
      sio_port_c        <= "00000000";
      sio_port_b        <= "00000000";
      next_init_state   <= stop_init;
      init <= '0';
    END IF;    
  END PROCESS;
  sio_clk <= init_clk AFTER t_del_sio_clk; -- WHEN init = '1' ELSE 'Z'; 

  --------------------------------
--Instantiierung des Schaltkreises
  --------------------------------
  system : vsdlx

    PORT MAP (
      --
      -- 8 digit display:
      --
     state      => state,      -- 2 digits 
      phi1_2     => phi1_2,  
    --  display    => OPEN,       -- 8 digits
    --  error      => OPEN,       -- most significant decimal point 
      --
      -- 2 clock sources
      --
      uc_clk    => sio_clk,     --  uc-port D (bit 0)
      q_clk     => q_clk,       -- external Crystal Clock generator
      -- LCD Display:
      --------------
      LCD_DB    => OPEN,                
      LCD_E     => OPEN,
      LCD_RS    => OPEN,
      LCD_RW    => OPEN,          
      --                                       
      -- SIO-Interface:
      --
      uc_port_a => OPEN,        -- uc-port A 
      uc_port_b => sio_port_b,  -- uc-port B
      uc_port_c => sio_port_c   -- uc-port C
      );
 
--   write_state_con : process(states, phi1_2)
--     variable L : Line;
--   BEGIN
--     IF phi1_2 = "10" THEN 
--       case states is
-- 	when "00000001" => write(L, string'("-> fsm_state : res_state"));
-- 	when "00000010" => write(L, string'("-> fsm_state : fetch"));	  
-- 	when "00000011" => write(L, string'("-> fsm_state : dec_pc4_ab"));
-- 	when "00000100" => write(L, string'("-> fsm_state : memory"));
-- 	when "00000101" => write(L, string'("-> fsm_state : load_w_1"));
-- 	when "00000110" => write(L, string'("-> fsm_state : load_w_2"));
-- 	when "00000111" => write(L, string'("-> fsm_state : store_w_1"));
-- 	when "00001000" => write(L, string'("-> fsm_state : store_w_2"));
--  	when "00001001" => write(L, string'("-> fsm_state : br_eqz"));
-- 	when "00010000" => write(L, string'("-> fsm_state : branch"));
-- 	when "00010001" => write(L, string'("-> fsm_state : jump"));
-- 	when "00010010" => write(L, string'("-> fsm_state : load_pc"));	  
-- 	when "00010011" => write(L, string'("-> fsm_state : sub_1"));
-- 	when "00010100" => write(L, string'("-> fsm_state : slt_1")); 
-- 	when "00010101" => write(L, string'("-> fsm_state : set_to_1")); 
-- 	when "00010110" => write(L, string'("-> fsm_state : set_to_0"));
-- 	when "00010111" => write(L, string'("-> fsm_state : wr_back")); 
-- 	when "00011000" => write(L, string'("-> fsm_state : hlt_state")); 
-- 	when "00011001" => write(L, string'("-> fsm_state : err_state")); 
-- --	when "" => write(L, string'("-> fsm_state : add_1"));
-- --	when "" => write(L, string'("-> fsm_state : and_1")); 
-- --	when "" => write(L, string'("-> fsm_state : or_1")); 
-- 	when "00100000" => write(L, string'("-> fsm_state : xor_1")); 
-- 	when OTHERS => write(L, string'("-> fsm_state : state not implemented")); 
--       end case;
--     END if;
--     writeline(output, L);
--   end process write_state_con;
 
--   CONSTANT res_state_no       : fsm_state_numbers := "00001"; -- state 1
--   CONSTANT fetch_no           : fsm_state_numbers := "00010"; -- state 2
--   CONSTANT dec_pcinc4_ab_no   : fsm_state_numbers := "00011"; -- state 3
--   CONSTANT memory_no          : fsm_state_numbers := "00100"; -- state 4
--   CONSTANT load_w_1_no        : fsm_state_numbers := "00101"; -- state 5
--   CONSTANT load_w_2_no        : fsm_state_numbers := "00110"; -- state 6
--   CONSTANT store_w_1_no       : fsm_state_numbers := "00111"; -- state 7
--   CONSTANT store_w_2_no       : fsm_state_numbers := "01000"; -- state 8
--   CONSTANT br_eqz_no          : fsm_state_numbers := "01001"; -- state 9
--   CONSTANT branch_no          : fsm_state_numbers := "10000"; -- state 10
--   CONSTANT jump_no            : fsm_state_numbers := "10001"; -- state 11
--   CONSTANT sub_no             : fsm_state_numbers := "10010"; -- state 12
--   CONSTANT slt_no             : fsm_state_numbers := "10011"; -- state 13
--   CONSTANT set_to_1_no        : fsm_state_numbers := "10100"; -- state 14
--   CONSTANT set_to_0_no        : fsm_state_numbers := "10101"; -- state 15
--   CONSTANT wr_back_no         : fsm_state_numbers := "10110"; -- state 16
--   CONSTANT hlt_state_no       : fsm_state_numbers := "10111"; -- state 17
--   CONSTANT err_state_no       : fsm_state_numbers := "11000"; -- state 18
--   CONSTANT add_no             : fsm_state_numbers := "11001"; -- state 19
 
  write_state_vwaves : PROCESS
  BEGIN
   WAIT UNTIL phi1_2 = "01"; 
      WAIT FOR 8 ns;
      CASE state IS
	WHEN "000001" => fsm_state <= res_state;
	WHEN "000010" => fsm_state <= fetch;	  
	WHEN "000011" => fsm_state <= dec_pcinc4_ab; 
	WHEN "000100" => fsm_state <= memory;
	WHEN "000101" => fsm_state <= load_w_1; 
	WHEN "000110" => fsm_state <= load_w_2;
	WHEN "000111" => fsm_state <= store_w_1;
	WHEN "001000" => fsm_state <= store_w_2;
 	WHEN "001001" => fsm_state <= br_eqz; 
	WHEN "010000" => fsm_state <= branch;
	WHEN "010001" => fsm_state <= jump; 	  
	WHEN "010010" => fsm_state <= sub; 
	WHEN "010011" => fsm_state <= slt; 
	WHEN "010100" => fsm_state <= set_to_1; 
	WHEN "010101" => fsm_state <= set_to_0;
	WHEN "010110" => fsm_state <= wr_back;  
	WHEN "010111" => fsm_state <= hlt_state; 
	WHEN "011000" => fsm_state <= err_state;  
	WHEN "011001" => fsm_state <= add;
	WHEN "100000" => fsm_state <= and_1;
	WHEN "100001" => fsm_state <= or_1;
	WHEN "100010" => fsm_state <= sll_1;
	WHEN "100011" => fsm_state <= srl_1;
--	when "" => fsm_state <= and_1; 
--	when "" => fsm_state <= or_1; 
--	WHEN "" => fsm_state <= xor_1; 
	WHEN OTHERS => fsm_state <= err_state; 
      END CASE;
  END PROCESS write_state_vwaves;
  
END structural;










