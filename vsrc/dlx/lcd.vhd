
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

PACKAGE lcd_conv IS
  
  TYPE lcd_char IS (
   ' ','!','"','#','$','%','&',''','(',')','*','+',',','-','.','/',
   '0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?',
   '@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
   'P','Q','R','S','T','U','V','W','X','Y','Z','[',    ']','^','_',
   '`','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',
   'p','q','r','s','t','u','v','w','x','y','z','{','|','}'
    );

  FUNCTION to_lcd (SIGNAL nibble : IN std_logic_vector(3 DOWNTO 0))
    RETURN std_logic_vector;

  FUNCTION to_lcd (CONSTANT zeichen : IN lcd_char)
    RETURN std_logic_vector;

  TYPE lcd_array IS ARRAY (integer RANGE 0 TO 1, integer RANGE 0 TO 15 ) OF std_logic_vector(7 DOWNTO 0);

END lcd_conv;

PACKAGE BODY lcd_conv IS

  FUNCTION to_lcd(SIGNAL nibble : IN std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector IS

    VARIABLE kodierung : std_logic_vector(7 DOWNTO 0);
  BEGIN

    CASE nibble IS
      WHEN "0000" => kodierung := "00110000";
      WHEN "0001" => kodierung := "00110001";
      WHEN "0010" => kodierung := "00110010";
      WHEN "0011" => kodierung := "00110011";
      WHEN "0100" => kodierung := "00110100";
      WHEN "0101" => kodierung := "00110101";
      WHEN "0110" => kodierung := "00110110";
      WHEN "0111" => kodierung := "00110111";
      WHEN "1000" => kodierung := "00111000";
      WHEN "1001" => kodierung := "00111001";
      WHEN "1010" => kodierung := "01000001";
      WHEN "1011" => kodierung := "01000010";
      WHEN "1100" => kodierung := "01000011";
      WHEN "1101" => kodierung := "01000100";
      WHEN "1110" => kodierung := "01000101";
      WHEN "1111" => kodierung := "01000110";
      WHEN OTHERS => kodierung := "11111111";
    END CASE;
    RETURN kodierung;  
  END to_lcd;

  FUNCTION to_lcd(CONSTANT zeichen : IN lcd_char) RETURN std_logic_vector IS

    VARIABLE kodierung : std_logic_vector(7 DOWNTO 0);
  BEGIN

    CASE zeichen IS

      WHEN (' ') => kodierung := "00100000";
      WHEN ('!') => kodierung := "00100001";
      WHEN ('"') => kodierung := "00100010";
      WHEN ('#') => kodierung := "00100011";
      WHEN ('$') => kodierung := "00100100";
      WHEN ('%') => kodierung := "00100101";
      WHEN ('&') => kodierung := "00100110";
      WHEN (''') => kodierung := "00100111";
                    
      WHEN ('(') => kodierung := "00101000";
      WHEN (')') => kodierung := "00101001";
      WHEN ('*') => kodierung := "00101010";
      WHEN ('+') => kodierung := "00101011";
      WHEN (',') => kodierung := "00101100";
      WHEN ('-') => kodierung := "00101101";
      WHEN ('.') => kodierung := "00101110";
      WHEN ('/') => kodierung := "00101111";
                    
      WHEN ('0') => kodierung := "00110000";
      WHEN ('1') => kodierung := "00110001";
      WHEN ('2') => kodierung := "00110010";
      WHEN ('3') => kodierung := "00110011";
      WHEN ('4') => kodierung := "00110100";
      WHEN ('5') => kodierung := "00110101";
      WHEN ('6') => kodierung := "00110110";
      WHEN ('7') => kodierung := "00110111";

      WHEN ('8') => kodierung := "00111000";
      WHEN ('9') => kodierung := "00111001";
      WHEN (':') => kodierung := "00111010";
      WHEN (';') => kodierung := "00111011";
      WHEN ('<') => kodierung := "00111100";
      WHEN ('=') => kodierung := "00111101";
      WHEN ('>') => kodierung := "00111110";
      WHEN ('?') => kodierung := "00111111";

      WHEN ('@') => kodierung := "01000000";
      WHEN ('A') => kodierung := "01000001";
      WHEN ('B') => kodierung := "01000010";
      WHEN ('C') => kodierung := "01000011";
      WHEN ('D') => kodierung := "01000100";
      WHEN ('E') => kodierung := "01000101";
      WHEN ('F') => kodierung := "01000110";
      WHEN ('G') => kodierung := "01000111";

      WHEN ('H') => kodierung := "01001000";
      WHEN ('I') => kodierung := "01001001";
      WHEN ('J') => kodierung := "01001010";
      WHEN ('K') => kodierung := "01001011";
      WHEN ('L') => kodierung := "01001100";
      WHEN ('M') => kodierung := "01001101";
      WHEN ('N') => kodierung := "01001110";
      WHEN ('O') => kodierung := "01001111";

      WHEN ('P') => kodierung := "01010000";
      WHEN ('Q') => kodierung := "01010001";
      WHEN ('R') => kodierung := "01010010";
      WHEN ('S') => kodierung := "01010011";
      WHEN ('T') => kodierung := "01010100";
      WHEN ('U') => kodierung := "01010101";
      WHEN ('V') => kodierung := "01010110";
      WHEN ('W') => kodierung := "01010111";

      WHEN ('X') => kodierung := "01011000";
      WHEN ('Y') => kodierung := "01011001";
      WHEN ('Z') => kodierung := "01011010";
      WHEN ('[') => kodierung := "01011011";
 
      WHEN (']') => kodierung := "01011101";
      WHEN ('^') => kodierung := "01011110";
      WHEN ('_') => kodierung := "01011111";

      WHEN ('`') => kodierung := "01100000";
      WHEN ('a') => kodierung := "01100001";
      WHEN ('b') => kodierung := "01100010";
      WHEN ('c') => kodierung := "01100011";
      WHEN ('d') => kodierung := "01100100";
      WHEN ('e') => kodierung := "01100101";
      WHEN ('f') => kodierung := "01100110";
      WHEN ('g') => kodierung := "01100111";

      WHEN ('h') => kodierung := "01101000";
      WHEN ('i') => kodierung := "01101001";
      WHEN ('j') => kodierung := "01101010";
      WHEN ('k') => kodierung := "01101011";
      WHEN ('l') => kodierung := "01101100";
      WHEN ('m') => kodierung := "01101101";
      WHEN ('n') => kodierung := "01101110";
      WHEN ('o') => kodierung := "01101111";

      WHEN ('p') => kodierung := "01110000";
      WHEN ('q') => kodierung := "01110001";
      WHEN ('r') => kodierung := "01110010";
      WHEN ('s') => kodierung := "01110011";
      WHEN ('t') => kodierung := "01110100";
      WHEN ('u') => kodierung := "01110101";
      WHEN ('v') => kodierung := "01110110";
      WHEN ('w') => kodierung := "01110111";

      WHEN ('x') => kodierung := "01111000";
      WHEN ('y') => kodierung := "01111001";
      WHEN ('z') => kodierung := "01111010";
      WHEN ('{') => kodierung := "01111011";
      WHEN ('|') => kodierung := "01111100";
      WHEN ('}') => kodierung := "01111101";

      WHEN OTHERS => kodierung := "01011100";
    END CASE;
    RETURN kodierung;  
  END to_lcd;
END lcd_conv;
-- ####################################################################

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;         
USE IEEE.STD_LOGIC_ARITH.ALL;        
USE IEEE.STD_LOGIC_UNSIGNED.ALL; 
LIBRARY work;
USE work.lcd_conv.ALL;

ENTITY lcd IS
  PORT(
    clk                         : IN  std_logic;
    lcd_data                    : IN lcd_array;
    LCD_DB                      : OUT std_logic_vector(7 DOWNTO 0);
    LCD_E, LCD_RS, LCD_RW       : OUT std_logic);
END lcd;
---------------------------------------------------
ARCHITECTURE behavior OF lcd IS
  
  TYPE zustands_typ IS (warten_power_on, function_set1, warten1, function_set2, warten2,
                        display_on, warten3, display_clear, warten4, entry_mode, warten5,
                        write_00_addr, write_01_addr, write_0_addr, write_2_addr, write_0_data,
                        write_2_data);                     
  SIGNAL   momentan, folge : zustands_typ;
  SIGNAL   en_i            : boolean;
  SIGNAL   en_a            : boolean;
  SIGNAL   clr_0_a         : boolean;
  SIGNAL   clr_1_a         : boolean;
  SIGNAL   addr_ddram      : std_logic_vector(7 DOWNTO 0); -- := "00000000";
  SIGNAL   i               : integer RANGE 0 TO 100000000;  --2500000;
  CONSTANT i_25            : integer := 25;  
  CONSTANT i_2000          : integer := 2000;
  CONSTANT i_82000         : integer := 82000;
  CONSTANT i_2500000       : integer := 2500000;
  SIGNAL   sel             : std_logic_vector(4 DOWNTO 0) := "00000";
  SIGNAL   do              : std_logic_vector(7 DOWNTO 0);
  CONSTANT  zeile_0_anfang : std_logic_vector(7 DOWNTO 0) := "10000000"; 
  CONSTANT  zeile_1_anfang : std_logic_vector(7 DOWNTO 0) := "11000000";
  CONSTANT  zeile_0_ende   : std_logic_vector(7 DOWNTO 0) := "10100000"; 
  CONSTANT  zeile_1_ende   : std_logic_vector(7 DOWNTO 0) := "11100000";
  
BEGIN
  zustandsspeicher : PROCESS(i, clk, addr_ddram)  --, addr0_ddram, addr1_ddram)
  BEGIN
    IF (clk'event AND clk = '1') THEN
      momentan <= folge;
      IF (en_i = true) THEN
        i      <= i + 1;
      ELSIF (en_i = false) THEN
        i      <= 0;
      END IF;
      IF    (en_a = true  AND clr_0_a = false AND clr_1_a = false) THEN
        addr_ddram <= addr_ddram + 1;
      ELSIF (en_a = false AND clr_0_a = true  AND clr_1_a = false) THEN
        addr_ddram <= zeile_0_anfang;
      ELSIF (en_a = false AND clr_0_a = false AND clr_1_a = true ) THEN
        addr_ddram <= zeile_1_anfang;
      ELSIF (en_a = false AND clr_0_a = false AND clr_1_a = false ) THEN
        addr_ddram <= addr_ddram;           
      ELSIF (en_a = false AND clr_0_a = true AND clr_1_a = true ) THEN
        addr_ddram <= addr_ddram;
      ELSIF (en_a = true AND clr_0_a = false AND clr_1_a = true ) THEN
        addr_ddram <= addr_ddram;           
      ELSIF (en_a = true AND clr_0_a = true AND clr_1_a = false ) THEN
        addr_ddram <= addr_ddram;           
      ELSIF (en_a = true AND clr_0_a = true AND clr_1_a = true ) THEN
        addr_ddram <= addr_ddram;
      END IF;
    END IF;
  END PROCESS;

  uebergangslogik : PROCESS(momentan, addr_ddram, do, i, en_i, en_a, clr_0_a, clr_1_a)
  BEGIN
    CASE momentan IS
-- Initialisierung:
------------------
-- nach power on 50ms warten
      WHEN warten_power_on =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0';
        LCD_Rw  <= '0';
        LCD_E   <= '0';
        LCD_DB  <= "00000000";
        IF(i = i_2500000)               -- 50ms 
        THEN
          en_i  <= false;
          en_a <= false;
          clr_0_a <= false;
          clr_1_a <= false;
          folge <= function_set1;
        ELSE
          en_i  <= true;
          en_a <= false;
          clr_0_a <= false;
          clr_1_a <= false;
          folge <= warten_power_on;
        END IF;

-- Function set:
-- RS R/W DB7 DB6 DB5 DB4 DB3 DB2 DB1 DB0
--  0  0  0   0   1   DL  N   F   X   X
-- DL='1' : 8 bit Datenbus
-- N ='1' : zwei Zeilen
-- F ='0' : 5x8 Punktmatrix
-- DB="0011 1000"
      WHEN function_set1 =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0';
        LCD_Rw  <= '0';
        LCD_E   <= '1';
        LCD_DB  <= "00111000";
        IF(i = i_2000)
        THEN
          en_i  <= false;
          folge <= warten1;
        ELSE
          en_i  <= true;
          folge <= function_set1;
        END IF;
        
-- 40us warten:
       WHEN warten1 =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0';  
        LCD_Rw  <= '0';
        LCD_E   <= '0';
        LCD_DB  <= "00111000";
        IF(i = i_2000)   
        THEN
          en_i  <= false;
          folge <= function_set2;
        ELSE
          en_i  <= true;
          folge <= warten1;
        END IF;

-- Function set wiederholen:
      WHEN function_set2 =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0';  
        LCD_Rw  <= '0';
        LCD_E   <= '1';
        LCD_DB  <= "00111000";
        IF(i = i_2000)
        THEN
          en_i  <= false;
          folge <= warten2;
        ELSE
          en_i  <= true;
          folge <= function_set2;
        END IF;

-- 40us warten:
      WHEN warten2 =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0'; 
        LCD_Rw  <= '0';
        LCD_E   <= '0';
        LCD_DB  <= "00111000";
        IF(i = i_2000) 
        THEN
          en_i  <= false;
          folge <= display_on;
        ELSE
          en_i  <= true;
          folge <= warten2;
        END IF;

-- Display ON/OFF control
-- RS R/W DB7 DB6 DB5 DB4 DB3 DB2 DB1 DB0
-- 0 0 0 0 0 0 1 D C B
-- D=1 display ON
-- C=0 cursor OFF
-- B=0 blink OFF
-- DB="00001100"
      WHEN display_on =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0'; 
        LCD_Rw  <= '0';
        LCD_E   <= '1';
        LCD_DB  <= "00001100";
        IF(i = i_2000)
        THEN
          en_i  <= false;
          folge <= warten3;
        ELSE
          en_i  <= true;
          folge <= display_on;
        END IF;

-- 40us warten:
      WHEN warten3 =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0'; 
        LCD_Rw  <= '0';
        LCD_E   <= '0';
        LCD_DB  <= "00001100";
        IF(i = i_2000) 
        THEN
          en_i  <= false;
          folge <= display_clear;
        ELSE
          en_i  <= true;
          folge <= warten3;
        END IF;
        
-- Display clear
-- RS R/W DB7 DB6 DB5 DB4 DB3 DB2 DB1 DB0
-- 0 0 0 0 0 0 0 0 0 1
-- DB="00000001"
      WHEN display_clear =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0';
        LCD_Rw  <= '0';
        LCD_E   <= '1';
        LCD_DB  <= "00000001";
        IF(i = i_2000) 
        THEN
          en_i  <= false;
          folge <= warten4;
        ELSE
          en_i  <= true;
          folge <= display_clear;
        END IF;
        
-- 1,6ms warten:
      WHEN warten4 =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0';
        LCD_Rw  <= '0';
        LCD_E   <= '0';
        LCD_DB  <= "00000001";
        IF(i = i_82000)   
        THEN
          en_i  <= false;
          folge <= entry_mode;
        ELSE
          en_i  <= true;
          folge <= warten4;
        END IF;

-- Entry mode set
-- RS R/W DB7 DB6 DB5 DB4 DB3 DB2 DB1 DB0
-- 0 0 0 0 0 0 0 1 I/D S
-- I/D=1 DDRAM-Adresse inkrementieren
-- S =0 nicht schieben
-- DB="00000110"
      WHEN entry_mode =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0'; 
        LCD_Rw  <= '0';
        LCD_E   <= '1';
        LCD_DB  <= "00000110";
        IF(i = i_25)  
        THEN
          en_i  <= false;
          folge <= warten5;
        ELSE
          en_i  <= true;
          folge <= entry_mode;
        END IF;

-- 40us warten:
      WHEN warten5 =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0';   
        LCD_Rw  <= '0';
        LCD_E   <= '0';
        LCD_DB  <= "00000110";
        IF(i = i_2000) 
        THEN
          en_i  <= false;
          folge <= write_00_addr;
        ELSE
          en_i  <= true;
          folge <= warten5;
        END IF;

-- Schreibzugriffe auf DDRAM:
----------------------------

-- Adresse einstellen:
-- schreiben: E=0, 500ns warten
      WHEN write_00_addr =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0';
        LCD_Rw  <= '0';
        LCD_E   <= '0';
        LCD_DB  <= addr_ddram;
        IF(i = i_25)
        THEN
          en_a <= false;
          en_i  <= false;
          clr_0_a <= true;
          clr_1_a <= false;
          folge <= write_0_addr;
        ELSE
          en_i  <= true;
          en_a <= false;
          clr_0_a <= false;
          clr_1_a <= false;
          folge <= write_00_addr;
        END IF;

-- schreiben: E=0, 500ns warten
      WHEN write_01_addr =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0'; 
        LCD_Rw  <= '0';
        LCD_E   <= '0';
        LCD_DB  <= addr_ddram; 
        IF(i = i_25)
        THEN
          en_i  <= false;
          en_a <= false;
          clr_0_a <= false;
          clr_1_a <= true;
          folge <= write_0_addr;
        ELSE
          en_i  <= true;
          en_a <= false;
          clr_0_a <= false;
          clr_1_a <= false;
          folge <= write_01_addr;
        END IF;

-- schreiben: E=1, 500ns warten
       WHEN write_0_addr =>
         en_a <= false;
         clr_0_a <= false;
         clr_1_a <= false;
         LCD_RS  <= '0';  
         LCD_Rw  <= '0';
         LCD_E   <= '1';
         LCD_DB  <= addr_ddram; 
         IF(i = i_25)
         THEN
           en_i  <= false;
           en_a <= false;
           clr_0_a <= false;
           clr_1_a <= false;
           folge <= write_2_addr;
         ELSE
           en_i  <= true;
           en_a <= false;
           clr_0_a <= false;
           clr_1_a <= false;
           folge <= write_0_addr;
         END IF;

-- schreiben: E=0, 800ns warten
      WHEN write_2_addr =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '0'; 
        LCD_Rw  <= '0';
        LCD_E   <= '0';
        LCD_DB  <= do; 
        IF(i = i_2000)
        THEN
          en_a <= false;
          en_i  <= false;
          clr_0_a <= false;
          clr_1_a <= false;
          folge <= write_0_data;
        ELSE
          en_i  <= true;
          en_a <= false;
          clr_0_a <= false;
          clr_1_a <= false;
          folge <= write_2_addr;
        END IF;
------------------------------------------------

-- Daten schreiben:
-- schreiben: E=1, 500ns warten
      WHEN write_0_data =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '1'; 
        LCD_Rw  <= '0';
        LCD_E   <= '1';
        LCD_DB  <= do; 
        IF(i = i_25)
        THEN
          en_a <= false;
          en_i  <= false;
          clr_0_a <= false;
          clr_1_a <= false;
          folge <= write_2_data;
        ELSE
          en_i  <= true;
          en_a <= false;
          clr_0_a <= false;
          clr_1_a <= false;
          folge <= write_0_data;
        END IF;
        
-- schreiben: E=0, 40us warten
      WHEN write_2_data =>
        en_a <= false;
        clr_0_a <= false;
        clr_1_a <= false;
        LCD_RS  <= '1'; 
        LCD_Rw  <= '0';
        LCD_E   <= '0';
        LCD_DB  <= do; 
        IF(i = i_2000)
        THEN
          en_a <= true;
          en_i  <= false;
          clr_0_a <= false;
          clr_1_a <= false;
          CASE addr_ddram IS
            WHEN zeile_0_ende =>
              folge <= write_01_addr;
            WHEN zeile_1_ende =>
              folge <= write_00_addr;
            WHEN OTHERS =>
              folge <= write_0_data;
          END CASE;               
        ELSE
          en_i  <= true;
          en_a <= false;
          clr_0_a <= false;
          clr_1_a <= false;
          folge <= write_2_data;
        END IF;
    END CASE;
  END PROCESS;
---------------------------------------------
  
  sel <= addr_ddram(6)&addr_ddram(3 downto 0);
  
  case_mux : PROCESS (sel, lcd_data) 
  BEGIN
    CASE sel IS
      WHEN "00000" => do <= lcd_data(0,0);
      WHEN "00001" => do <= lcd_data(0,1);
      WHEN "00010" => do <= lcd_data(0,2);
      WHEN "00011" => do <= lcd_data(0,3);
      WHEN "00100" => do <= lcd_data(0,4);
      WHEN "00101" => do <= lcd_data(0,5);
      WHEN "00110" => do <= lcd_data(0,6);
      WHEN "00111" => do <= lcd_data(0,7);
      WHEN "01000" => do <= lcd_data(0,8);
      WHEN "01001" => do <= lcd_data(0,9);
      WHEN "01010" => do <= lcd_data(0,10);
      WHEN "01011" => do <= lcd_data(0,11);
      WHEN "01100" => do <= lcd_data(0,12);
      WHEN "01101" => do <= lcd_data(0,13);
      WHEN "01110" => do <= lcd_data(0,14);
      WHEN "01111" => do <= lcd_data(0,15);
      WHEN "10000" => do <= lcd_data(1,0);
      WHEN "10001" => do <= lcd_data(1,1);
      WHEN "10010" => do <= lcd_data(1,2);
      WHEN "10011" => do <= lcd_data(1,3);
      WHEN "10100" => do <= lcd_data(1,4);
      WHEN "10101" => do <= lcd_data(1,5);
      WHEN "10110" => do <= lcd_data(1,6);
      WHEN "10111" => do <= lcd_data(1,7);
      WHEN "11000" => do <= lcd_data(1,8);
      WHEN "11001" => do <= lcd_data(1,9);
      WHEN "11010" => do <= lcd_data(1,10);
      WHEN "11011" => do <= lcd_data(1,11);
      WHEN "11100" => do <= lcd_data(1,12);
      WHEN "11101" => do <= lcd_data(1,13);
      WHEN "11110" => do <= lcd_data(1,14);
      WHEN "11111" => do <= lcd_data(1,15);
      WHEN OTHERS  => do <= "01011100";
    END CASE;
  END PROCESS;
END;

