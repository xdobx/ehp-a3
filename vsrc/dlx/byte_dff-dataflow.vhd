-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
--   Dataflow architecture for edge triggered flipflop (8-bit)
--  (file byte_dff-dataflow.vhd)
--------------------------------------------------------------

ARCHITECTURE dataflow OF byte_dff IS

BEGIN
  
  PROCESS (clk, clk_en, d)
  BEGIN 
    IF (clk'event AND clk = '1') THEN 
      IF (clk_en = '1') THEN
        q <= To_UX01(d);
      END IF;
    END IF;
  END PROCESS;

END dataflow; 
