-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Entity declaration of a 4k-Byte memory module
--  implemented with internal Virtex-E Block SelectRAM
--
--  (file mem-structural.vhd)
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;

--synopsys translate_off
LIBRARY UNISIM;
USE UNISIM.ALL;
-- spartan:
----------
-- use UNISIM.vcomponents.all;

--synopsys translate_on

USE WORK.dlx_types.ALL;

ENTITY mem IS
  
  PORT (
-- DLX-Processor: 
    wea         : IN    std_logic;                      -- write enable port A
    ena         : IN    std_logic;                      -- enable port A
    clka        : IN    std_logic;                      -- clock port A
    addra       : IN    std_logic_vector(9 DOWNTO 0);   -- address port A 
    dia         : IN    std_logic_vector(31 DOWNTO 0);  -- data port A
    doa         : OUT   std_logic_vector(31 DOWNTO 0);  -- data port A
--    oena        : IN    std_logic;                      -- output enable port A
-- Interface:
    web         : IN    std_logic;                      -- write enable port B
    enb         : IN    std_logic_vector(0 TO 3);       -- enable port B
    clkb        : IN    std_logic;                      -- clock port B
    addrb       : IN    std_logic_vector(9 DOWNTO 0);   -- address port B 
    dib         : IN    std_logic_vector(7 DOWNTO 0);   -- data input port A
    dob         : OUT   std_logic_vector(7 DOWNTO 0)    -- data output port A
    );
END mem;
































