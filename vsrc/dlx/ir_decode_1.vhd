-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Entity declaration for the instruction decoder 1
--  (decoder 1 decodes the instructions, load/store instr. are decoded
--   further in decoder 2)
--
--  file ir_decode_1.vhd
-- ------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;
USE WORK.dlx_instructions.ALL;
USE WORK.control_types.ALL;

ENTITY ir_decode_1 IS 

  PORT (
    instr_in : IN  dlx_word;
    dec1_out : OUT fsm_states);
END ir_decode_1;


