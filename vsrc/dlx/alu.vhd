-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
--  Entity declaration for the alu 
--  (file alu.vhd)
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;

ENTITY alu IS
    
  PORT (
    s1       : IN  dlx_word;
    s2       : IN  dlx_word;
    phi1     : IN  std_logic;
    result   : OUT dlx_word;
    func     : IN  alu_func_type;
    zero     : OUT std_logic;
    negative : OUT std_logic
    );
END alu;



