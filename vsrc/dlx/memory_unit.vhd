-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
--   Entity declaration for the internal memory  
--   Dual-port RAM,
--   from core side used as:
--   4k-Byte ROM (program memory)
--   4k-Byte RAM (data memory)
--
--   (file memory.vhd)
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
--synopsys translate_off
LIBRARY UNISIM;
USE UNISIM.ALL;
--synopsys translate_on

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;

ENTITY memory_unit IS
  PORT (
    -- core:
    a_bus               : IN  dlx_address; -- address mux output (from PC/MAR)
    d_bus_in            : OUT dlx_word;    -- data input (to data mux and IR)
    d_bus_out           : IN  dlx_word;    -- data output
    enable              : IN  std_logic;
    rw                  : IN  std_logic;
    -- uc:
    mem_dob             : OUT std_logic_vector( 7 DOWNTO 0);
    uc_port_b           : IN  std_logic_vector( 7 DOWNTO 0);
    mem_addrb           : IN  std_logic_vector( 9 DOWNTO 0);
    rom_web             : IN  std_logic;
    rom_enb             : IN  std_logic_vector( 0 TO 3);
    ram_web             : IN  std_logic;
    ram_enb             : IN  std_logic_vector( 0 TO 3);
    phi2                : IN  std_logic;
    phi_uc              : IN  std_logic;
    -- display:
    disp_latch          : OUT dlx_word
    ); 
                                        
END memory_unit ;










