-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
--   Architecture for the alu core
--   a fast version 
--   (file alu_core-behaviour.vhd)
-- -----------------------------------------------------------

ARCHITECTURE behaviour OF alu_core IS

 -- SIGNAL tmp_res : std_logic_vector(0 TO 31);

BEGIN
  
-- specify the output to change on a change to any input
  s1_func_s2: PROCESS(stored_s1, stored_s2, alu_op)
    VARIABLE tmp_res : std_logic_vector(0 TO 31);
  BEGIN
    CASE alu_op IS
      WHEN alu_pass_s1
        =>      tmp_res := stored_s1;
      WHEN alu_pass_s2
        =>      tmp_res := stored_s2;                
      WHEN alu_s1_add_s2
        =>      sv_add(stored_s1, stored_s2, tmp_res);
      WHEN alu_s1_sub_s2
        =>      sv_sub(stored_s1, stored_s2, tmp_res);

-- another possibility:
--      WHEN alu_s1_add_s2
--        =>      tmp_res := stored_s1 + stored_s2;
--      WHEN alu_s1_sub_s2
--        =>      tmp_res := stored_s1 - stored_s2;

      WHEN alu_s1_and_s2
        =>      tmp_res := stored_s1 AND stored_s2;
      WHEN alu_s1_or_s2
        =>      tmp_res := stored_s1 OR stored_s2;
      WHEN alu_sll_s1_s2
        =>      sv_sll(stored_s1, tmp_res, stored_s2(27 to 31));
      WHEN alu_srl_s1_s2     
        =>      sv_srl(stored_s1, tmp_res, stored_s2(27 to 31));
      WHEN OTHERS
        =>      NULL;
    END CASE;
    negative  <= tmp_res(0);
    result    <= tmp_res;

  END PROCESS s1_func_s2;

  -- negative  <= tmp_res(0);
  zero      <= '1' WHEN (stored_s1 = 0) ELSE '0';
  -- result    <= tmp_res;
       
END behaviour;
