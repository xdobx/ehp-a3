-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--   Structural architecture for the register file (32 x 32-bit-reg.)
--   one input, two outputs 
--
--  (file reg_fileram-structural.vhd)
-- -----------------------------------------------------------

ARCHITECTURE structural OF reg_fileram IS

--  COMPONENT RAM16X1D_1 -- virtexe
  COMPONENT  RAM16X1D -- spartan
    --synopsys synthesis_off
    GENERIC (INIT : bit_vector(15 DOWNTO 0));
    --synopsys synthesis_on
  PORT(
      D         : IN std_logic;
      A3        : IN std_logic;
      A2        : IN std_logic;
      A1        : IN std_logic;
      A0        : IN std_logic;
      DPRA3     : IN std_logic;
      DPRA2     : IN std_logic;
      DPRA1     : IN std_logic;
      DPRA0     : IN std_logic;
      WE        : IN std_logic;
      WCLK 	: IN std_logic;
      SPO 	: OUT std_logic;
      DPO 	: OUT std_logic);
  END COMPONENT;

  SIGNAL  addr_a        : dlx_reg_addr;
  SIGNAL  q1_low        : dlx_word;
  SIGNAL  q1_high	: dlx_word;
  SIGNAL  q2_low	: dlx_word;
  SIGNAL  q2_high	: dlx_word;
  SIGNAL  q1_int	: dlx_word;
  SIGNAL  q2_int	: dlx_word;
  SIGNAL  neg_write_en  : std_logic;
  SIGNAL  we_low	: std_logic;
  SIGNAL  we_high	: std_logic;

BEGIN

----------------------------------------------------
-- Component Instantiation 
----------------------------------------------------
  rf_low : FOR i IN 0 TO 31 GENERATE
    -- ram_l : RAM16X1D_1 -- virtexe
    ram_l : RAM16X1D -- spartan     
    GENERIC MAP (INIT => X"1234")
      PORT MAP (
	D	=> d(i),
	A3	=> addr_a(3),
	A2	=> addr_a(2),
	A1	=> addr_a(1),
	A0	=> addr_a(0),
	DPRA3	=> addr_out2(3),
	DPRA2	=> addr_out2(2),
	DPRA1	=> addr_out2(1),
	DPRA0	=> addr_out2(0),
	WE	=> we_low,
	WCLK	=> phi2,
	SPO	=> q1_low(i),
	DPO	=> q2_low(i));
  END GENERATE;

  rf_high : FOR i IN 0 TO 31 GENERATE
    -- ram_h : RAM16X1D_1 -- virtexe
    ram_h : RAM16X1D -- spartan
    --synopsys synthesis_off
    GENERIC MAP (INIT => X"0000")      
    --synopsys synthesis_on
      PORT MAP (
	D	=> d(i),
	A3	=> addr_a(3),
	A2	=> addr_a(2),
	A1	=> addr_a(1),
	A0	=> addr_a(0),
	DPRA3	=> addr_out2(3),
	DPRA2	=> addr_out2(2),
	DPRA1	=> addr_out2(1),
	DPRA0	=> addr_out2(0),
	WE	=> we_high,
	WCLK	=> phi2,
	SPO	=> q1_high(i),
	DPO	=> q2_high(i));
  END GENERATE;
-------------------------------------------------------
-- Multiplexer for read/write address A (signal addr_a)
-------------------------------------------------------
  addr_a <= addr_out1 WHEN write_en = '1' ELSE addr_in;

-------------------------------------------------------
-- invert write_en
-------------------------------------------------------
  neg_write_en <= NOT write_en;
  -- high activ WE for component RAM16X1D_1

-------------------------------------------------------
-- select the_dpram_low or the_dpram_high,
-- dependent on address bit 4 
-------------------------------------------------------
  we_low  <= neg_write_en AND NOT addr_in(4);
  we_high <= neg_write_en AND addr_in(4);
  
  q1_int      <= q1_low WHEN addr_out1(4) = '0' ELSE q1_high;
  q2_int      <= q2_low WHEN addr_out2(4) = '0' ELSE q2_high;

--------------------------------------------------------
-- the value of R0 is always 0,
-- a write access do not change its content 
--------------------------------------------------------
  q1 <= (OTHERS => '0') WHEN addr_out1 = "00000" ELSE q1_int;
  q2 <= (OTHERS => '0') WHEN addr_out2 = "00000" ELSE q2_int;
  
END structural;

 




