-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Entity declaration for transparent latch (32-bit) 
--    gate enable,
--    one tri-state output, enabled with out_en  
--
--  (file word_latch_e_1e.vhd)
-- -----------------------------------------------------------
--  gate_en  : high active
--  out_en   : low activ
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

--synopsys translate_off
LIBRARY UNISIM;
USE UNISIM.ALL;
--synopsys translate_on

-- library DLX;
USE WORK.dlx_types.ALL;

ENTITY word_latch_e_1e IS

  PORT (d  : IN dlx_word;
	q  : OUT dlx_word;
        
	gate_en, gate : IN std_logic;
	out_en   : IN std_logic);
END word_latch_e_1e;





