-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
-- Structural architecture of the microcontroller interface
--
-- (file uc_if-structural.vhd)
-- -----------------------------------------------------------

ARCHITECTURE structural OF uc_if IS


  COMPONENT qclk2phi
    PORT (
      clkin       : IN  std_logic;   -- from external crystal oscillator 
      phi1, phi2  : OUT std_logic);  -- two-phase non-overlapping clock
  END COMPONENT;
		
  COMPONENT byte_dff
    PORT (
      d       : IN  std_logic_vector(7 DOWNTO 0);
      q       : OUT std_logic_vector(7 DOWNTO 0);
      clk_en  : IN  std_logic;
      clk     : IN  std_logic);
  END COMPONENT;
  
   COMPONENT IBUF
    PORT (
      I                 : IN  std_logic;        -- pad input
      O                 : OUT std_logic);       -- from pad to core
  END COMPONENT;
  
  COMPONENT IBUFG
    PORT (
      I                 : IN  std_logic;        -- pad input
      O                 : OUT std_logic);       -- from pad to core
  END COMPONENT;

  COMPONENT BUFG
    PORT (
      I                 : IN  std_logic;        -- pad input
      O                 : OUT std_logic);       -- from pad to core
  END COMPONENT;

  COMPONENT OBUF_F_12
    PORT (
      I                 : IN  std_logic;        -- from core to pad
      O                 : OUT std_logic);       -- pad out
  END COMPONENT;
  
  COMPONENT disp_mux
    PORT (
      core_addr_out          : IN  dlx_address;  -- address out to memory
      core_data_in           : IN  dlx_word;     -- data in from memory
      core_data_out          : IN  dlx_word;     -- data out to memory
      display_latch_out      : IN  dlx_word;     -- result latch out
      s1_bus_display         : IN  dlx_word;     -- s1_bus for display only
      s2_bus_display         : IN  dlx_word;     -- s2_bus for display only
      dest_bus_display       : IN  dlx_word;     -- dest_bus for display only
      ir_reg_display         : IN  dlx_word;     -- ir output  for display only
      sel                    : IN  std_logic_vector(2 DOWNTO 0); -- select lines
      y                      : OUT dlx_word);     -- output 
  END COMPONENT;

  SIGNAL port_a_sig             : std_logic_vector( 7 DOWNTO 0);
  SIGNAL port_b_sig             : std_logic_vector( 7 DOWNTO 0);
  SIGNAL port_c_sig             : std_logic_vector( 7 DOWNTO 0);
  SIGNAL addr_low_we            : std_logic;
  SIGNAL addr_high_we           : std_logic;
  SIGNAL mem_reg_en             : std_logic;
  SIGNAL res_hlt_reg_en         : std_logic;
  SIGNAL core_clk_sel_reg_en    : std_logic;
  SIGNAL disp_sel_reg_en        : std_logic;
  SIGNAL core_clk_sel           : std_logic_vector( 2 DOWNTO 0);
  SIGNAL rom_en_sig             : std_logic_vector( 0 TO 9);
  SIGNAL ram_en_sig             : std_logic_vector( 0 TO 9);
  SIGNAL rom_we_sig             : std_logic;
  SIGNAL rom_rd_sig             : std_logic;
  SIGNAL mem_en_sig             : std_logic_vector( 0 TO 3);
  SIGNAL bus_ucrd_sel           : std_logic_vector( 2 DOWNTO 0);
  SIGNAL ram_we_sig             : std_logic;
  SIGNAL ram_rd_sig             : std_logic;
  SIGNAL phi1_bufg              : std_logic;    -- two-phase core clock 
  SIGNAL phi2_bufg              : std_logic;    -- two-phase core clock 
  SIGNAL phi1_dll               : std_logic;    -- two-phase crystal clock  
  SIGNAL phi2_dll               : std_logic;    -- two-phase crystal clock  
  SIGNAL phi_uc_sig             : std_logic;    -- uc generated clock 
  SIGNAL phi_q_sig              : std_logic;    -- crystal clock 
  SIGNAL bus_ucrd_sig           : dlx_word;
  SIGNAL bus_ucrd_sel_reg_en    : std_logic;
  SIGNAL reg_uc_reg_en          : std_logic;
  CONSTANT low_const            : std_logic := '0';         
  SIGNAL low_sig                : std_logic;         
  SIGNAL states_sig             : std_logic_vector(7 DOWNTO 0);  
  
BEGIN        

  low_sig  <= low_const;
  
  addr_low_we         <= '1' WHEN port_c_sig = "00110000" ELSE '0'; -- 0x30
  addr_high_we        <= '1' WHEN port_c_sig = "00110001" ELSE '0'; -- 0x31

  mem_reg_en          <= '1' WHEN port_c_sig = "01000000" ELSE '0'; -- 0x40
  res_hlt_reg_en      <= '1' WHEN port_c_sig = "01010000" ELSE '0'; -- 0x50
  core_clk_sel_reg_en <= '1' WHEN port_c_sig = "01100000" ELSE '0'; -- 0x60
  disp_sel_reg_en     <= '1' WHEN port_c_sig = "01110000" ELSE '0'; -- 0x70
  bus_ucrd_sel_reg_en <= '1' WHEN port_c_sig = "10000000" ELSE '0'; -- 0x80
  reg_uc_reg_en       <= '1' WHEN port_c_sig = "10100000" ELSE '0'; -- 0xA0
  
 --  Bus multiplexer for output via uc:
  mux_bus_ucrd : disp_mux  
    PORT MAP (
      core_addr_out          => a_bus,
      core_data_in           => d_bus_in,
      core_data_out          => d_bus_out,
      display_latch_out      => disp_latch_out, 
      s1_bus_display         => s1_bus,
      s2_bus_display         => s2_bus, 
      dest_bus_display       => dest_bus, 
      ir_reg_display         => instr_out,
      sel                    => bus_ucrd_sel,
      y                      => bus_ucrd_sig); 

  rom_en_sig <= port_c_sig & rom_we_sig & rom_rd_sig;

  rom_enb_0 : PROCESS (rom_en_sig)
   BEGIN
     CASE rom_en_sig IS
       WHEN "0001000010" => rom_enb(0) <= '1';
       WHEN "0001000001" => rom_enb(0) <= '1';
       WHEN OTHERS       => rom_enb(0) <= '0';                
     END CASE;
   END PROCESS rom_enb_0;
  rom_enb_1 : PROCESS (rom_en_sig)
   BEGIN
     CASE rom_en_sig IS
       WHEN "0001000110" => rom_enb(1) <= '1';
       WHEN "0001000101" => rom_enb(1) <= '1';
       WHEN OTHERS       => rom_enb(1) <= '0';                
     END CASE;
   END PROCESS rom_enb_1;
  rom_enb_2 : PROCESS (rom_en_sig)
   BEGIN
     CASE rom_en_sig IS
       WHEN "0001001010" => rom_enb(2) <= '1';
       WHEN "0001001001" => rom_enb(2) <= '1';
       WHEN OTHERS       => rom_enb(2) <= '0';                
     END CASE;
   END PROCESS rom_enb_2;
  rom_enb_3 : PROCESS (rom_en_sig)
   BEGIN
     CASE rom_en_sig IS
       WHEN "0001001110" => rom_enb(3) <= '1';
       WHEN "0001001101" => rom_enb(3) <= '1';
       WHEN OTHERS       => rom_enb(3) <= '0';                
     END CASE;
   END PROCESS rom_enb_3;

  ram_en_sig <= port_c_sig & ram_we_sig & ram_rd_sig;

  ram_enb_0 : PROCESS (ram_en_sig)
   BEGIN
     CASE ram_en_sig IS
       WHEN "0001000010" => ram_enb(0) <= '1';
       WHEN "0001000001" => ram_enb(0) <= '1';
       WHEN OTHERS       => ram_enb(0) <= '0';                
     END CASE;
   END PROCESS ram_enb_0;
  ram_enb_1 : PROCESS (ram_en_sig)
   BEGIN
     CASE ram_en_sig IS
       WHEN "0001000110" => ram_enb(1) <= '1';
       WHEN "0001000101" => ram_enb(1) <= '1';
       WHEN OTHERS       => ram_enb(1) <= '0';                
     END CASE;
   END PROCESS ram_enb_1;
  ram_enb_2 : PROCESS (ram_en_sig)
   BEGIN
     CASE ram_en_sig IS
       WHEN "0001001010" => ram_enb(2) <= '1';
       WHEN "0001001001" => ram_enb(2) <= '1';
       WHEN OTHERS       => ram_enb(2) <= '0';                
     END CASE;
   END PROCESS ram_enb_2;
  ram_enb_3 : PROCESS (ram_en_sig)
   BEGIN
     CASE ram_en_sig IS
       WHEN "0001001110" => ram_enb(3) <= '1';
       WHEN "0001001101" => ram_enb(3) <= '1';
       WHEN OTHERS       => ram_enb(3) <= '0';                
     END CASE;
   END PROCESS ram_enb_3;


   states_sig(5 DOWNTO 0) <= state;
   states_sig(6) <= low_sig;
   states_sig(7) <= low_sig;
   
   port_a_sig <= bus_ucrd_sig( 0 TO  7) WHEN
                 port_c_sig = "00100011" ELSE "ZZZZZZZZ"; -- bus_ucrd_pos_3 (0x23)
   port_a_sig <= bus_ucrd_sig( 8 TO 15) WHEN
                 port_c_sig = "00100010" ELSE "ZZZZZZZZ"; -- bus_ucrd_pos_2 (0x22)
   port_a_sig <= bus_ucrd_sig(16 TO 23) WHEN
                 port_c_sig = "00100001" ELSE "ZZZZZZZZ"; -- bus_ucrd_pos_1 (0x21)
   port_a_sig <= bus_ucrd_sig(24 TO 31) WHEN
                 port_c_sig = "00100000" ELSE "ZZZZZZZZ"; -- bus_ucrd_pos_0 (0x20)
   --
   port_a_sig <= mem_dob WHEN
                 port_c_sig = "00010000" ELSE "ZZZZZZZZ"; -- data_pos_0 (0x10)
   port_a_sig <= mem_dob WHEN
                 port_c_sig = "00010001" ELSE "ZZZZZZZZ"; -- data_pos_1 (0x11)
   port_a_sig <= mem_dob WHEN
                 port_c_sig = "00010010" ELSE "ZZZZZZZZ"; -- data_pos_2 (0x12)
   port_a_sig <= mem_dob WHEN
                 port_c_sig = "00010011" ELSE "ZZZZZZZZ"; -- data_pos_3 (0x13)
   --
   port_a_sig <= states_sig WHEN
                 port_c_sig = "10010000" ELSE "ZZZZZZZZ"; -- states_ucrd (0x90)
   --
   port_a_sig <= uc_rf_data_out2( 0 TO  7) WHEN
                 port_c_sig = "10110000" ELSE "ZZZZZZZZ"; -- uc_rf_data_out2_pos_0 (0xB0)
   port_a_sig <= uc_rf_data_out2( 8 TO 15) WHEN
                 port_c_sig = "10110001" ELSE "ZZZZZZZZ"; -- uc_rf_data_out2_pos_1 (0xB1)
   port_a_sig <= uc_rf_data_out2(16 TO 23) WHEN
                 port_c_sig = "10110010" ELSE "ZZZZZZZZ"; -- uc_rf_data_out2_pos_2 (0xB2)
   port_a_sig <= uc_rf_data_out2(24 TO 31) WHEN
                 port_c_sig = "10110011" ELSE "ZZZZZZZZ"; -- uc_rf_data_out2_pos_3 (0xB3)

   
  addr_l_r:  byte_dff
    PORT MAP (
      d             => port_b_sig,
      q             => mem_addrb(7 DOWNTO 0),
      clk_en        => addr_low_we,
      clk           => phi_uc_sig 
      );

  addr_h_r : byte_dff
    PORT MAP (
      d             => port_b_sig,
      q(0)          => mem_addrb(8),
      q(1)          => mem_addrb(9),
      q(7 DOWNTO 2) => OPEN,
      clk_en        => addr_high_we,
      clk           => phi_uc_sig 
      );
  
  memaccess_r : byte_dff
    PORT MAP (
      d             => port_b_sig,
      q(0)          => rom_we_sig,
      q(1)          => rom_rd_sig,
      q(3 DOWNTO 2) => OPEN,
      q(4)          => ram_we_sig,
      q(5)          => ram_rd_sig,
      q(7 DOWNTO 6) => OPEN,
      clk_en        => mem_reg_en,
      clk           => phi_uc_sig 
      );
  rom_web <= rom_we_sig;
  ram_web <= ram_we_sig;
  
  reshlt_r : byte_dff
    PORT MAP (
      d              => port_b_sig,
      q(1 DOWNTO 0)  => res_hlt,
      q(7 DOWNTO 2)  => OPEN,
      clk_en         => res_hlt_reg_en,
      clk            => phi_uc_sig
      );
  
  ifclk_r : byte_dff
    PORT MAP (
      d              => port_b_sig,
      q(2 DOWNTO 0)  => core_clk_sel,
      q(7 DOWNTO 3)  => OPEN,      
      clk_en         => core_clk_sel_reg_en,
      clk            => phi_uc_sig 
      );
  
  disp_r : byte_dff
    PORT MAP (
      d              => port_b_sig,
      q(2 DOWNTO 0)  => disp_sel,
      q(7 DOWNTO 3)  => OPEN,      
      clk_en         => disp_sel_reg_en ,
      clk            => phi_uc_sig 
      );
             
  bus_ucrd_r : byte_dff
    PORT MAP (
      d              => port_b_sig,
      q(2 DOWNTO 0)  => bus_ucrd_sel,
      q(7 DOWNTO 3)  => OPEN,      
      clk_en         => bus_ucrd_sel_reg_en ,
      clk            => phi_uc_sig 
      );
  reg_uc_r : byte_dff
    PORT MAP (
      d              => port_b_sig,
      q(4 DOWNTO 0)  => uc_rf_addr_out2,      
      q(5)           => uc_rf_addr_out2_sel,
      q(7 DOWNTO 6)  => uc_a_mux_sel,      
      clk_en         => reg_uc_reg_en ,
      clk            => phi_uc_sig 
      );

   -- register dump access (reg_uc_r):
   --
   --                uc_a  uc_rf  uc_rf 
   --                _mux  _addr  _addr
   --                _sel  _out2  _out2
   --                      _sel
   -- not_reg_dump    00     0    xxxxx  (0x0)
   -- rf-access_0     00     1    00000  (0x20)
   --   .                           .
   --   .                           . 
   --   .                           .
   -- rf-access_31    00     1    11111  (0x3F)
   -- in conjunction with bus dump
   -- for addr_out:
   -- pc_access       10     x    xxxxx  (0x80)
   -- mar_access      11     x    xxxxx  (0xC0)
   -- in conjunction with bus dump
   -- for core_data_out:   
   -- mdr_access      xx     x    xxxxx  (0x0)
   
  ifqclk : qclk2phi
    PORT MAP (
      clkin          => q_clk,
      phi1           => phi1_dll,
      phi2           => phi2_dll);
  --
  -- clock source selection:
  --
  phi1_bufg <=
    core_clk_sel(1) WHEN core_clk_sel(0) = '0' ELSE phi1_dll;
  phi2_bufg <=
    core_clk_sel(2) WHEN core_clk_sel(0) = '0' ELSE phi2_dll;
  --
  -- global buffers:  
  --  
  bufgphi1 : BUFG
    PORT MAP (I => phi1_bufg,
              O => phi1);
  
  bufgphi2 : BUFG
    PORT MAP (I => phi2_bufg,
              O => phi2);
  --
  -- global clock pad cells
  --
--   ibufgifck : IBUFG
--    PORT MAP (
--      I         => uc_clk,
--      O         => phi_uc_sig);
  phi_uc_sig <= uc_clk;
  phi_uc <= phi_uc_sig;
   
  
--  ibufgqck : IBUFG
--    PORT MAP (
--      I         => q_clk,
--      O         => phi_q_sig);
phi_q_sig <= q_clk;
  --
  -- input pad cells
  --
--  uc_b : FOR i IN 7 DOWNTO 0 GENERATE  
--    ibufport_b : IBUF
--      PORT MAP ( 
--        I       => uc_port_b(i),
--        O       => port_b_sig(i));
--  END GENERATE;
port_b_sig <= uc_port_b;
  port_b <= port_b_sig;
  
--  uc_c : FOR i IN 7 DOWNTO 0 GENERATE  
--    ibufport_c : IBUF
--      PORT MAP ( 
--        I       => uc_port_c(i),
--        O       => port_c_sig(i));
--  END GENERATE; 
port_c_sig <= uc_port_c;

  --
  -- output pad cells
  --
--  uc_a : FOR i IN 7 DOWNTO 0 GENERATE  
--    obufport_a : OBUF_F_12
--      PORT MAP ( 
--        I       => port_a_sig(i),
--        O       => uc_port_a(i));
--  END GENERATE;
uc_port_a <= port_a_sig;
END structural;





























