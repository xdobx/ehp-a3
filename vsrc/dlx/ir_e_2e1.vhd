-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
--  Entity declaration for controller
--  (file controller.vhd)
--------------------------------------------------------------
--
-- -----------------------------------------------------------
--  Entity declaration for instruction register
--
--  (file ir.vhd)
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  latch_en    : low active
--  immed_o1_en : low active
--  immed_o2_en : low active
--  immed_size  : '0'-> 16 bit   / '1'-> 26 bit
--------------------------------------------------------------------------

USE WORK.dlx_types.ALL;

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

LIBRARY IEEE_EXTD;
USE IEEE_EXTD.stdl1164_vector_arithmetic.sv_zext,
    IEEE_EXTD.stdl1164_vector_arithmetic.sv_sext;

--synopsys translate_off
LIBRARY UNISIM;
USE UNISIM.ALL;
--synopsys translate_on

ENTITY ir_e_2e1 IS

  PORT (d               : IN dlx_word;
	gate_en, gate   : IN std_logic;
	ir_out          : OUT dlx_word;
	immed_o1_en     : IN std_logic;
 	immed_out1      : OUT dlx_word;
	immed_o2_en     : IN std_logic;
 	immed_out2      : OUT dlx_word;
 	immed_size      : IN std_logic;                 
 	immed_sign      : IN std_logic                 
        );              
END ir_e_2e1;


