-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
--   Entity declaration for the external seven segment interface  
--   (file display_if.vhd)
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
--synopsys translate_off
LIBRARY UNISIM;
USE UNISIM.ALL;
--synopsys translate_on

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;
USE work.lcd_conv.ALL;

ENTITY lcd_if IS
  PORT (
    -- core:
    a_bus               : IN dlx_address;      -- address mux output (from PC/MAR)
    d_bus_in            : IN dlx_word;         -- data input (to data mux and IR)
    d_bus_out           : IN dlx_word;         -- data output
    s1_bus              : IN dlx_word;
    s2_bus              : IN dlx_word;
    dest_bus            : IN dlx_word;
    instr_out           : IN dlx_word;
    disp_latch_out      : IN dlx_word;
    disp_sel            : IN std_logic_vector( 2 DOWNTO 0);
    state               : IN fsm_state_numbers;
    error               : IN std_logic;
    phi1, phi2          : IN std_logic;

    -- display:
    lcd_data             : OUT lcd_array                                         
    );                                         
END lcd_if ;










