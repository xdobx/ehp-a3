-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
--   Entity declaration for edge triggered flipflop (8-bit)
--   (file byte_dff.vhd)
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  clk_en : high active
--------------------------------------------------------------

LIBRARY  IEEE;
USE  IEEE.std_logic_1164.all;

ENTITY byte_dff IS

  PORT (d       : IN  std_logic_vector(7 DOWNTO 0);
      	q       : OUT std_logic_vector(7 DOWNTO 0);
	clk_en  : IN  std_logic;
        clk     : IN  std_logic);
END byte_dff;




