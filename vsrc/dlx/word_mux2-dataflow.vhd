-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Dataflow description of the 32 x 2:1 multiplexor
--  
--  (file word_mux2-dataflow.vhd)
-- -----------------------------------------------------------

ARCHITECTURE dataflow OF word_mux2 IS

BEGIN

--  y <= in0 WHEN sel = '0' ELSE in1;

  y <= in0 WHEN sel = '0' ELSE (OTHERS => 'Z');
  y <= in1 WHEN sel = '1' ELSE (OTHERS => 'Z');

END dataflow;
