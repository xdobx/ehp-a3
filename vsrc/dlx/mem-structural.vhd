-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Structural architecture of a 4k-byte_dff memory module
--  implemented with internal Virtex-E Block SelectRAM
--
--  (file mem-structural.vhd)
-- -----------------------------------------------------------
--
-- ------------------------------------------------------------------------
--
--  DLX - Word (32 bit / Big Endian)
-- ----------------------------------
--  Bit  | 0  1  ... 7 | 8  9  ... 15 | 16  17  ... 23 | 24 25 ... 31 |
--        MSB                                                      LSB
--
--  Memory
-- --------
--  Adr       xxx00         xxx01           xxx02           xxx03        
--  Byte	No. 0	      No. 1	      No. 2	      No. 3
--  Bit  | 0  1  ... 7 | 0  1  ...  7 |  0  1   ...  7 |  0  1 ...  7 |
--
-- ------------------------------------------------------------------------

ARCHITECTURE structural OF mem IS

-- spartran:
-----------  
  COMPONENT RAMB16_S9_S9
     PORT (
       DOA,   DOB   : OUT std_logic_vector(0 TO 7);
       DOPA,  DOPB  : OUT std_logic_vector(0 DOWNTO 0);
       DIA,   DIB   : IN  std_logic_vector(0 TO 7);
       DIPA,  DIPB  : IN  std_logic_vector(0 DOWNTO 0);
       ADDRA, ADDRB : IN  std_logic_vector(10 DOWNTO 0);
       WEA,   WEB   : IN  std_logic;
       ENA,   ENB   : IN  std_logic;
       CLKA,  CLKB  : IN  std_logic;
       SSRA,  SSRB  : IN  std_logic);
   END COMPONENT;

-- virtexe:
--   --
--   -- Virtex Dual-Port Synchronous Block RAM (configured to 8 Bits): 
--   --
--   COMPONENT RAMB4_S8_S8
--     PORT (
--       DOA,   DOB   : OUT std_logic_vector(0 TO 7);
--       DIA,   DIB   : IN  std_logic_vector(0 TO 7);
--       ADDRA, ADDRB : IN  std_logic_vector(8 DOWNTO 0);
--       WEA,   WEB   : IN  std_logic;
--       ENA,   ENB   : IN  std_logic;
--       CLKA,  CLKB  : IN  std_logic;
--       RSTA,  RSTB  : IN  std_logic);
--   END COMPONENT;

  CONSTANT disable_const        : std_logic := '0';
  CONSTANT disable_const_vec    : std_logic_vector := "0";
  CONSTANT enable_const         : std_logic := '1';
  SIGNAL disable_sig            : std_logic ;
  SIGNAL disable_sig_vec        : std_logic_vector(0 DOWNTO 0);
  SIGNAL enable_sig             : std_logic ;
  SIGNAL ena_low, ena_high      : std_logic ;
  SIGNAL wea_int                : std_logic ;
  SIGNAL enb0_low, enb0_high    : std_logic ;
  SIGNAL enb1_low, enb1_high    : std_logic ;
  SIGNAL enb2_low, enb2_high    : std_logic ;
  SIGNAL enb3_low, enb3_high    : std_logic ;
  SIGNAL doa_low                : std_logic_vector(31 DOWNTO 0);
  SIGNAL doa_high               : std_logic_vector(31 DOWNTO 0);
  SIGNAL dob_ramb0_low          : std_logic_vector(7 DOWNTO 0);    
  SIGNAL dob_ramb1_low          : std_logic_vector(7 DOWNTO 0);    
  SIGNAL dob_ramb2_low          : std_logic_vector(7 DOWNTO 0);    
  SIGNAL dob_ramb3_low          : std_logic_vector(7 DOWNTO 0);    
  SIGNAL dob_ramb0_high         : std_logic_vector(7 DOWNTO 0);    
  SIGNAL dob_ramb1_high         : std_logic_vector(7 DOWNTO 0);    
  SIGNAL dob_ramb2_high         : std_logic_vector(7 DOWNTO 0);    
  SIGNAL dob_ramb3_high         : std_logic_vector(7 DOWNTO 0);    
  SIGNAL enb_sel                : std_logic_vector(0 TO 4);    
  SIGNAL addra_sig              : std_logic_vector(10 DOWNTO 0);    
  SIGNAL addrb_sig              : std_logic_vector(10 DOWNTO 0);    
  CONSTANT inputs_8_low_const   : std_logic_vector(7 DOWNTO 0)
    := "00000000";
  SIGNAL inputs_8_low_sig       : std_logic_vector(7 DOWNTO 0);    


BEGIN
  
  disable_sig           <= disable_const;
  disable_sig_vec       <= disable_const_vec;
  enable_sig            <= enable_const;
  inputs_8_low_sig      <= inputs_8_low_const;

  wea_int <= NOT wea;   
  
  doa <= doa_low  WHEN (NOT(addra(9)) AND ena AND wea) = '1' ELSE (OTHERS => 'Z');
  doa <= doa_high WHEN (    addra(9)  AND ena AND wea) = '1' ELSE (OTHERS => 'Z');

  enb_sel <= enb & addrb(9);
  
  addra_sig <= addra(8 DOWNTO 0) & disable_sig & disable_sig;
  addrb_sig <= addrb(8 DOWNTO 0) & disable_sig & disable_sig;

  dob <= dob_ramb0_low  WHEN enb_sel = "10000" ELSE "ZZZZZZZZ";
  dob <= dob_ramb1_low  WHEN enb_sel = "01000" ELSE "ZZZZZZZZ";
  dob <= dob_ramb2_low  WHEN enb_sel = "00100" ELSE "ZZZZZZZZ";
  dob <= dob_ramb3_low  WHEN enb_sel = "00010" ELSE "ZZZZZZZZ";
  dob <= dob_ramb0_high WHEN enb_sel = "10001" ELSE "ZZZZZZZZ";
  dob <= dob_ramb1_high WHEN enb_sel = "01001" ELSE "ZZZZZZZZ";
  dob <= dob_ramb2_high WHEN enb_sel = "00101" ELSE "ZZZZZZZZ";
  dob <= dob_ramb3_high WHEN enb_sel = "00011" ELSE "ZZZZZZZZ";
  
  enb0_low  <= '1' WHEN enb_sel = "10000" ELSE '0' ;
  enb1_low  <= '1' WHEN enb_sel = "01000" ELSE '0' ;
  enb2_low  <= '1' WHEN enb_sel = "00100" ELSE '0' ;
  enb3_low  <= '1' WHEN enb_sel = "00010" ELSE '0' ;
  enb0_high <= '1' WHEN enb_sel = "10001" ELSE '0' ;
  enb1_high <= '1' WHEN enb_sel = "01001" ELSE '0' ;
  enb2_high <= '1' WHEN enb_sel = "00101" ELSE '0' ;
  enb3_high <= '1' WHEN enb_sel = "00011" ELSE '0' ;

--
  -- Port assignment:
  -- RAM Port A ( 32 Bit data , 10 Bit address, 1 wire enable) -> DLX-Processor
  -- RAM Port B (  8 Bit data , 10 Bit address, 4 wire enable) -> Interface
  --
  ena_low      <= ena    AND (NOT addra(9));
  ena_high     <= ena    AND addra(9);

-- spartan:
----------
  
  ramb0_low : RAMB16_S9_S9   
    PORT MAP (
      WEA       => wea_int,
      ENA       => ena_low,
      SSRA      => disable_sig,
      CLKA      => clka,
      ADDRA     => addra_sig, --addra(8 DOWNTO 0) & disable_sig & disable_sig,
      DIA       => dia(7 DOWNTO 0),
      DIPA      => disable_sig_vec,
      DOA       => doa_low(7 DOWNTO 0),
      DOPA      => OPEN,
      WEB       => web,
      ENB       => enb0_low,
      SSRB      => disable_sig,
      CLKB      => clkb,
      ADDRB     => addrb_sig, --addrb(8 DOWNTO 0),
      DIB       => dib,
      DIPB      => disable_sig_vec,
      DOB       => dob_ramb0_low,
      DOPB      => OPEN
      );
  
  ramb0_high : RAMB16_S9_S9   
    PORT MAP (
      WEA       => wea_int,
      ENA       => ena_high,
      SSRA      => disable_sig,
      CLKA      => clka,
      ADDRA     => addra_sig, --addra(8 DOWNTO 0) & disable_sig & disable_sig,
      DIA       => dia(7 DOWNTO 0),
      DIPA      => disable_sig_vec,
      DOA       => doa_high(7 DOWNTO 0),
      DOPA      => OPEN,
      WEB       => web,
      ENB       => enb0_high,
      SSRB      => disable_sig,
      CLKB      => clkb,
      ADDRB     => addrb_sig, --addrb(8 DOWNTO 0),
      DIB       => dib,
      DIPB      => disable_sig_vec,
      DOB       => dob_ramb0_high,
      DOPB      => OPEN
      );
  
  ramb1_low : RAMB16_S9_S9   
    PORT MAP (
      WEA       => wea_int,
      ENA       => ena_low,
      SSRA      => disable_sig,
      CLKA      => clka,
      ADDRA     => addra_sig, --addra(8 DOWNTO 0) & disable_sig & disable_sig,
      DIA       => dia(15 DOWNTO 8),
      DIPA      => disable_sig_vec,
      DOA       => doa_low(15 DOWNTO 8),
      DOPA      => OPEN,
      WEB       => web,
      ENB       => enb1_low,
      SSRB      => disable_sig,
      CLKB      => clkb,
      ADDRB     => addrb_sig, --addrb(8 DOWNTO 0),
      DIB       => dib,
      DIPB      => disable_sig_vec,
      DOB       => dob_ramb1_low,
      DOPB      => OPEN
      );
  
  ramb1_high : RAMB16_S9_S9   
    PORT MAP (
      WEA       => wea_int,
      ENA       => ena_high,
      SSRA      => disable_sig,
      CLKA      => clka,
      ADDRA     => addra_sig, --addra(8 DOWNTO 0) & disable_sig & disable_sig,
      DIA       => dia(15 DOWNTO 8),
      DIPA      => disable_sig_vec,
      DOA       => doa_high(15 DOWNTO 8),
      DOPA      => OPEN,
      WEB       => web,
      ENB       => enb1_high,
      SSRB      => disable_sig,
      CLKB      => clkb,
      ADDRB     => addrb_sig, --addrb(8 DOWNTO 0),
      DIB       => dib,
      DIPB      => disable_sig_vec,
      DOB       => dob_ramb1_high,
      DOPB      => OPEN
      );
  
  ramb2_low : RAMB16_S9_S9   
    PORT MAP (
      WEA       => wea_int,
      ENA       => ena_low,
      SSRA      => disable_sig,
      CLKA      => clka,
      ADDRA     => addra_sig, --addra(8 DOWNTO 0) & disable_sig & disable_sig,
      DIA       => dia(23 DOWNTO 16),
      DIPA      => disable_sig_vec,
      DOA       => doa_low(23 DOWNTO 16),
      DOPA      => OPEN,
      WEB       => web,
      ENB       => enb2_low,
      SSRB      => disable_sig,
      CLKB      => clkb,
      ADDRB     => addrb_sig, --addrb(8 DOWNTO 0),
      DIB       => dib,
      DIPB      => disable_sig_vec,
      DOB       => dob_ramb2_low,
      DOPB      => OPEN
      );
  
  ramb2_high : RAMB16_S9_S9   
    PORT MAP (
      WEA       => wea_int,
      ENA       => ena_high,
      SSRA      => disable_sig,
      CLKA      => clka,
      ADDRA     => addra_sig, --addra(8 DOWNTO 0) & disable_sig & disable_sig,
      DIA       => dia(23 DOWNTO 16),
      DIPA      => disable_sig_vec,
      DOA       => doa_high(23 DOWNTO 16),
      DOPA      => OPEN,
      WEB       => web,
      ENB       => enb2_high,
      SSRB      => disable_sig,
      CLKB      => clkb,
      ADDRB     => addrb_sig, --addrb(8 DOWNTO 0),
      DIB       => dib,
      DIPB      => disable_sig_vec,
      DOB       => dob_ramb2_high,
      DOPB      => OPEN
      );
  
  ramb3_low : RAMB16_S9_S9   
    PORT MAP (
      WEA       => wea_int,
      ENA       => ena_low,
      SSRA      => disable_sig,
      CLKA      => clka,
      ADDRA     => addra_sig, --addra(8 DOWNTO 0) & disable_sig & disable_sig,
      DIA       => dia(31 DOWNTO 24),
      DIPA      => disable_sig_vec,
      DOA       => doa_low(31 DOWNTO 24),
      DOPA      => OPEN,
      WEB       => web,
      ENB       => enb3_low,
      SSRB      => disable_sig,
      CLKB      => clkb,
      ADDRB     => addrb_sig, --addrb(8 DOWNTO 0),
      DIB       => dib,
      DIPB      => disable_sig_vec,
      DOB       => dob_ramb3_low,
      DOPB      => OPEN
      );
  
  ramb3_high : RAMB16_S9_S9   
    PORT MAP (
      WEA       => wea_int,
      ENA       => ena_high,
      SSRA      => disable_sig,
      CLKA      => clka,
      ADDRA     => addra_sig, --addra(8 DOWNTO 0) & disable_sig & disable_sig,
      DIA       => dia(31 DOWNTO 24),
      DIPA      => disable_sig_vec,
      DOA       => doa_high(31 DOWNTO 24),
      DOPA      => OPEN,
      WEB       => web,
      ENB       => enb3_high,
      SSRB      => disable_sig,
      CLKB      => clkb,
      ADDRB     => addrb_sig, --addrb(8 DOWNTO 0),
      DIB       => dib,
      DIPB      => disable_sig_vec,
      DOB       => dob_ramb3_high,
      DOPB      => OPEN
      );

-- virtexe:
----------
--   ramb0_low : RAMB4_S8_S8   
--     PORT MAP (
--       WEA       => wea_int,
--       ENA       => ena_low,
--       RSTA      => disable_sig,
--       CLKA      => clka,
--       ADDRA     => addra(8 DOWNTO 0),
--       DIA       => dia(7 DOWNTO 0),
--       DOA       => doa_low(7 DOWNTO 0),
--       WEB       => web,
--       ENB       => enb0_low,
--       RSTB      => disable_sig,
--       CLKB      => clkb,
--       ADDRB     => addrb(8 DOWNTO 0),
--       DIB       => dib,
--       DOB       => dob_ramb0_low
--       );
  
--   ramb0_high : RAMB4_S8_S8   
--     PORT MAP (
--       WEA       => wea_int,
--       ENA       => ena_high,
--       RSTA      => disable_sig,
--       CLKA      => clka,
--       ADDRA     => addra(8 DOWNTO 0),
--       DIA       => dia(7 DOWNTO 0),
--       DOA       => doa_high(7 DOWNTO 0),
--       WEB       => web,
--       ENB       => enb0_high,
--       RSTB      => disable_sig,
--       CLKB      => clkb,
--       ADDRB     => addrb(8 DOWNTO 0),
--       DIB       => dib,
--       DOB       => dob_ramb0_high
--       );
  
--   ramb1_low : RAMB4_S8_S8   
--     PORT MAP (
--       WEA       => wea_int,
--       ENA       => ena_low,
--       RSTA      => disable_sig,
--       CLKA      => clka,
--       ADDRA     => addra(8 DOWNTO 0),
--       DIA       => dia(15 DOWNTO 8),
--       DOA       => doa_low(15 DOWNTO 8),
--       WEB       => web,
--       ENB       => enb1_low,
--       RSTB      => disable_sig,
--       CLKB      => clkb,
--       ADDRB     => addrb(8 DOWNTO 0),
--       DIB       => dib,
--       DOB       => dob_ramb1_low
--       );
  
--   ramb1_high : RAMB4_S8_S8   
--     PORT MAP (
--       WEA       => wea_int,
--       ENA       => ena_high,
--       RSTA      => disable_sig,
--       CLKA      => clka,
--       ADDRA     => addra(8 DOWNTO 0),
--       DIA       => dia(15 DOWNTO 8),
--       DOA       => doa_high(15 DOWNTO 8),
--       WEB       => web,
--       ENB       => enb1_high,
--       RSTB      => disable_sig,
--       CLKB      => clkb,
--       ADDRB     => addrb(8 DOWNTO 0),
--       DIB       => dib,
--       DOB       => dob_ramb1_high
--       );
  
--   ramb2_low : RAMB4_S8_S8   
--     PORT MAP (
--       WEA       => wea_int,
--       ENA       => ena_low,
--       RSTA      => disable_sig,
--       CLKA      => clka,
--       ADDRA     => addra(8 DOWNTO 0),
--       DIA       => dia(23 DOWNTO 16),
--       DOA       => doa_low(23 DOWNTO 16),
--       WEB       => web,
--       ENB       => enb2_low,
--       RSTB      => disable_sig,
--       CLKB      => clkb,
--       ADDRB     => addrb(8 DOWNTO 0),
--       DIB       => dib,
--       DOB       => dob_ramb2_low
--       );
  
--   ramb2_high : RAMB4_S8_S8   
--     PORT MAP (
--       WEA       => wea_int,
--       ENA       => ena_high,
--       RSTA      => disable_sig,
--       CLKA      => clka,
--       ADDRA     => addra(8 DOWNTO 0),
--       DIA       => dia(23 DOWNTO 16),
--       DOA       => doa_high(23 DOWNTO 16),
--       WEB       => web,
--       ENB       => enb2_high,
--       RSTB      => disable_sig,
--       CLKB      => clkb,
--       ADDRB     => addrb(8 DOWNTO 0),
--       DIB       => dib,
--       DOB       => dob_ramb2_high
--       );
  
--   ramb3_low : RAMB4_S8_S8   
--     PORT MAP (
--       WEA       => wea_int,
--       ENA       => ena_low,
--       RSTA      => disable_sig,
--       CLKA      => clka,
--       ADDRA     => addra(8 DOWNTO 0),
--       DIA       => dia(31 DOWNTO 24),
--       DOA       => doa_low(31 DOWNTO 24),
--       WEB       => web,
--       ENB       => enb3_low,
--       RSTB      => disable_sig,
--       CLKB      => clkb,
--       ADDRB     => addrb(8 DOWNTO 0),
--       DIB       => dib,
--       DOB       => dob_ramb3_low
--       );
  
--   ramb3_high : RAMB4_S8_S8   
--     PORT MAP (
--       WEA       => wea_int,
--       ENA       => ena_high,
--       RSTA      => disable_sig,
--       CLKA      => clka,
--       ADDRA     => addra(8 DOWNTO 0),
--       DIA       => dia(31 DOWNTO 24),
--       DOA       => doa_high(31 DOWNTO 24),
--       WEB       => web,
--       ENB       => enb3_high,
--       RSTB      => disable_sig,
--       CLKB      => clkb,
--       ADDRB     => addrb(8 DOWNTO 0),
--       DIB       => dib,
--       DOB       => dob_ramb3_high
--       );

END structural;









