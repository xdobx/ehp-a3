-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
--   Entity declaration for the alu core
--   a fast version 
--   (file alu_core.vhd)
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_arith.ALL;
--use IEEE.std_logic_unsigned.all;
USE IEEE.std_logic_signed.ALL;
USE WORK.control_types.ALL;
LIBRARY IEEE_EXTD;
USE IEEE_EXTD.stdl1164_vector_arithmetic.ALL;

ENTITY alu_core IS

  PORT(
    stored_s1 : IN  std_logic_vector (0 TO 31);
    stored_s2 : IN  std_logic_vector (0 TO 31);
    alu_op    : IN  alu_func_type;
    result    : OUT std_logic_vector (0 TO 31);
    negative  : OUT std_logic;
    zero      : OUT std_logic);
END alu_core;


