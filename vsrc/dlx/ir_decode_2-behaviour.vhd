-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Behavioural architecture for the instruction decoder 2
--
--  file ir_decode_2-behaviour.vhd
-- -----------------------------------------------------------

ARCHITECTURE behaviour OF ir_decode_2 IS

BEGIN
  decode_2 : PROCESS(instr_in)
  BEGIN
    CASE instr_in(0 TO 5) IS       -- Opcode from IR
      WHEN op_lw_i =>              -- |-> load word 
	dec2_out <= load_w_1;      -- |    -> load_w_1
      WHEN op_sw_i =>              -- |-> store word
	dec2_out <= store_w_1;     -- |    -> store_w_1
      WHEN OTHERS =>               -- |-> other Opcode
        dec2_out <= err_state;     -- |    -> err_state
    END CASE;
  END PROCESS decode_2;    
END behaviour;





