-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--  Structural architecture of the datapath
--
--  file datapath-structural.vhd
--------------------------------------------------------------------------

ARCHITECTURE structural OF datapath IS

  COMPONENT bus_const32
    PORT (
      q1        : OUT dlx_word;
      q2        : OUT dlx_word;
      out_en1   : IN  std_logic;
      out_en2   : IN  std_logic;
      sel       : IN  std_logic_vector(0 TO 1));
  END COMPONENT;

  COMPONENT word_mux2 
    PORT (
      in0, in1  : IN  dlx_word;
      y         : OUT dlx_word;
      sel       : IN  std_logic);
  END COMPONENT;
  
  COMPONENT word_latch_e_1
    PORT (
      d         : IN  dlx_word;
      q         : OUT dlx_word;
      gate_en   : IN  std_logic;
      gate      : IN  std_logic);
  END COMPONENT;

  COMPONENT word_latch_e_1e 
    PORT (
      d         : IN  dlx_word;
      q         : OUT dlx_word;
      gate_en   : IN  std_logic;
      gate      : IN  std_logic;
      out_en    : IN  std_logic);
  END COMPONENT;

  COMPONENT word_latch_e_1e1 
    PORT (
      d         : IN  dlx_word;
      q1        : OUT dlx_word; --BUS;
      q2        : OUT dlx_word; --BUS;
      gate_en   : IN  std_logic;
      gate      : IN  std_logic;
      out_en1   : IN  std_logic);
  END COMPONENT;
  
  COMPONENT reg_fileram 
    PORT (
      addr_out1 : IN  dlx_reg_addr;
      q1        : OUT dlx_word;
      addr_out2 : IN  dlx_reg_addr;
      q2        : OUT dlx_word;
      addr_in   : IN  dlx_reg_addr;
      d         : IN  dlx_word;
      phi2      : IN  std_logic; 
      write_en  : IN  std_logic); 
  END COMPONENT;

  COMPONENT ir_e_2e1 
    PORT (
      d           : IN  dlx_word;
      gate_en     : IN  std_logic;
      gate        : IN  std_logic;
      ir_out      : OUT dlx_word;
      immed_o1_en : IN  std_logic;
      immed_out1  : OUT dlx_word;
      immed_o2_en : IN  std_logic;
      immed_out2  : OUT dlx_word;
      immed_size  : IN  std_logic;      -- '0'-> 16 bit / '1'-> 26 bit
      immed_sign  : IN  std_logic);     -- '0'-> zero / '1'-> sign
  END COMPONENT;

  COMPONENT alu 
    PORT (
      s1        : IN  dlx_word;
      s2        : IN  dlx_word;
      phi1      : IN  std_logic;
      result    : OUT dlx_word;
      func      : IN  alu_func_type;
      zero      : OUT std_logic;
      negative  : OUT std_logic);
  END COMPONENT;
  --
  -- internal busses
  --
  SIGNAL s1_bus_int     : dlx_word;
  SIGNAL s2_bus_int     : dlx_word;
  SIGNAL dest_bus_int   : dlx_word;
  SIGNAL pc_addr_out    : dlx_word;
  SIGNAL mar_addr_out   : dlx_word;
  SIGNAL dmux_data_out  : dlx_word;
  SIGNAL rf_data_out1   : dlx_word;
  SIGNAL rf_data_out2   : dlx_word;
  SIGNAL rf_data_in     : dlx_word;

BEGIN 
  
  aluunit : alu                
    PORT MAP (
      s1        => s1_bus_int,
      s2        => s2_bus_int,
      phi1      => phi1,
      result    => dest_bus_int,
      func      => alu_func,
      zero      => alu_zero,
      negative  => alu_neg);
  
  rf : reg_fileram
    PORT MAP (
      addr_out1 => rf_addr_out1,
      q1        => rf_data_out1,
      addr_out2 => rf_addr_out2,
      q2        => rf_data_out2,
      addr_in   => rf_addr_in,
      d         => rf_data_in,
      write_en  => rf_wr_en,
      phi2      => phi2);

  uc_rf_data_out2 <= rf_data_out2;

  a : word_latch_e_1e
    PORT MAP (d => rf_data_out1,
              q => s1_bus_int,
              gate_en => a_latch_en,
              gate => phi2,
              out_en => a_out_en);
  

  b : word_latch_e_1e
    PORT MAP (d => rf_data_out2,
              q => s2_bus_int,
              gate_en => b_latch_en,
              gate => phi2,
              out_en => b_out_en);


  c : word_latch_e_1
    PORT MAP (d => dest_bus_int,
              q => rf_data_in,
              gate_en => c_latch_en,
              gate => phi2);  

  pc : word_latch_e_1e1
    PORT MAP (d         => dest_bus_int,
              q1        => s2_bus_int,
              q2        => pc_addr_out,
              gate_en   => pc_latch_en,
              gate      => phi2,
              out_en1   => pc_out_en);

  mar : word_latch_e_1e1
    PORT MAP (d         => dest_bus_int,
              q1        => s2_bus_int,
              q2        => mar_addr_out,
              gate_en   => mar_latch_en,
              gate      => phi2,
              out_en1   => mar_out1_en);
  
  mdr : word_latch_e_1e1
    PORT MAP (d         => dmux_data_out,
              q1        => s1_bus_int,
              q2        => data_out,
              gate_en   => mdr_latch_en,
              gate      => phi2,
              out_en1   => mdr_out1_en);
 
  ir : ir_e_2e1
    PORT MAP (d                 => data_in,
              gate_en           => ir_latch_en,
              gate              => phi2,
              ir_out            => instr_out,
	      immed_o1_en       => ir_immed_o1_en,
              immed_out1        => s1_bus_int,
	      immed_o2_en       => ir_immed_o2_en,
              immed_out2        => s2_bus_int,
              immed_size        => ir_immed_size,
              immed_sign        => ir_immed_sign);

  amux : word_mux2
    PORT MAP (
      in0       => pc_addr_out,
      in1       => mar_addr_out,
      y         => addr_out,
      sel       => a_mux_sel);

  dmux : word_mux2
    PORT MAP (
      in0       => dest_bus_int,
      in1       => data_in,
      y         => dmux_data_out,
      sel       => d_mux_sel);

  const : bus_const32
    PORT MAP (
      q1        => s1_bus_int,
      out_en1   => const_o1_en,
      q2        => s2_bus_int,
      out_en2   => const_o2_en,
      sel       => const_sel);

  -- internal busses for display:
  s1_bus       <= s1_bus_int;
  s2_bus       <= s2_bus_int;
  dest_bus     <= dest_bus_int;
  
END structural;




