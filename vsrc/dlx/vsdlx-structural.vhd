-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Structural architecture of the DLXJ Processor System (top)
--
--  V(ery)S(mall)DLX-version for Virtex FPGA
--  
--  (file vsdlx-structural.vhd)
-- -----------------------------------------------------------

ARCHITECTURE structural OF vsdlx IS 

  COMPONENT core
    PORT (
      --
      -- Memory interface
      --
      a_bus             : OUT dlx_address;      -- address mux output (from
                                                -- PC/MAR)
      d_bus_in          : IN  dlx_word;         -- data input (to data mux
                                                -- and IR)
      d_bus_out         : OUT dlx_word;         -- data output
      enable            : OUT std_logic;
      rw                : OUT std_logic;
      --
      -- Display outputs
      --
      s1_disp           : OUT dlx_word;         -- s1_bus for display only
      s2_disp           : OUT dlx_word;         -- s2_bus for display only
      dest_disp         : OUT dlx_word;         -- dest_bus for display only
      instr_disp        : OUT dlx_word;         -- ir output  for display only
      state_number      : OUT fsm_state_numbers; -- display FSM states
      error             : OUT std_logic;
      --
      -- external control signals
      --
      reset             : IN  std_logic;        -- asynchronous reset 
      halt              : IN  std_logic;        -- freeze of processor state
      phi1              : IN  std_logic;        -- 2 phase non overlap. clock
      phi2              : IN  std_logic;
    --
    -- RF-address dump signals:
    --
    uc_rf_data_out2     : OUT dlx_word;  
    uc_rf_addr_out2     : IN dlx_reg_addr;  
    uc_rf_addr_out2_sel : IN std_logic;
    uc_a_mux_sel        : IN std_logic_vector(1 DOWNTO 0));
  END COMPONENT;

  
  COMPONENT memory_unit
    PORT (
      -- core:
      a_bus               : IN  dlx_address;      -- address mux output (from
                                                  -- PC/MAR)
      d_bus_in            : OUT dlx_word;         -- data input (to data mux
                                                  -- and IR)
      d_bus_out           : IN  dlx_word;         -- data output
      enable              : IN  std_logic;
      rw                  : IN  std_logic;
      -- uc:
      mem_dob             : OUT std_logic_vector( 7 DOWNTO 0);
      uc_port_b           : IN  std_logic_vector( 7 DOWNTO 0);
      mem_addrb           : IN  std_logic_vector( 9 DOWNTO 0);
      rom_web             : IN  std_logic;
      rom_enb             : IN  std_logic_vector( 0 TO 3);
      ram_web             : IN  std_logic;
      ram_enb             : IN  std_logic_vector( 0 TO 3);
      phi2                : IN  std_logic;
      phi_uc              : IN  std_logic;
      -- display:
      disp_latch          : OUT dlx_word
    ); 
   END COMPONENT;

  COMPONENT lcd_if
    PORT (
    -- core:
    a_bus               : IN dlx_address;      -- address mux output (from PC/MAR)
    d_bus_in            : IN dlx_word;         -- data input (to data mux and IR)
    d_bus_out           : IN dlx_word;         -- data output
    s1_bus              : IN dlx_word;
    s2_bus              : IN dlx_word;
    dest_bus            : IN dlx_word;
    instr_out           : IN dlx_word;
    disp_latch_out      : IN dlx_word;
    disp_sel            : IN std_logic_vector( 2 DOWNTO 0);
    state               : IN fsm_state_numbers;
    error               : IN std_logic;
    phi1, phi2          : IN std_logic;

    -- display:
    lcd_data             : OUT lcd_array                                         
      ); 
  END COMPONENT;

--   COMPONENT display_if
--     PORT (
--       -- core:
--       a_bus             : IN dlx_address;      -- address mux output (from PC/MAR)
--       d_bus_in          : IN dlx_word;         -- data input (to data mux and IR)
--       d_bus_out         : IN dlx_word;         -- data output
--       s1_bus            : IN dlx_word;
--       s2_bus            : IN dlx_word;
--       dest_bus          : IN dlx_word;
--       instr_out         : IN dlx_word;
--       disp_latch_out    : IN dlx_word;
--       disp_sel          : IN std_logic_vector( 2 DOWNTO 0);
--       state             : IN fsm_state_numbers;
--       error             : IN std_logic;
--       phi1, phi2        : IN std_logic;
--       -- display:
--       disp_8dig         : OUT dlx_word;                      -- display data or address
--                                                              -- (8 digits)
--       disp_2dig         : OUT std_logic_vector(7 DOWNTO 0);  -- display states 
--       disp_8dp          : OUT std_logic_vector(7 DOWNTO 0);  -- display error signal
--                                                              -- (8 decimal points)
--       disp_2dp          : OUT std_logic_vector(1 DOWNTO 0)   -- display two-phase clock
--                                                              -- (2 decimal points)
--       ); 
--   END COMPONENT;

  COMPONENT uc_if
    PORT (
      -- external pad-signals
      uc_port_a           : OUT std_logic_vector( 7 DOWNTO 0);
      uc_port_b           : IN  std_logic_vector( 7 DOWNTO 0);
      uc_port_c           : IN  std_logic_vector( 7 DOWNTO 0);
      uc_clk              : IN  std_logic;
      q_clk               : IN  std_logic;
      -- internal fpga-signals
      mem_dob             : IN  std_logic_vector( 7 DOWNTO 0);
      port_b              : OUT std_logic_vector( 7 DOWNTO 0);
      res_hlt             : OUT std_logic_vector( 1 DOWNTO 0);
      disp_sel            : OUT std_logic_vector( 2 DOWNTO 0);
      mem_addrb           : OUT std_logic_vector( 9 DOWNTO 0);
      rom_web             : OUT std_logic;
      rom_enb             : OUT std_logic_vector( 0 TO 3);
      ram_web             : OUT std_logic;
      ram_enb             : OUT std_logic_vector( 0 TO 3);
      phi1                : OUT std_logic;
      phi2                : OUT std_logic;
      phi_uc              : OUT std_logic;
      a_bus               : IN  dlx_address;      -- address mux output (from PC/MAR)
      d_bus_in            : IN  dlx_word;         -- data input (to data mux and IR)
      d_bus_out           : IN  dlx_word;         -- data output
      s1_bus              : IN  dlx_word;
      s2_bus              : IN  dlx_word;
      dest_bus            : IN  dlx_word;
      instr_out           : IN  dlx_word;
      disp_latch_out      : IN  dlx_word;
      state               : IN  fsm_state_numbers;
      uc_rf_data_out2     : IN  dlx_word;  
      uc_rf_addr_out2     : OUT dlx_reg_addr;  
      uc_rf_addr_out2_sel : OUT std_logic;
      uc_a_mux_sel        : OUT std_logic_vector(1 DOWNTO 0));
  END COMPONENT;

  COMPONENT lcd
    PORT (
    clk                         : IN  std_logic;
    lcd_data                    : IN lcd_array;
    LCD_DB                      : OUT std_logic_vector(7 DOWNTO 0);
    LCD_E, LCD_RS, LCD_RW       : OUT std_logic);
  END COMPONENT;

  COMPONENT IBUFG
    PORT (
      I                 : IN  std_logic;        -- pad input
      O                 : OUT std_logic);       -- from pad to core
  END COMPONENT;

  --
  -- signals
  --
  SIGNAL core_addr        : dlx_address;  -- addr. out 
  SIGNAL core_data_in     : std_logic_vector(31 DOWNTO 0); -- data in 
  SIGNAL core_data_out    : dlx_word;    -- data_out 
  SIGNAL core_s1_bus      : dlx_word;    -- s1_bus for display only
  SIGNAL core_s2_bus      : dlx_word;    -- s2_bus for display only
  SIGNAL core_dest_bus    : dlx_word;    -- dest_bus for display only
  SIGNAL core_instr       : dlx_word;    -- ir output  for display only
  SIGNAL core_states      : fsm_state_numbers;
  SIGNAL core_enable      : std_logic;
  SIGNAL core_rw          : std_logic;
  SIGNAL core_error       : std_logic;
  SIGNAL uc_if_rom_enb    : dlx_nibble;
  SIGNAL uc_if_rom_web    : std_logic;
  SIGNAL uc_if_ram_enb    : dlx_nibble;
  SIGNAL uc_if_ram_web    : std_logic;
  SIGNAL uc_if_mem_addrb  : std_logic_vector( 9 DOWNTO 0);
  SIGNAL uc_if_res_hlt    : std_logic_vector( 1 DOWNTO 0);
  SIGNAL uc_if_disp_sel   : std_logic_vector( 2 DOWNTO 0);
  SIGNAL mem_dob_sig      : std_logic_vector(7 DOWNTO 0);
  SIGNAL uc_if_port_b     : std_logic_vector(7 DOWNTO 0);
  SIGNAL uc_if_phi        : std_logic;
  SIGNAL mem_disp         : dlx_word; 
  SIGNAL phi1             : std_logic;    -- two-phase core clock 
  SIGNAL phi2             : std_logic;    -- two-phase core clock 
  SIGNAL q_clk_sig        : std_logic; 
  SIGNAL uc_clk_sig        : std_logic; 
  SIGNAL uc_rf_data_out2_sig     : dlx_word;  
  SIGNAL uc_rf_addr_out2_sig     : dlx_reg_addr;  
  SIGNAL uc_rf_addr_out2_sel_sig : std_logic;
  SIGNAL uc_a_mux_sel_sig        : std_logic_vector(1 DOWNTO 0);
     --
  --SIGNAL state       : std_logic_vector(7 DOWNTO 0);  -- display states                                                     -- (2 digits)
  --SIGNAL phi1_2_sig  : std_logic_vector(1 DOWNTO 0);  -- display two-phase clock
  --SIGNAL phi1_2_lcd  : std_logic_vector(3 DOWNTO 0);  -- display two-phase clock
  --SIGNAL display     : std_logic_vector(31 DOWNTO 0); -- display data or
   SIGNAL lcd_data_sig       : lcd_array;                                                  -- address (8 digits)
  SIGNAL error       : std_logic_vector(7 DOWNTO 0);  -- display error signal
  
BEGIN
  ibufgqck : IBUFG
    PORT MAP (
      I         => q_clk,       -- Portsignal
      O         => q_clk_sig);  -- int. Signal

  ibufgifck : IBUFG
    PORT MAP (
      I         => uc_clk,      -- Portsignal
      O         => uc_clk_sig); -- int. Signal
state <= core_states;
  phi1_2 <= phi2 & phi1;
  
  lcd_display : lcd
    PORT MAP (
      clk         => q_clk_sig,
      lcd_data    => lcd_data_sig,
      LCD_DB      => LCD_DB,
      LCD_E       => LCD_E,
      LCD_RS      => LCD_RS,
      LCD_RW      => LCD_RW);

  coredlx : core
    PORT MAP ( 
      -- Memory interface
      a_bus             => core_addr,
      d_bus_in          => core_data_in,
      d_bus_out         => core_data_out,
      enable            => core_enable,
      rw                => core_rw,
      -- Display outputs
      s1_disp           => core_s1_bus,     -- s1_bus for display only
      s2_disp           => core_s2_bus,     -- s2_bus for display only
      dest_disp         => core_dest_bus,   -- dest_bus for display only
      instr_disp        => core_instr,      -- ir output  for display only
      state_number      => core_states,
      error             => core_error,
      -- external control signals
      reset             => uc_if_res_hlt(0),
      halt              => uc_if_res_hlt(1),
      phi1              => phi1,
      phi2              => phi2,
      -- for RF dump only
      uc_rf_data_out2     => uc_rf_data_out2_sig,  
      uc_rf_addr_out2     => uc_rf_addr_out2_sig, 
      uc_rf_addr_out2_sel => uc_rf_addr_out2_sel_sig,
      uc_a_mux_sel        => uc_a_mux_sel_sig
    );
  
  memory : memory_unit
    PORT MAP (
      -- core:
      a_bus             => core_addr,        -- address mux output (from PC/MAR)
      d_bus_in          => core_data_in,     -- data input (to data mux and IR)
      d_bus_out         => core_data_out,    -- data output
      enable            => core_enable,
      rw                => core_rw,
      -- uc_if:
      mem_dob           => mem_dob_sig,
      uc_port_b         => uc_if_port_b,
      mem_addrb         => uc_if_mem_addrb,
      rom_web           => uc_if_rom_web,
      rom_enb           => uc_if_rom_enb,
      ram_web           => uc_if_ram_web,
      ram_enb           => uc_if_ram_enb,
      phi2              => phi2,
      phi_uc            => uc_if_phi,
      -- display:
      disp_latch        => mem_disp
      );

  lcd_disp : lcd_if
    PORT MAP (
      -- core:
      a_bus             => core_addr,     -- address mux output (from PC/MAR)
      d_bus_in          => core_data_in,  -- data input (to data mux and IR)
      d_bus_out         => core_data_out, -- data output
      s1_bus            => core_s1_bus,
      s2_bus            => core_s2_bus,
      dest_bus          => core_dest_bus,
      instr_out         => core_instr,
      disp_latch_out    => mem_disp,      
      disp_sel          => uc_if_disp_sel,
      state             => core_states,
      error             => core_error,
      phi1              => phi1,
      phi2              => phi2,
      -- LCD display:
     lcd_data           => lcd_data_sig                                          
      ); 



  
  avr_if : uc_if
    PORT MAP (
    --  
    -- external pad-signals
    --  
      uc_port_a         => uc_port_a,
      uc_port_b         => uc_port_b,
      uc_port_c         => uc_port_c,
      uc_clk            => uc_clk_sig,
      q_clk             => q_clk_sig,
   --   
   -- internal fpga-signals      
   --
      -- DLXJ to Host data transfer 
      port_b            => uc_if_port_b,
      -- processor control                                       
      res_hlt           => uc_if_res_hlt,
      -- display control
      disp_sel          => uc_if_disp_sel,
      -- two phase clock
      phi1              => phi1,
      phi2              => phi2,
      -- dual port memory communication
      phi_uc            => uc_if_phi,
      mem_dob           => mem_dob_sig,
      mem_addrb         => uc_if_mem_addrb,
      rom_web           => uc_if_rom_web,
      rom_enb           => uc_if_rom_enb,
      ram_web           => uc_if_ram_web,
      ram_enb           => uc_if_ram_enb,
      -- for bus dump only
      a_bus             => core_addr,     -- address mux output (from PC/MAR)
      d_bus_in          => core_data_in,  -- data input (to data mux and IR)
      d_bus_out         => core_data_out, -- data output
      s1_bus            => core_s1_bus,
      s2_bus            => core_s2_bus,
      dest_bus          => core_dest_bus,
      instr_out         => core_instr,
      disp_latch_out    => mem_disp,
      -- for states dump only
      state             => core_states,
      -- for register dump only
      uc_rf_data_out2     => uc_rf_data_out2_sig,  
      uc_rf_addr_out2     => uc_rf_addr_out2_sig, 
      uc_rf_addr_out2_sel => uc_rf_addr_out2_sel_sig,
      uc_a_mux_sel        => uc_a_mux_sel_sig
      );

END structural;









