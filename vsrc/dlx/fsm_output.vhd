-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Entity declaration for the finite state machine output logic
--
--  file fsm_output.vhd
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;

ENTITY fsm_output IS 

  PORT (
    current_state : IN 	fsm_states;               -- the current state
    s1_enab       : OUT std_logic_vector(0 TO 3); -- select s1 source
    s2_enab	  : OUT std_logic_vector(0 TO 5); -- select s2_source
    dest_enab     : OUT std_logic_vector(0 TO 3); -- select destination
    alu_op_sel	  : OUT alu_func_type;            -- alu operation
    const_sel     : OUT std_logic_vector(0 TO 1); -- select const for s1
    rf_op_sel     : OUT std_logic_vector(0 TO 1); -- select reg file op.
    immed_sel     : OUT std_logic_vector(0 TO 1); -- select immed. from ir
    mem_ctrl      : OUT std_logic_vector(0 TO 5); -- memory control lines
    state_number  : OUT fsm_state_numbers         -- state numbers
    );
END fsm_output;











