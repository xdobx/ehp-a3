-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
--  Structural architecture of the controller
--  (decoder and instantiated state machine)
--
--  file controller-dataflow.vhd
-- -----------------------------------------------------------

ARCHITECTURE dataflow OF controller IS 

  COMPONENT fsm
    PORT (
      phi1            : IN  std_logic;          -- phase one of clock
      reset           : IN  std_logic;          -- reset input
      halt            : IN  std_logic;          -- halt input
      alu_zero        : IN  std_logic;          -- alu zero bit
      alu_neg         : IN  std_logic;          -- alu negative bit
      dec_1_in        : IN  fsm_states;         -- input from instr. dec. 1
      dec_2_in        : IN  fsm_states;         -- input from instr. dec. 2
      s1_enab         : OUT std_logic_vector(0 TO 3);   -- select s1 source
      s2_enab         : OUT std_logic_vector(0 TO 5);   -- select s2_source
      dest_enab       : OUT std_logic_vector(0 TO 3);   -- select destination
      alu_op_sel      : OUT alu_func_type;              -- alu operation
      const_sel       : OUT std_logic_vector(0 TO 1);   -- select const for s1
      rf_op_sel       : OUT std_logic_vector(0 TO 1);   -- select reg file op.
      immed_sel       : OUT std_logic_vector(0 TO 1);   -- sel. immed. from ir
      mem_ctrl        : OUT std_logic_vector(0 TO 5);   -- memory control lines
      state_number    : OUT fsm_state_numbers);         -- state numbers      
  END COMPONENT;

  COMPONENT ir_decode_1
    PORT (
      instr_in          : IN  dlx_word;
      dec1_out          : OUT fsm_states);
  END COMPONENT;	

  COMPONENT ir_decode_2
  PORT (
    instr_in            : IN  dlx_word;
    dec2_out            : OUT fsm_states);
  END COMPONENT;	
  
  COMPONENT ir_decode_3 
  PORT (instr_in        : IN dlx_word;
	rd_s2_adr_sel   : OUT std_logic
        );
  END COMPONENT;
	
  SIGNAL decode_1_out    : fsm_states;
  SIGNAL decode_2_out    : fsm_states;
  SIGNAL intrn_s1_enab   : std_logic_vector(0 TO 3);
  SIGNAL intrn_s2_enab   : std_logic_vector(0 TO 5);
  SIGNAL intrn_dest_enab : std_logic_vector(0 TO 3);
  SIGNAL intrn_rfop_sel  : std_logic_vector(0 TO 1);
  SIGNAL intrn_mem_ctrl  : std_logic_vector(0 TO 5);
  SIGNAL rd_s2_adr_sel   : std_logic;
  SIGNAL s1_sel          : std_logic;
  
BEGIN
  --
  -- instantiation of the fsm
  --
  sma : fsm
    PORT MAP (
      phi1         => phi1,
      reset        => reset,
      halt         => halt,
      alu_zero     => alu_zero,
      alu_neg      => alu_neg,
      dec_1_in     => decode_1_out,
      dec_2_in     => decode_2_out,
      s1_enab      => intrn_s1_enab,
      s2_enab      => intrn_s2_enab,
      dest_enab    => intrn_dest_enab,
      alu_op_sel   => alu_op_sel,
      const_sel    => const_sel,
      rf_op_sel    => intrn_rfop_sel,
      immed_sel    => immed_sel,
      mem_ctrl     => intrn_mem_ctrl,
      state_number => state_number);
  --
  -- instruction decoder 1
  -- decodes most instructions except load/store
  --
  dec1: ir_decode_1
    PORT MAP (
      instr_in  => instr_in,
      dec1_out  => decode_1_out); 
  --
  -- instruction decoder 2
  -- decodes exact load/store instruction
  --
  dec2: ir_decode_2
    PORT MAP (
      instr_in  => instr_in,
      dec2_out  => decode_2_out);
  --
  -- instruction decoder 3
  -- set signals for the generation of rd, s2(y) 
  --
  dec3: ir_decode_3
    PORT MAP (
      instr_in          => instr_in,
      rd_s2_adr_sel     => rd_s2_adr_sel); 
  --
  -- generate register file addresses
  --
  rs1_out <=    instr_in(6 TO 10);
  
  rs2_out <=    instr_in(11 TO 15);

  rd_out  <=    instr_in(16 TO 20)
                WHEN rd_s2_adr_sel = '1'  -- Rtype dest.
                ELSE instr_in(11 TO 15);  -- Itype dest.                 
  --
  -- connect s1 out_en signals (low active)
  --
  s1_enab <= intrn_s1_enab;  -- |a|mdr|immed|consta|

  --
  -- connect s2 out_en signals (low active)
  --
    -- source b or immed depends on instruction type (selected by rd_s2_adr_sel):   
  s2_enab(0) <= intrn_s2_enab(0) AND
                (NOT rd_s2_adr_sel OR intrn_s2_enab(2));        -- |b| (Rtype)
  s2_enab(1) <= intrn_s2_enab(1) AND
                (rd_s2_adr_sel OR intrn_s2_enab(2));            -- |immed| (Itype)
  -- other sources 
  s2_enab(2 TO 4) <= intrn_s2_enab(3 TO 5);                     -- |pc|mar|constb|

  --
  -- connect dest_enab signals (low active)
  --       
  dest_enab <= intrn_dest_enab;

  --
  -- connect mem_ctrl signals (low active)
  -- mem_ctrl(0) is gated with phi2 (see
  -- file "datapath-structural.vhd")
  --       
  mem_ctrl <= intrn_mem_ctrl;
  --
  -- connect rf_op_sel(0) = ab_latch_enable 
  -- rf_op_sel(0) is gated with phi2 (see
  -- file "datapath-structural.vhd")
  -- connect rf_op_sel(1) = regf_write_enable 
  -- rf_op_sel(1) is gated with phi2 (see
  -- file "reg_fileram-structural.vhd")
  --
  rf_op_sel <= intrn_rfop_sel;

END dataflow;












