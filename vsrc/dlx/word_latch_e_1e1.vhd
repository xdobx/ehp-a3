-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--   Dataflow architecture for transparent latch (32-bit)
--   gate enable,
--   one tri-state output, enabled with out_en1,
--   one normal output 
--
--  (file word_latch_e_1e1.vhd)
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  latch_en : high active
--  out_en1  : low activ
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

--synopsys translate_off
LIBRARY UNISIM;
USE UNISIM.ALL;
--synopsys translate_on

-- library DLX;
USE WORK.dlx_types.ALL;

ENTITY word_latch_e_1e1 IS

  PORT (d               : IN dlx_word;
	q1, q2          : OUT dlx_word;
	gate_en, gate   : IN std_logic;
	out_en1         : IN std_logic);
END word_latch_e_1e1;
