-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--   Structural architecture of the internal memory
--   Dual-port RAM, from core side used as:
--   4k-Byte ROM (program memory)
--   4k-Byte RAM (data memory)
--
--   (file memeory-structural.vhd)
-- -----------------------------------------------------------

ARCHITECTURE structural OF memory_unit IS 


  COMPONENT mem
    PORT (
      -- DLX-Processor: 
      wea         : IN    std_logic;                      -- write enable port A
      ena         : IN    std_logic;                      -- enable port A
      clka        : IN    std_logic;                      -- clock port A
      addra       : IN    std_logic_vector(9 DOWNTO 0);   -- address port A 
      dia         : IN    std_logic_vector(31 DOWNTO 0);  -- data port A
      doa         : OUT   std_logic_vector(31 DOWNTO 0);  -- data port A
      -- Interface:
      web         : IN    std_logic;                      -- write enable port B
      enb         : IN    std_logic_vector(3 DOWNTO 0);   -- enable port B
      clkb        : IN    std_logic;                      -- clock port B
      addrb       : IN    std_logic_vector(9 DOWNTO 0);   -- address port B 
      dib         : IN    std_logic_vector(7 DOWNTO 0);   -- data input port A
      dob         : OUT   std_logic_vector(7 DOWNTO 0)    -- data output port A
      );
  END COMPONENT;
  
  COMPONENT decoder
    PORT (
      a_bus_hn          : IN  std_logic_vector(31 DOWNTO 28);
      enable            : IN  std_logic;
      enab_rom          : OUT std_logic;
      enab_ram          : OUT std_logic;
      enab_display      : OUT std_logic); 
  END COMPONENT;
  

  COMPONENT word_latch_e_1
    PORT (
      d                 : IN  dlx_word;
      q                 : OUT dlx_word;
      gate_en           : IN  std_logic;
      gate              : IN  std_logic);
  END COMPONENT;

  --
  -- signals
  --
  CONSTANT high_const           : std_logic := '1';         
  SIGNAL high_sig               : std_logic;         
  CONSTANT low_const            : std_logic := '0';         
  SIGNAL low_sig                : std_logic;         
  SIGNAL dec_enab_rom           : std_logic;
  SIGNAL dec_enab_ram           : std_logic;
  SIGNAL dec_enab_display       : std_logic;
  SIGNAL dec_disp_latch_en      : std_logic;
  SIGNAL rom_dob_sig            : std_logic_vector( 7 DOWNTO 0); 
  SIGNAL ram_dob_sig            : std_logic_vector( 7 DOWNTO 0); 
  CONSTANT inputs_32_low_const  : std_logic_vector(31 DOWNTO 0)
    := "00000000000000000000000000000000";
  SIGNAL inputs_32_low_sig      : std_logic_vector(31 DOWNTO 0);    

  
BEGIN        

  high_sig              <= high_const;
  low_sig               <= low_const;
  inputs_32_low_sig     <= inputs_32_low_const;

  rom : mem
    PORT MAP (
      -- DLX-Processor: 
      wea         => high_sig, 
      ena         => dec_enab_rom, 
      clka        => phi2,
      addra       => a_bus(11 DOWNTO 2),
      dia         => inputs_32_low_sig,
      doa         => d_bus_in,
      -- Interface:
      web         => rom_web,           -- write enabe blockram port B
      enb         => rom_enb,           -- enabe blockram port B
      clkb        => phi_uc,            -- uc clk
      addrb       => mem_addrb,         -- address blockram port B
      dib         => uc_port_b,         -- data input from uc port B
      dob         => rom_dob_sig            -- data output to uc port A
      );
  
  ram : mem
    PORT MAP (
      -- DLX-Processor: 
      wea         => rw,
      ena         => dec_enab_ram, 
      clka        => phi2, 
      addra       => a_bus(11 DOWNTO 2),
      dia         => d_bus_out,
      doa         => d_bus_in,
      -- Interface:
      web         => ram_web,
      enb         => ram_enb,
      clkb        => phi_uc, 
      addrb       => mem_addrb,
      dib         => uc_port_b, 
      dob         => ram_dob_sig
      );
  
  mem_dob <= rom_dob_sig WHEN
             ((rom_enb = "0001") OR (rom_enb = "0010") OR
              (rom_enb = "0100") OR (rom_enb = "1000"))
             ELSE "ZZZZZZZZ";
  
  mem_dob <= ram_dob_sig WHEN
             ((ram_enb = "0001") OR (ram_enb = "0010") OR
              (ram_enb = "0100") OR (ram_enb = "1000"))
             ELSE "ZZZZZZZZ"; 

  decmem : decoder
    PORT MAP (
      a_bus_hn          => a_bus(31 DOWNTO 28),
      enable            => enable,
      enab_rom          => dec_enab_rom,
      enab_ram          => dec_enab_ram,
      enab_display      => dec_enab_display);

  --
  -- display latch enable
  --
  dec_disp_latch_en <= ((NOT rw) AND dec_enab_display);

  latchdisp : word_latch_e_1
    PORT MAP (
      d         => d_bus_out,
      q         => disp_latch,
      gate_en   => dec_disp_latch_en,
      gate      => phi2);
  
END structural;









