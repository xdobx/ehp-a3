-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--  entity declaration of the DLX processor core  
--  
--  (file core.vhd)
--------------------------------------------------------------------------

LIBRARY  IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;

ENTITY core IS 
  PORT (                                        
    --
    -- Memory interface
    --
    a_bus             : OUT dlx_address;      -- address mux output (from
                                                -- PC/MAR)
    d_bus_in          : IN  dlx_word;         -- data input (to data mux
                                                -- and IR)
    d_bus_out         : OUT dlx_word;         -- data output
    enable            : OUT std_logic;
    rw                : OUT std_logic;
    --
    -- Display outputs
    --
    s1_disp           : OUT dlx_word;         -- s1_bus for display only
    s2_disp           : OUT dlx_word;         -- s2_bus for display only
    dest_disp         : OUT dlx_word;         -- dest_bus for display only
    instr_disp        : OUT dlx_word;         -- ir output  for display only
    state_number      : OUT fsm_state_numbers; -- display FSM states
    error             : OUT std_logic;
    --
    -- external control signals
    --
    reset             : IN  std_logic;        -- asynchronous reset 
    halt              : IN  std_logic;        -- freeze of processor state
    phi1              : IN  std_logic;        -- 2 phase non overlapp. clock
    phi2              : IN  std_logic;
    --
    -- RF-address dump signals:
    --
    uc_rf_data_out2     : OUT dlx_word;  
    uc_rf_addr_out2     : IN dlx_reg_addr;  
    uc_rf_addr_out2_sel : IN std_logic;
    uc_a_mux_sel        : IN std_logic_vector(1 DOWNTO 0));
END core;




