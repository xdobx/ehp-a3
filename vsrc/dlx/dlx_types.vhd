-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
--------------------------------------------------------------------------
--  Package for the data types used in dlx description
--  
--  (file dlx_types.vhd)
--------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

PACKAGE dlx_types IS
  
  SUBTYPE dlx_word	IS std_logic_vector(0 TO 31);	-- bit 0 is msb
  SUBTYPE dlx_half	IS std_logic_vector(0 TO 15);	-- bit 0 is msb
  SUBTYPE dlx_byte	IS std_logic_vector(0 TO 7);	-- bit 0 is msb
  SUBTYPE dlx_nibble	IS std_logic_vector(0 TO 3); 	-- bit 0 is msb

  TYPE dlx_word_array	IS ARRAY (natural RANGE<>) OF dlx_word;
 
  TYPE mem_access_type	IS (word, half, byte);

  SUBTYPE dlx_address	  IS std_logic_vector(31 DOWNTO 0); -- bit 31 is msb

  SUBTYPE dlx_opcode	IS std_logic_vector(0 TO 5);
  SUBTYPE dlx_rr_func	IS std_logic_vector(0 TO 5);
  SUBTYPE dlx_fp_func	IS std_logic_vector(0 TO 4);
  SUBTYPE dlx_reg_addr	IS std_logic_vector(0 TO 4);
  SUBTYPE dlx_immed16	IS std_logic_vector(0 TO 15);
  SUBTYPE dlx_immed26	IS std_logic_vector(0 TO 25);
  
  SUBTYPE reg_index	IS natural RANGE 0 TO 31;
  
  SUBTYPE uc_byte	IS std_logic_vector(7 DOWNTO 0);
  

END dlx_types;


