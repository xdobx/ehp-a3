-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--   Dataflow architecture for transparent latch (32-bit) 
--     gate enable
--     one tri-state output, enabled with out_en  
--
--  (file word_latch_e_1e-dataflow.vhd)
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  gate_en  : high active
--  out_en   : low activ
-- -----------------------------------------------------------

ARCHITECTURE dataflow OF word_latch_e_1e IS

-- virtexe:
--   COMPONENT  LDE
--     PORT (
--        D,G,GE : IN  std_logic;
--        Q      : OUT std_logic);    
--   END COMPONENT;

-- spartan:
  COMPONENT  LDCPE
    --synopsys synthesis_off
    GENERIC (INIT : bit);
    --synopsys synthesis_on
    PORT (
      CLR,D,G,GE,PRE    : IN  std_logic;
      Q                 : OUT std_logic);    
  END COMPONENT;
  
  CONSTANT gnd_const: std_logic := '0';
  SIGNAL   gnd_sig      : std_logic;
  SIGNAL latch_value : dlx_word;

BEGIN
  gnd_sig <= gnd_const;

-- spartan:  
  word_ld:  FOR i IN 0 TO 31 GENERATE 
    latch : LDCPE
      --synopsys synthesis_off
      GENERIC MAP (INIT => '0') -- Initial value of latch ('0' or '1')
      --synopsys synthesis_on
      PORT MAP (
        D   => d(i),
        G   => gate,
        Q   => latch_value(i),
        CLR => gnd_sig, 
        GE  => gate_en, 
        PRE => gnd_sig
        );
  END GENERATE;
  

-- virtexe:
--   word_lde:  FOR i IN 0 TO 31 GENERATE 
--     latch_e : LDE
--       PORT MAP (
--         D  => d(i),
--         G  => gate,
--         GE => gate_en,
--         Q  => latch_value(i)
--         );
--   END GENERATE;

  q <= latch_value WHEN out_en = '0' ELSE
       (OTHERS => 'Z');

END dataflow;




