-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
-- Entity declaration for the microcontroller interface
--
-- (file uc_if.vhd)
-- -----------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;
--synopsys translate_off
LIBRARY UNISIM;
USE UNISIM.ALL;
--synopsys translate_on

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;

ENTITY uc_if IS
  
  PORT (
-- external pad-signals
    uc_port_a           : OUT std_logic_vector( 7 DOWNTO 0);
    uc_port_b           : IN  std_logic_vector( 7 DOWNTO 0);
    uc_port_c           : IN  std_logic_vector( 7 DOWNTO 0);
    uc_clk              : IN  std_logic;
    q_clk               : IN  std_logic;
-- internal fpga-signals
    mem_dob             : IN  std_logic_vector( 7 DOWNTO 0);
    port_b              : OUT std_logic_vector( 7 DOWNTO 0);
    res_hlt             : OUT std_logic_vector( 1 DOWNTO 0);
    disp_sel            : OUT std_logic_vector( 2 DOWNTO 0);
    mem_addrb           : OUT std_logic_vector( 9 DOWNTO 0);
    rom_web             : OUT std_logic;
    rom_enb             : OUT std_logic_vector( 0 TO 3);
    ram_web             : OUT std_logic;
    ram_enb             : OUT std_logic_vector( 0 TO 3);
    phi1                : OUT std_logic;
    phi2                : OUT std_logic;
    phi_uc              : OUT std_logic;
    a_bus               : IN  dlx_address;      -- address mux output (from PC/MAR)
    d_bus_in            : IN  dlx_word;         -- data input (to data mux and IR)
    d_bus_out           : IN  dlx_word;         -- data output
    s1_bus              : IN  dlx_word;
    s2_bus              : IN  dlx_word;
    dest_bus            : IN  dlx_word;
    instr_out           : IN  dlx_word;
    disp_latch_out      : IN  dlx_word;
    state               : IN  fsm_state_numbers;
    uc_rf_data_out2     : IN  dlx_word;  
    uc_rf_addr_out2     : OUT dlx_reg_addr;  
    uc_rf_addr_out2_sel : OUT std_logic;
    uc_a_mux_sel        : OUT std_logic_vector(1 DOWNTO 0));

END uc_if;





