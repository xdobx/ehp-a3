-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--
-- -----------------------------------------------------------
--  Dataflow architecture of two phase clock builder  
--  
--  (file qclk2phi-dataflow.vhd)
-- -----------------------------------------------------------

ARCHITECTURE dataflow OF qclk2phi IS


COMPONENT DCM_SP  
    --synopsys synthesis_off
    GENERIC (clkdv_divide :     real);
    --synopsys synthesis_on
    PORT(
      CLK0        : OUT std_logic;  -- 0 degree DCM CLK ouptput
      CLK180      : OUT std_logic;  -- 180 degree DCM CLK output
      CLK270      : OUT std_logic;  -- 270 degree DCM CLK output
      CLK2X       : OUT std_logic;  -- 2X DCM CLK output
      CLK2X180    : OUT std_logic;  -- 2X, 180 degree DCM CLK out
      CLK90       : OUT std_logic;  -- 90 degree DCM CLK output
      CLKDV       : OUT std_logic;  -- Divided DCM CLK out (CLKDV_DIVIDE)
      CLKFX       : OUT std_logic;  -- DCM CLK synthesis out (M/D)
      CLKFX180    : OUT std_logic;  -- 180 degree CLK synthesis out
      LOCKED      : OUT std_logic;  -- DCM LOCK status output
      PSDONE      : OUT std_logic;  -- Dynamic phase adjust done output
      STATUS      : OUT std_logic_vector(7 DOWNTO 0);
                                    -- 8-bit DCM status bits output
      CLKFB       : IN  std_logic;  -- DCM clock feedback
      CLKIN       : IN  std_logic;  -- Clock input (from IBUFG, BUFG or DCM)
      DSSEN       : IN  std_logic;  --
      PSCLK       : IN  std_logic;  -- Dynamic phase adjust clock input
      PSEN        : IN  std_logic;  -- Dynamic phase adjust enable input
      PSINCDEC    : IN  std_logic;  -- Dynamic phase adjust increment/decrement
      RST         : IN  std_logic   -- DCM asynchronous reset input
      );
 END COMPONENT;

--   COMPONENT CLKDLL
--     --synopsys synthesis_off
--     GENERIC (clkdv_divide :     real);
--     --synopsys synthesis_on
--     PORT (
--       CLKIN               : IN  std_logic;
--       CLKFB               : IN  std_logic;
--       RST                 : IN  std_logic;
--       CLK0                : OUT std_logic;
--       CLK90               : OUT std_logic;
--       CLK180              : OUT std_logic;
--       CLK270              : OUT std_logic;
--       CLK2X               : OUT std_logic;
--       CLKDV               : OUT std_logic;
--       LOCKED              : OUT std_logic);
--   END COMPONENT;

  COMPONENT BUFG
    PORT (
      I : IN  std_logic;
      O : OUT std_logic);
  END COMPONENT;

  SIGNAL   CLK2X_1      : std_logic;
  SIGNAL   CLK2X_2      : std_logic;
  SIGNAL   CLK_FB_2     : std_logic;
  SIGNAL   LOCKED_1     : std_logic;
  SIGNAL   CLKDV_1      : std_logic;    -- clkin divided by 1.5|2|2.5|3|4|5|8|16
  SIGNAL   CLKDV_1_del1 : std_logic;
  SIGNAL   CLKDV_1_del2 : std_logic;
  SIGNAL   RST_2        : std_logic;
  CONSTANT GND_BIT_const: std_logic := '0';
  SIGNAL   GND_BIT      : std_logic;

BEGIN
  
  GND_BIT <= GND_BIT_const;
  RST_2   <= NOT LOCKED_1;

-- spartan
  ckdll_1 :DCM_SP 
    --synopsys synthesis_off
    GENERIC MAP ( clkdv_divide => 4.0)
    --synopsys synthesis_on
      port map (CLKFB=>CLK2X_1,
                CLKIN=>CLKIN,
                DSSEN=>GND_BIT,
                PSCLK=>GND_BIT,
                PSEN=>GND_BIT,
                PSINCDEC=>GND_BIT,
                RST=>GND_BIT,
                CLKDV=>CLKDV_1,
                CLKFX=>open,
                CLKFX180=>open,
                CLK0=>CLK2X_1,--open,
                CLK2X=>open,--CLK2X_1,
                CLK2X180=>open,
                CLK90=>open,
                CLK180=>open,
                CLK270=>open,
                LOCKED=>LOCKED_1,
                PSDONE=>open,
                STATUS=>open
                );
-- virtexe:
--     PORT MAP (
--       CLKIN  => clkin,
--       CLKFB  => CLK2X_1,
--       RST    => GND_BIT,
--       CLK0   => OPEN,
--       CLK90  => OPEN,
--       CLK180 => OPEN,
--       CLK270 => OPEN,
--       CLK2X  => CLK2X_1,
--       CLKDV  => CLKDV_1,
--       LOCKED => LOCKED_1);

-- spartan:   
  ckdll_2 : DCM_SP
    --synopsys synthesis_off
    GENERIC MAP ( clkdv_divide => 3.0)
    --synopsys synthesis_on
      port map (CLKFB=>CLK_FB_2,
                CLKIN=>CLK2X_1,
                DSSEN=>GND_BIT,
                PSCLK=>GND_BIT,
                PSEN=>GND_BIT,
                PSINCDEC=>GND_BIT,
                RST=>RST_2,
                CLKDV=>open,
                CLKFX=>open,
                CLKFX180=>open,
                CLK0=>CLK2X_2,--open,
                CLK2X=>open,--CLK2X_2,
                CLK2X180=>open,
                CLK90=>open,
                CLK180=>open,
                CLK270=>open,
                LOCKED=>open,
                PSDONE=>open,
                STATUS=>open
                );


-- virtexe:    
--     PORT MAP (
--       CLKIN                    => CLK2X_1,
--       CLKFB                    => CLK_FB_2,
--       RST                      => RST_2,
--       CLK0                     => OPEN,
--       CLK90                    => OPEN,
--       CLK180                   => OPEN,
--       CLK270                   => OPEN,
--       CLK2X                    => CLK2X_2,
--       CLKDV                    => OPEN,
--       LOCKED                   => OPEN);

  fb_2 : BUFG
    PORT MAP (
      I => CLK2X_2,
      O => CLK_FB_2);


  dly1 : PROCESS(CLK2X_2)
  BEGIN
    IF
      ( CLK2X_2'event AND CLK2X_2 = '0') THEN
      CLKDV_1_del1 <= CLKDV_1;
    END IF;
  END PROCESS;

  dly2 : PROCESS(CLK2X_2)
  BEGIN
    IF
      ( CLK2X_2'event AND CLK2X_2 = '0') THEN 	
      CLKDV_1_del2 <= CLKDV_1_del1;
    END IF;
  END PROCESS;
  
  phi2 <= CLKDV_1 AND CLKDV_1_del1;
  phi1 <= (NOT CLKDV_1) AND (NOT CLKDV_1_del1);
  
END dataflow;










