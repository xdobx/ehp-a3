-- -----------------------------------------------------------
--   DLX Processor Experiment within the
--   "COMPUTERTECHNIK-PRAKTIKUM"  
--   Sept 2002 A.Reinsch / LS-RA
--   University of Jena / Dept. of Computer Science / LS-RA
-- -----------------------------------------------------------
--   A very small DLX Processor version
--  
--   Synthesizable code for:
--        SYNOPSYS-FPGA-Express (FPGA-Compiler II)
--   Target architecture:
--        XILINX Virtex-E  
-- -----------------------------------------------------------
--   ORIGINAL VERSION:
--   DLX PROCESSOR MODEL SUITE
--   Copyright (C) 1995, Martin Gumm
--   Univ. of Stuttgart / Dept. of Computer Science / IPVR-ISE
-- -----------------------------------------------------------
--
--  Entity declaration for decoder
--  (file decoder.vhd)
--------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

USE WORK.dlx_types.ALL;
USE WORK.control_types.ALL;

ENTITY decoder IS 

  PORT (
    a_bus_hn            : IN  std_logic_vector(31 DOWNTO 28);
    enable              : IN  std_logic;
    enab_rom            : OUT std_logic;
    enab_ram            : OUT std_logic;
    enab_display        : OUT std_logic);	
END decoder;










