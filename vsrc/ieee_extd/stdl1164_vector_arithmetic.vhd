--------------------------------------------------------------------------
--  DLX PROCESSOR MODEL SUITE
--  Copyright (C) 1995, Martin Gumm
--  University of Stuttgart / Department of Computer Science / IPVR-ISE
--------------------------------------------------------------------------
--  Partly derived from 
--   - DLX model suite
--     Copyright (C) 1993, Peter J. Ashenden
--     University of Adelaide, Australia / Dept. Computer Science
--------------------------------------------------------------------------
--  This program is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 1, or (at your option)
--  any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--------------------------------------------------------------------------
--  Last revision date : November 15 1995
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--  *** SYNOPSYS synthesizable code (ver. 3.2.a) ***
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--  Package declaration for the vector arithmetic package
--  
--  (file stdl1164_vector_arithmetic.vhd)
--------------------------------------------------------------------------

library IEEE;
USE IEEE.std_logic_1164.ALL;

package stdl1164_vector_arithmetic is
  
  ----------------------------------------------------------------
  --  ***** Type conversions *****
  ----------------------------------------------------------------

  ----------------------------------------------------------------
  -- Convert std_logic vector encoded unsigned integer to natural.
  ----------------------------------------------------------------
  function sv_to_natural (sv : in std_logic_vector) return natural;

  -- synopsys synthesis_off

  ----------------------------------------------------------------
  -- Convert natural to std_logic vector encoded unsigned integer.
  -- (length is used as the size of the result.);
  ----------------------------------------------------------------
  function natural_to_sv (nat : in natural;
      	      	      	  length : in natural) return std_logic_vector;

  ----------------------------------------------------------------
  -- Convert std_logic vector encoded signed integer to integer
  ----------------------------------------------------------------
  function sv_to_integer (sv : in std_logic_vector) return integer;
  
  ----------------------------------------------------------------
  -- Convert integer to std_logic vector encoded signed integer.
  -- (length is used as the size of the result.)
  ----------------------------------------------------------------
  function integer_to_sv (int : in integer;
      	      	      	  length : in natural) return std_logic_vector;

  -- synopsys synthesis_on
  ----------------------------------------------------------------
  --  ***** Arithmetic operations *****
  ----------------------------------------------------------------

  ----------------------------------------------------------------
  -- Signed addition with overflow detection
  ----------------------------------------------------------------
  procedure sv_add (sv1, sv2  : in  std_logic_vector;
      	       	    sv_result : out std_logic_vector;
		    overflow  : out std_logic);

  ----------------------------------------------------------------
  -- Signed addition without overflow detection
  ----------------------------------------------------------------
  procedure sv_add (sv1, sv2  : in  std_logic_vector;
      	       	    sv_result : out std_logic_vector);

  ----------------------------------------------------------------
  -- Unsigned addition with overflow detection
  ----------------------------------------------------------------
  procedure sv_addu (sv1, sv2  : in  std_logic_vector;
      	       	     sv_result : out std_logic_vector;
		     overflow  : out std_logic);

  ----------------------------------------------------------------
  -- Unsigned addition without overflow detection
  ----------------------------------------------------------------
  procedure sv_addu (sv1, sv2  : in  std_logic_vector;
      	       	     sv_result : out std_logic_vector);

  ----------------------------------------------------------------
  -- Signed subtraction with overflow detection
  ----------------------------------------------------------------
  procedure sv_sub (sv1, sv2  : in  std_logic_vector;
      	       	    sv_result : out std_logic_vector;
		    overflow  : out std_logic);

  ----------------------------------------------------------------
  -- Signed subtraction without overflow detection
  ----------------------------------------------------------------
  procedure sv_sub (sv1, sv2  : in  std_logic_vector;
      	       	    sv_result : out std_logic_vector);

  ----------------------------------------------------------------
  -- Unsigned subtraction with overflow detection
  ----------------------------------------------------------------
  procedure sv_subu (sv1, sv2  : in  std_logic_vector;
      	       	     sv_result : out std_logic_vector;
		     overflow  : out std_logic);

  ----------------------------------------------------------------
  -- Unsigned subtraction without overflow detection
  ----------------------------------------------------------------
  procedure sv_subu (sv1, sv2  : in  std_logic_vector;
      	       	     sv_result : out std_logic_vector);

  ----------------------------------------------------------------
  -- Signed negation with overflow detection
  ----------------------------------------------------------------
  procedure sv_neg (sv        : in  std_logic_vector;
                    sv_result : out std_logic_vector;
                    overflow  : out std_logic);

  ----------------------------------------------------------------
  -- Signed negation without overflow detection
  ----------------------------------------------------------------
  procedure sv_neg (sv        : in  std_logic_vector;
                    sv_result : out std_logic_vector);

--  ----------------------------------------------------------------
--  -- Signed multiplication with overflow detection
--  ----------------------------------------------------------------
--  procedure sv_mult (sv1, sv2  : in std_logic_vector;
--      	       	     sv_result : out std_logic_vector;
--		     overflow  : out std_logic);
--
--  ----------------------------------------------------------------
--  -- Signed multiplication without overflow detection
--  ----------------------------------------------------------------
--  procedure sv_mult (sv1, sv2  : in  std_logic_vector;
--                     sv_result : out std_logic_vector);
--
--  ----------------------------------------------------------------
--  -- Unsigned multiplication with overflow detection
--  ----------------------------------------------------------------
--  procedure sv_multu (sv1, sv2  : in  std_logic_vector;
--      	       	      sv_result : out std_logic_vector;
--		      overflow  : out std_logic);
--
--  ----------------------------------------------------------------
--  -- Unsigned multiplication without overflow detection
--  ----------------------------------------------------------------
--  procedure sv_multu (sv1, sv2  : in  std_logic_vector;
--      	       	      sv_result : out std_logic_vector);
--
--  ----------------------------------------------------------------
--  -- Signed division with divide by zero and overflow detection
--  ----------------------------------------------------------------
--  procedure sv_div (sv1, sv2    : in  std_logic_vector;
--      	       	    sv_result   : out std_logic_vector;
--		    div_by_zero : out std_logic;
--                    overflow    : out std_logic);
--
--  ----------------------------------------------------------------
--  -- Signed division without divide by zero and overflow detection
--  ----------------------------------------------------------------
--  procedure sv_div (sv1, sv2  : in  std_logic_vector;
--                    sv_result : out std_logic_vector);
--
--  ----------------------------------------------------------------
--  -- Unsigned division with divide by zero detection
--  ----------------------------------------------------------------
--  procedure sv_divu (sv1, sv2    : in  std_logic_vector;
--      	       	     sv_result   : out std_logic_vector;
--		     div_by_zero : out std_logic);
--
--  ----------------------------------------------------------------
--  -- Unsigned division without divide by zero detection
--  ----------------------------------------------------------------
--  procedure sv_divu (sv1, sv2  : in  std_logic_vector;
--      	       	     sv_result : out std_logic_vector);

  ----------------------------------------------------------------
  --  ***** Logical operators *****
  --  (Provided for VHDL-87, built in for VHDL-93)
  ----------------------------------------------------------------
  ----------------------------------------------------------------
  -- sv_sll
  -- Shift left logical (fill with '0' bits)
  ----------------------------------------------------------------
  procedure sv_sll (sv        : in  std_logic_vector;
                    sv_result : out std_logic_vector;
                    shift_count : in std_logic_vector);

  ----------------------------------------------------------------
  -- sv_srl
  -- Shift right logical (fill with '0' bits)
  ----------------------------------------------------------------
  procedure sv_srl (sv        : in  std_logic_vector;
                    sv_result : out std_logic_vector;
      	       	    shift_count : in std_logic_vector);

  ----------------------------------------------------------------
  -- sv_sra
  -- Shift right arithmetic (fill with copy of sign bit)
  ----------------------------------------------------------------
  procedure sv_sra (sv        : in  std_logic_vector;
                    sv_result : out std_logic_vector;
      	            shift_count : in std_logic_vector);

  ----------------------------------------------------------------
  -- Rotate left
  ----------------------------------------------------------------
  procedure sv_rol (sv        : in  std_logic_vector;
                    sv_result : out std_logic_vector;
        	    rotate_count : in natural);

  ----------------------------------------------------------------
  -- Rotate right
  ----------------------------------------------------------------
  procedure sv_ror (sv        : in  std_logic_vector;
                    sv_result : out std_logic_vector;
       	            rotate_count : in natural);

  ----------------------------------------------------------------
  --  ***** Arithmetic comparison operators. *****
  ----------------------------------------------------------------

  ----------------------------------------------------------------
  -- sv_lt
  -- Signed less than comparison
  ----------------------------------------------------------------
  procedure sv_lt (sv1, sv2  : in  std_logic_vector;
                   sv_result : out std_logic);
  
  ----------------------------------------------------------------
  -- sv_ltu
  -- Unsigned less than comparison
  ----------------------------------------------------------------
  procedure sv_ltu (sv1, sv2  : in  std_logic_vector;
                    sv_result : out std_logic);

  ----------------------------------------------------------------
  -- sv_le
  -- Signed less than or equal comparison
  ----------------------------------------------------------------
  procedure sv_le (sv1, sv2  : in  std_logic_vector;
                   sv_result : out std_logic);

  ----------------------------------------------------------------
  -- sv_leu
  -- Unsigned less than or equal comparison
  ----------------------------------------------------------------
  procedure sv_leu (sv1, sv2  : in  std_logic_vector;
                    sv_result : out std_logic);

  ----------------------------------------------------------------
  -- sv_gt
  -- Signed greater than comparison
  ----------------------------------------------------------------
  procedure sv_gt (sv1, sv2  : in  std_logic_vector;
                   sv_result : out std_logic);

  ----------------------------------------------------------------
  -- sv_gtu
  -- Unsigned greater than comparison
  ----------------------------------------------------------------
  procedure sv_gtu (sv1, sv2  : in  std_logic_vector;
                    sv_result : out std_logic);

  ----------------------------------------------------------------
  -- sv_ge
  -- Signed greater than or equal comparison
  ----------------------------------------------------------------
  procedure sv_ge (sv1, sv2  : in  std_logic_vector;
                   sv_result : out std_logic);

  ----------------------------------------------------------------
  -- sv_geu
  -- Unsigned greater than or equal comparison
  ----------------------------------------------------------------
  procedure sv_geu (sv1, sv2  : in  std_logic_vector;
                   sv_result : out std_logic);

  ----------------------------------------------------------------
  --  ***** Extension operators *****
  ----------------------------------------------------------------

  ----------------------------------------------------------------
  -- sv_sext
  ----------------------------------------------------------------
  function sv_sext (sv     : in  std_logic_vector;
                    length : in natural) return std_logic_vector;

  ----------------------------------------------------------------
  -- sv_zext
  ----------------------------------------------------------------
  function sv_zext (sv     : in  std_logic_vector;
                    length : in natural) return std_logic_vector;

  ----------------------------------------------------------------
  -- sv_expand
  ----------------------------------------------------------------
  function sv_expand (sv     : in  std_ulogic;
      	      	      length : in natural) return std_logic_vector;

end stdl1164_vector_arithmetic;


