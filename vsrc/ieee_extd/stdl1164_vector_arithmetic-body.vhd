--------------------------------------------------------------------------
--  DLX PROCESSOR MODEL SUITE
--  Copyright (C) 1995, Martin Gumm
--  University of Stuttgart / Department of Computer Science / IPVR-ISE
--------------------------------------------------------------------------
--  Partly derived from 
--   - DLX model suite
--     Copyright (C) 1993, Peter J. Ashenden
--     University of Adelaide, Australia / Dept. Computer Science
--------------------------------------------------------------------------
--  This program is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 1, or (at your option)
--  any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--------------------------------------------------------------------------
--  Last revision date : November 15 1995
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--  *** SYNOPSYS synthesizable code (ver. 3.2.a) ***
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--  Body of the arithmetic package for the IEEE 1164 standard logic system
--  
--  (file fiveval_arithm-body.vhdl)
--------------------------------------------------------------------------

PACKAGE BODY stdl1164_vector_arithmetic IS

  ----------------------------------------------------------------
  --  ***** Type conversions *****
  ----------------------------------------------------------------

  ----------------------------------------------------------------
  -- sv_to_natural
  -- Convert std_logic vector encoded unsigned integer to natural.
  ----------------------------------------------------------------

  FUNCTION sv_TO_natural(sv : IN std_logic_vector) RETURN natural IS

    VARIABLE result : natural;

  BEGIN
   -- synopsys synthesis_off
   ASSERT ( IS_X(sv) = FALSE)
      REPORT "sv_to_natural: Conversion with unknown input value !"
      SEVERITY WARNING; 
      
   IF ( IS_X(sv) = TRUE) THEN
     RETURN 0;
   ELSE
   -- synopsys synthesis_on
      result := 0;
      FOR i IN sv'RANGE LOOP
	--
	--IF (std_logic'pos(sv(i)) > 3) THEN		-- 'L' or 'H'
        --  result := result * 2 + (std_logic'pos(sv(i)) - 6); 
	--ELSE						-- '0' or '1'
	--  result := result * 2 + (std_logic'pos(sv(i)) - 2);
	--END IF;
	--
     	if sv(i) = '1' then
	  result := result + result + 1;
	else
 	  result := result + result;
	end if;
      END LOOP;
      RETURN result;
   -- synopsys synthesis_off
    END IF;
   -- synopsys synthesis_on
  END sv_TO_natural;

  -- synopsys synthesis_off

  ----------------------------------------------------------------
  -- natural_to_sv
  -- Convert natural to std_logic vector encoded unsigned integer.
  -- (length is used as the size of the result.)
  ----------------------------------------------------------------

  FUNCTION natural_TO_sv(nat : IN natural;
      	      	      	 length : IN natural) RETURN std_logic_vector IS

    VARIABLE temp : natural := nat;
    VARIABLE result : std_logic_vector(0 TO length-1);

  BEGIN
    FOR i IN result'reverse_RANGE LOOP
      result(i) := std_logic'val((temp REM 2) + 2);
      temp := temp / 2;
    END LOOP;
    RETURN result;
  END natural_TO_sv;


  ----------------------------------------------------------------
  -- sv_to_integer
  -- Convert std_logic vector encoded signed integer to integer
  ----------------------------------------------------------------

  FUNCTION sv_TO_integer(sv : IN std_logic_vector) RETURN integer IS

    VARIABLE temp : std_logic_vector(sv'RANGE);
    VARIABLE result : integer := 0;

  BEGIN
    ASSERT ( IS_X(sv) = FALSE)
      REPORT "sv_to integer: Conversion with unknown input value !"
      SEVERITY WARNING; 

    IF ( IS_X(sv) = TRUE) THEN
      RETURN 0;
    ELSE
      IF sv(sv'left) = '1' THEN	  -- negative number
        temp := NOT sv;
      ELSE
        temp := sv;
      END IF;
      FOR i IN sv'RANGE LOOP	  -- sign bit of temp = '0'
        IF (std_logic'pos(sv(i)) > 3) THEN		-- 'L' or 'H'
          result := result * 2 + (std_logic'pos(temp(i)) - 6); 
	ELSE						-- '0' or '1'
	  result := result * 2 + (std_logic'pos(temp(i)) - 2);
	END IF;    
      END LOOP;
      IF sv(sv'left) = '1' THEN
        result := (-result) - 1;
      END IF;
    RETURN result;
    END IF;     
  END sv_TO_integer;


  ----------------------------------------------------------------
  -- integer_to_sv
  -- Convert integer to std_logic vector encoded signed integer.
  -- (length is used as the size of the result.)
  ----------------------------------------------------------------

  FUNCTION integer_TO_sv(int : IN integer;
      	      	      	 length : IN natural) RETURN std_logic_vector IS

    VARIABLE temp : integer;
    VARIABLE result : std_logic_vector(0 TO length-1);

  BEGIN
    IF int < 0 THEN 
      temp := -(int+1); 
    ELSE 
      temp := int; 
    END IF;
    FOR i IN result'reverse_RANGE LOOP
      result(i) := std_logic'val((temp REM 2) + 2);
      temp := temp / 2;
    END LOOP;
    IF int < 0 THEN
      result := NOT result;
      result(result'left) := '1';
    END IF;
    RETURN result;
  END integer_TO_sv;


  -- synopsys synthesis_on
  ----------------------------------------------------------------
  --  ***** Arithmetic operations *****
  ----------------------------------------------------------------

  ----------------------------------------------------------------
  -- sv_add
  -- Signed addition with overflow detection
  ----------------------------------------------------------------
  PROCEDURE sv_add (sv1, sv2 : IN std_logic_vector;
      	       	    sv_result : OUT std_logic_vector;
		    overflow : OUT std_logic) IS

    VARIABLE sv1op: std_logic_vector(1 TO sv1'length);
    VARIABLE sv2op: std_logic_vector(1 TO sv2'length);
    VARIABLE result : std_logic_vector(1 TO sv_result'length);
    VARIABLE intern : std_logic;
    VARIABLE carry_IN : std_logic;
    VARIABLE carry_OUT : std_logic;

  BEGIN
    --synopsys synthesis_off
    ASSERT (sv1'length = sv2'length) AND (sv1'length = sv_result'length)
      REPORT "sv_add: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_add: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_add: second operand contains unknown value !"
      SEVERITY WARNING; 

    --synopsys synthesis_on
    sv1op := sv1;
    sv2op := sv2;
    carry_out := '0';
    FOR index IN result'reverse_RANGE LOOP
      carry_IN := carry_OUT;  			-- of previous bit
      intern := sv1op(index) XOR sv2op(index);
      result(index) := intern XOR carry_IN;
      carry_OUT := (sv1op(index) AND sv2op(index)) OR (carry_IN AND intern);
    END LOOP;
    overflow := carry_OUT XOR carry_IN;
    sv_result:= result;
  END sv_add;

  ----------------------------------------------------------------
  -- sv_add
  -- Signed addition without overflow detection
  ----------------------------------------------------------------
  PROCEDURE sv_add (sv1, sv2 : IN std_logic_vector;
      	       	    sv_result : OUT std_logic_vector) IS

    VARIABLE sv1op: std_logic_vector(1 TO sv1'length);
    VARIABLE sv2op: std_logic_vector(1 TO sv2'length);
    VARIABLE result : std_logic_vector(1 TO sv_result'length);
    VARIABLE intern : std_logic;
    VARIABLE carry_IN : std_logic;
    VARIABLE carry_OUT : std_logic;

  BEGIN
    --synopsys synthesis_off
    ASSERT (sv1'length = sv2'length) AND (sv1'length = sv_result'length)
      REPORT "sv_add: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_add: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_add: second operand contains unknown value !"
      SEVERITY WARNING; 

    --synopsys synthesis_on
    sv1op := sv1;
    sv2op := sv2;
    carry_out := '0';
    FOR index IN result'reverse_RANGE LOOP
      carry_IN := carry_OUT;  			-- of previous bit
      intern := sv1op(index) XOR sv2op(index);
      result(index) := intern XOR carry_IN;
      carry_OUT := (sv1op(index) AND sv2op(index)) OR (carry_IN AND intern);
    END LOOP;
    sv_result := result;
  END sv_add;

  ----------------------------------------------------------------
  -- sv_addu
  -- Unsigned addition with overflow detection
  ----------------------------------------------------------------
  PROCEDURE sv_addu (sv1, sv2 : IN std_logic_vector;
      	       	     sv_result : OUT std_logic_vector;
		     overflow : OUT std_logic) IS

    VARIABLE sv1op: std_logic_vector(1 TO sv1'length);
    VARIABLE sv2op: std_logic_vector(1 TO sv2'length);
    VARIABLE result : std_logic_vector(1 TO sv_result'length);  
    VARIABLE intern : std_logic;
    VARIABLE carry : std_logic;

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length AND sv1'length = sv_result'length
      REPORT "sv_addu: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_add: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_add: second operand contains unknown value !"
      SEVERITY WARNING; 

    --synopsys synthesis_on
    sv1op := sv1;
    sv2op := sv2;
    carry := '0';
    FOR index IN result'reverse_RANGE LOOP
      intern := sv1op(index) XOR sv2op(index);
      result(index) := intern XOR carry;
      carry := (sv1op(index) AND sv2op(index)) OR (carry AND intern);
    END LOOP;
    sv_result := result;
    overflow := carry;
  END sv_addu;

  ----------------------------------------------------------------
  -- sv_addu
  -- Unsigned addition without overflow detection
  ----------------------------------------------------------------
  PROCEDURE sv_addu (sv1, sv2 : IN std_logic_vector;
      	       	     sv_result : OUT std_logic_vector) IS

    VARIABLE sv1op: std_logic_vector(1 TO sv1'length);
    VARIABLE sv2op: std_logic_vector(1 TO sv2'length);
    VARIABLE result : std_logic_vector(1 TO sv_result'length);
    VARIABLE intern : std_logic;
    VARIABLE carry : std_logic;

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length AND sv1'length = sv_result'length
      REPORT "sv_addu: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_add: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_add: second operand contains unknown value !"
      SEVERITY WARNING; 

    --synopsys synthesis_on
    sv1op := sv1;
    sv2op := sv2;
    carry := '0';
    FOR index IN result'reverse_RANGE LOOP
      intern := sv1op(index) XOR sv2op(index);
      result(index) :=  intern XOR carry;
      carry := (sv1op(index) AND sv2op(index)) OR (carry AND intern);
    END LOOP;
    sv_result := result;
  END sv_addu;

  ----------------------------------------------------------------
  -- sv_sub
  -- Signed subtraction with overflow detection
  -- (signed addition with second operand negated and carry_in set)
  ----------------------------------------------------------------
  PROCEDURE sv_sub (sv1, sv2 : IN std_logic_vector;
      	       	    sv_result : OUT std_logic_vector;
		    overflow : OUT std_logic) IS

    VARIABLE sv1op: std_logic_vector(1 TO sv1'length);
    VARIABLE sv2op: std_logic_vector(1 TO sv2'length);
    VARIABLE result : std_logic_vector(1 TO sv_result'length);
    VARIABLE intern : std_logic;
    VARIABLE carry_IN : std_logic;
    VARIABLE carry_OUT : std_logic;

  BEGIN
    --synopsys synthesis_off
    ASSERT (sv1'length = sv2'length) AND (sv1'length = sv_result'length)
      REPORT "sv_sub: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_sub: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_sub: second operand contains unknown value !"
      SEVERITY WARNING; 

    --synopsys synthesis_on
    sv1op := sv1;
    sv2op := sv2;
    carry_out := '1';
    FOR index IN result'reverse_RANGE LOOP
      carry_IN := carry_OUT;  			-- of previous bit
      intern := sv1op(index) XOR (NOT sv2op(index));
      result(index) := intern XOR carry_IN;
      carry_OUT := (sv1op(index) AND (NOT sv2op(index))) OR (carry_IN AND intern);
    END LOOP;
    overflow := carry_OUT XOR carry_IN;
    sv_result:= result;
  END sv_sub;

  ----------------------------------------------------------------
  -- sv_sub
  -- Signed subtraction without overflow detection
  -- (signed addition with second operand negated and carry_in set)
  ----------------------------------------------------------------
  PROCEDURE sv_sub (sv1, sv2 : IN std_logic_vector;
      	       	    sv_result : OUT std_logic_vector) IS


    VARIABLE sv1op: std_logic_vector(1 TO sv1'length);
    VARIABLE sv2op: std_logic_vector(1 TO sv2'length);
    VARIABLE result : std_logic_vector(1 TO sv_result'length);
    VARIABLE intern : std_logic;
    VARIABLE carry_IN : std_logic;
    VARIABLE carry_OUT : std_logic;

  BEGIN
    --synopsys synthesis_off
    ASSERT (sv1'length = sv2'length) AND (sv1'length = sv_result'length)
      REPORT "sv_sub: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_sub: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_sub: second operand contains unknown value !"
      SEVERITY WARNING; 

    --synopsys synthesis_on
    sv1op := sv1;
    sv2op := sv2;
    carry_out := '1';
    FOR index IN result'reverse_RANGE LOOP
      carry_IN := carry_OUT;  			-- of previous bit
      intern := sv1op(index) XOR (NOT sv2op(index));
      result(index) := intern XOR carry_IN;
      carry_OUT := (sv1op(index) AND (NOT sv2op(index))) OR (carry_IN AND intern);
    END LOOP;
    sv_result := result;
  END sv_sub;

  ----------------------------------------------------------------
  -- sv_subu
  -- Unsigned subtraction with overflow detection
  ----------------------------------------------------------------
  PROCEDURE sv_subu (sv1, sv2 : IN std_logic_vector;
      	       	     sv_result : OUT std_logic_vector;
		     overflow : OUT std_logic) IS

    VARIABLE sv1op: std_logic_vector(1 TO sv1'length);
    VARIABLE sv2op: std_logic_vector(1 TO sv2'length);
    VARIABLE result : std_logic_vector(1 TO sv_result'length);
    VARIABLE intern : std_logic;
    VARIABLE borrow : std_logic;

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length AND sv1'length = sv_result'length
      REPORT "sv_subu: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_subu: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_subu: second operand contains unknown value !"
      SEVERITY WARNING; 

    --synopsys synthesis_on
    sv1op := sv1;
    sv2op := sv2;
    borrow := '0';
    FOR index IN result'reverse_RANGE LOOP
      intern := sv1op(index) XOR sv2op(index);
      result(index) := intern XOR borrow;
      borrow := (NOT(sv1op(index)) AND sv2op(index)) OR (borrow AND NOT(intern));
    END LOOP;
    sv_result := result;
    overflow := borrow;
  END sv_subu;

  ----------------------------------------------------------------
  -- sv_subu
  -- Unsigned subtraction without overflow detection
  ----------------------------------------------------------------
  PROCEDURE sv_subu (sv1, sv2 : IN std_logic_vector;
      	       	     sv_result : OUT std_logic_vector) IS

    VARIABLE sv1op: std_logic_vector(1 TO sv1'length);
    VARIABLE sv2op: std_logic_vector(1 TO sv2'length);
    VARIABLE result : std_logic_vector(1 TO sv_result'length);
    VARIABLE intern : std_logic;
    VARIABLE borrow : std_logic;

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length AND sv1'length = sv_result'length
      REPORT "sv_subu: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_subu: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_subu: second operand contains unknown value !"
      SEVERITY WARNING; 

    --synopsys synthesis_on
    sv1op := sv1;
    sv2op := sv2;
    borrow := '0';
    FOR index IN result'reverse_RANGE LOOP
      intern := sv1op(index) XOR sv2op(index);
      result(index) := intern XOR borrow;
      borrow := (NOT(sv1op(index)) AND sv2op(index)) OR (borrow AND NOT(intern));
    END LOOP;
    sv_result := result;
  END sv_subu;

  ----------------------------------------------------------------
  -- sv_neg
  -- Signed negation with overflow detection
  ----------------------------------------------------------------
  PROCEDURE sv_neg (sv : IN std_logic_vector;
                    sv_result : OUT std_logic_vector;
                    overflow : OUT std_logic) IS

    CONSTANT zero : std_logic_vector(sv'RANGE) := (OTHERS => '0');

  BEGIN
    sv_sub( zero, sv, sv_result, overflow );
  END sv_neg;

  ----------------------------------------------------------------
  -- sv_neg
  -- Signed negation without overflow detection
  ----------------------------------------------------------------
  PROCEDURE sv_neg (sv : IN std_logic_vector;
                    sv_result : OUT std_logic_vector) IS

    CONSTANT zero : std_logic_vector(sv'RANGE) := (OTHERS => '0');

  BEGIN
    sv_sub( zero, sv, sv_result);
  END sv_neg;

--  ----------------------------------------------------------------
--  -- sv_mult
--  --
--  -- Signed multiplication with overflow detection
--  ----------------------------------------------------------------
--  PROCEDURE sv_mult (sv1, sv2  : IN std_logic_vector;
--      	       	     sv_result : OUT std_logic_vector;
--		     overflow  : OUT std_logic) IS
--
--    VARIABLE    negative_result  : std_logic;
--    VARIABLE    op1              : std_logic_vector(sv1'RANGE) := sv1;
--    VARIABLE    op2              : std_logic_vector(sv2'RANGE) := sv2;
--    VARIABLE    multu_result     : std_logic_vector(sv1'RANGE);
--    VARIABLE    multu_overflow   : std_logic;
--    constant    abs_min_int      : std_logic_vector(sv1'range)
--                                    := (sv1'left => '1', others => '0');
--
--
--  BEGIN
--    ASSERT sv1'length = sv2'length AND sv1'length = sv_result'length
--      REPORT "sv_mult: operands of different lengths"
--      SEVERITY FAILURE;
--
--    negative_result := (op1(op1'left) = '1') XOR (op2(op2'left) = '1');
--    IF (op1(op1'left) = '1') THEN
--      op1 := - sv1;
--    END IF;
--    IF (op2(op2'left) = '1') THEN
--      op2 := - sv2;
--    END IF;
--    sv_multu(op1, op2, multu_result, multu_overflow);
--    IF (negative_result) THEN
--      overflow := multu_overflow OR (multu_result > ABS_min_int);
--      sv_result := - multu_result;
--    ELSE
--      overflow := multu_overflow OR (multu_result(multu_result'left) = '1');
--      sv_result := multu_result;
--    END IF;
--  END sv_mult;
--
--  ----------------------------------------------------------------
--  -- sv_mult
--  --
--  -- Signed multiplication without overflow detection
--  ----------------------------------------------------------------
--  procedure sv_mult (sv1, sv2  : in  std_logic_vector;
--                     sv_result : out std_logic_vector) is
--
--    VARIABLE    negative_result  : std_logic;
--    VARIABLE    op1              : std_logic_vector(sv1'RANGE) := sv1;
--    VARIABLE    op2              : std_logic_vector(sv2'RANGE) := sv2;
--    VARIABLE    result           : std_logic_vector(sv1'RANGE);
--    
--  BEGIN
--    ASSERT sv1'length = sv2'length
--      REPORT "sv_mult : operands of different lengths"
--      SEVERITY FAILURE;
--
--    negative_result := (op1(op1'left) = '1') XOR (op2(op2'left) = '1');
--    IF (op1(op1'left) = '1') THEN
--      op1 := - sv1;
--    END IF;
--    IF (op2(op2'left) = '1') THEN
--      op2 := - sv2;
--    END IF;
--    sv_multu(op1, op2, result);
--    IF (negative_result) THEN
--      result := - result;
--    END IF;
--    sv_result := result;
--  END sv_mult;
--
--
--  ---------------------------------------------------------------------------
--  -- sv_multu
--  --
--  -- Unsigned multiplication with overflow detection
--  -- Based on shift&add multiplier in Appendix A of Hennessy & Patterson
--  ---------------------------------------------------------------------------
--  PROCEDURE sv_multu (sv1, sv2 : IN std_logic_vector;
--      	       	      sv_result : OUT std_logic_vector;
--		      overflow : OUT std_logic) IS
--
--
--    CONSTANT    sv_length        : natural := sv1'length;
--    CONSTANT    accum_length     : natural := sv_length * 2;
--    CONSTANT    zero             : std_logic_vector(accum_length-1 DOWNTO sv_length)
--                                    := (OTHERS => '0');
--    VARIABLE    accum            : std_logic_vector(accum_length-1 DOWNTO 0);
--    VARIABLE    addu_overflow    : std_logic;
--    VARIABLE    carry            : bit;
--
--  BEGIN
--    ASSERT sv1'length = sv2'length AND sv1'length = sv_result'length
--      REPORT "sv_multu: operands of different lengths"
--      SEVERITY FAILURE;
--      
--    accum(sv_length-1 DOWNTO 0) := sv1;
--    accum(accum_length-1 DOWNTO sv_length) := zero;
--    FOR count IN 1 TO sv_length LOOP
--      IF (accum(0) = '1') THEN
--        sv_addu( accum(accum_length-1 DOWNTO sv_length), sv2,
--                 accum(accum_length-1 DOWNTO sv_length), addu_overflow);
--        carry := std_logic'val(std_logic'pos(addu_overflow)- 2);
--      ELSE
--        carry := '0';
--      END IF;
--      accum := carry & accum(accum_length-1 DOWNTO 1);
--    END LOOP;
--    sv_result := accum(sv_length-1 DOWNTO 0);
--    overflow := accum(accum_length-1 DOWNTO sv_length) /= zero;
--  END sv_multu;
--
--  ----------------------------------------------------------------
--  --  sv_multu
--  --
--  -- Unsigned multiplication without overflow detection
--  -- Use sv_multu with overflow detection, but ignore overflow flag
--  ----------------------------------------------------------------
--  PROCEDURE sv_multu (sv1, sv2 : IN std_logic_vector;
--      	       	      sv_result : OUT std_logic_vector) IS
--
--    VARIABLE    tmp_overflow : std_logic;
--
--  BEGIN
--    -- following procedure asserts sv1'length = sv2'length
--    sv_multu(sv1, sv2, sv_result, tmp_overflow);
--  END sv_multu;
--
--  ----------------------------------------------------------------
--  -- sv_div
--  --
--  -- Signed division with divide by zero and overflow detection
--  ----------------------------------------------------------------
--  procedure sv_div (sv1, sv2 : in std_logic_vector;
--      	       	    sv_result : out std_logic_vector;
--		    div_by_zero : out std_logic;
--                    overflow : out std_logic) is
--
--    --  Need overflow, in case divide b"10...0" (min_int) by -1
--    --  Don't use sv_to_int, in case size bigger than host machine!
--
--    variable    negative_result  : std_logic;
--    variable    op1              : std_logic_vector(sv1'range) := sv1;
--    variable    op2              : std_logic_vector(sv2'range) := sv2;
--    variable    divu_result      : std_logic_vector(sv1'range);
--
--  begin
--    assert sv1'length = sv2'length
--      report "sv_div: operands of different lengths"
--      severity failure;
--    negative_result := (op1(op1'left) = '1') xor (op2(op2'left) = '1');
--    if (op1(op1'left) = '1') then
--      op1 := - sv1;
--    end if;
--    if (op2(op2'left) = '1') then
--      op2 := - sv2;
--    end if;
--    sv_divu(op1, op2, divu_result, div_by_zero);
--    if (negative_result) then
--      overflow := false;
--      sv_result := - divu_result;
--    else
--      overflow := divu_result(divu_result'left) = '1';
--      sv_result := divu_result;
--    end if;
--  end sv_div;
--
--  ----------------------------------------------------------------
--  -- sv_div
--  --
--  -- Signed division without divide by zero and overflow detection
--  ----------------------------------------------------------------
--  procedure sv_div (sv1, sv2  : in  std_logic_vector;
--                    sv_result : out std_logic_vector) is
--
--    variable    negative_result  : std_logic;
--    variable    op1              : std_logic_vector(sv1'range) := sv1;
--    variable    op2              : std_logic_vector(sv2'range) := sv2;
--    variable    result           : std_logic_vector(sv1'range);
--
--  begin
--    assert sv1'length = sv2'length
--      report "sv_div: operands of different lengths"
--      severity failure;
--
--    negative_result := (op1(op1'left) = '1') xor (op2(op2'left) = '1');
--    if (op1(op1'left) = '1') then
--      op1 := - sv1;
--    end if;
--    if (op2(op2'left) = '1') then
--      op2 := - sv2;
--    end if;
--    sv_divu(op1, op2, result);
--    if (negative_result) then
--      result := - result;
--    end if;
--    return result;
--  end "/";
--
--  ----------------------------------------------------------------
--  -- sv_divu
--  --
--  -- Unsigned division with divide by zero detection
--  --  based on algorithm in Sun Sparc architecture manual
--  ----------------------------------------------------------------
--  procedure sv_divu (sv1, sv2 : in std_logic_vector;
--      	       	     sv_result : out std_logic_vector;
--		     div_by_zero : out std_logic) is
--
--    constant    len              : natural := sv1'length;
--    variable    zero, one,
--                big_value        : std_logic_vector(len-1 downto 0)
--				     := (others => '0');
--    variable    dividend         : std_logic_vector(sv1'length-1 downto 0) := sv1;
--    variable    divisor          : std_logic_vector(sv2'length-1 downto 0) := sv2;
--    variable    quotient         : std_logic_vector(len-1 downto 0);  --  unsigned
--    variable    remainder        : std_logic_vector(len-1 downto 0);  --  signed
--    variable    shifted_divisor,
--                shifted_1        : std_logic_vector(len-1 downto 0);
--    variable    log_quotient     : natural;
--    variable    ignore_overflow  : std_logic;
--
--  begin
--    assert sv1'length = sv2'length
--      report "sv_divu: operands of different lengths"
--      severity failure;
--    one(0) := '1';
--    big_value(len-2) := '1';
--    --  
--    --  check for zero divisor
--    --  
--    if (divisor = zero) then
--      div_by_zero := true;
--      return;
--    end if;
--    --  
--    --  estimate log of quotient
--    --
--    log_quotient := 0;
--    shifted_divisor := divisor;
--    loop 
--      exit when (log_quotient >= len)
--                or (shifted_divisor > big_value)
--                or (shifted_divisor >= dividend);
--      log_quotient := log_quotient + 1;
--      shifted_divisor := sv_sll(shifted_divisor, 1);
--    end loop;
--    --
--    --  perform division
--    --
--    remainder := dividend;
--    quotient := zero;
--    shifted_divisor := sv_sll(divisor, log_quotient);
--    shifted_1 := sv_sll(one, log_quotient);
--    for iter in log_quotient downto 0 loop
--      if sv_ge(remainder, zero) then
--        sv_sub(remainder, shifted_divisor, remainder, ignore_overflow);
--        sv_addu(quotient, shifted_1, quotient, ignore_overflow);
--      else
--        sv_add(remainder, shifted_divisor, remainder, ignore_overflow);
--        sv_subu(quotient, shifted_1, quotient, ignore_overflow);
--      end if;
--      shifted_divisor := '0' & shifted_divisor(len-1 downto 1);
--      shifted_1 := '0' & shifted_1(len-1 downto 1);
--    end loop;
--    if (sv_lt(remainder, zero)) then
--      sv_add(remainder, divisor, remainder, ignore_overflow);
--      sv_subu(quotient, one, quotient, ignore_overflow);
--    end if;
--    sv_result := quotient;
--  end sv_divu;
--
--  ----------------------------------------------------------------
--  -- sv_divu
--  --
--  -- Unsigned division without divide by zero detection
--  ----------------------------------------------------------------
--  procedure sv_divu (sv1, sv2 : in std_logic_vector;
--      	       	     sv_result : out std_logic_vector) is
--
--    -- Use sv_divu with divide by zero detection,
--    -- but ignore div_by_zero flag
--
--    variable tmp_div_by_zero : std_logic;
--
--  begin
--    -- following procedure asserts sv1'length = sv2'length
--    sv_divu(sv1, sv2, sv_result, tmp_div_by_zero);
--  end sv_divu;
  
  ----------------------------------------------------------------
  --  ***** Logical operators *****
  ----------------------------------------------------------------
  ----------------------------------------------------------------
  -- sv_sll
  -- Shift left logical (fill with '0' bits)
  ----------------------------------------------------------------
  PROCEDURE sv_sll (sv : IN std_logic_vector;
                    sv_result: OUT std_logic_vector;
       	      	    shift_count : IN std_logic_vector) IS
 	
	constant control_msb: INTEGER := shift_count'length - 1;
	variable control: std_logic_vector (control_msb downto 0);
	constant result_msb: INTEGER := sv'length-1;
	subtype rtype is std_logic_vector (result_msb downto 0);
	variable result, temp: rtype;
    begin
	control := shift_count;
	-- synopsys synthesis_off
	if (control(0) = 'X') then
	    result := rtype'(others => 'X');
	    sv_result := result;
	end if;
	-- synopsys synthesis_on
	result := sv;
	for i in 0 to control_msb loop
	    if control(i) = '1' then
		temp := rtype'(others => '0');
		if 2**i <= result_msb then
		    temp(result_msb downto 2**i) := 
				    result(result_msb - 2**i downto 0);
		end if;
		result := temp;
	    end if;
	end loop;
	sv_result := result;
     END sv_sll;

  ----------------------------------------------------------------
  -- sv_srl
  -- Shift right logical (fill with '0' bits)
  ----------------------------------------------------------------
  PROCEDURE sv_srl (sv : IN std_logic_vector;
                    sv_result: OUT std_logic_vector;
      	      	    shift_count : IN std_logic_vector) IS

 	constant control_msb: INTEGER := shift_count'length - 1;
	variable control: std_logic_vector(control_msb downto 0);
	constant result_msb: INTEGER := sv'length-1;
	subtype rtype is std_logic_vector (result_msb downto 0);
	variable result, temp: rtype;
    begin
	control := shift_count;
	-- synopsys synthesis_off
	if (control(0) = 'X') then
	    result := rtype'(others => 'X');
	    sv_result := result;
	end if;
	-- synopsys synthesis_on
	result := sv;
	for i in 0 to control_msb loop
	    if control(i) = '1' then
		temp := rtype'(others => '0');
		if 2**i <= result_msb then
		    temp(result_msb - 2**i downto 0) := 
					result(result_msb downto 2**i);
		end if;
		result := temp;
	    end if;
	end loop;
	sv_result := result;
   END sv_srl;

  ----------------------------------------------------------------
  -- sv_sra
  -- Shift right arithmetic (fill with copy of sign bit)
  ----------------------------------------------------------------
  PROCEDURE sv_sra (sv : IN std_logic_vector;
                    sv_result: OUT std_logic_vector;
              	    shift_count : IN std_logic_vector) IS

    constant control_msb: INTEGER := shift_count'length - 1; 
    VARIABLE control : std_logic_vector(control_msb downto 0);
    constant result_msb: INTEGER := sv'length-1;    
    subtype rtype is std_logic_vector(result_msb downto 0);
    VARIABLE result, temp : rtype;
    variable sign_bit : std_ulogic;

    begin
	control := shift_count;
	-- synopsys synthesis_off
	if (control(0) = 'X') then
	    result := rtype'(others => 'X');
	    sv_result := result;
	end if;
	-- synopsys synthesis_on
	result := sv;
	sign_bit := sv(sv'left);
	for i in 0 to control_msb loop
	    if control(i) = '1' then
		temp := rtype'(others => sign_bit);
		if 2**i <= result_msb then
		    temp(result_msb - 2**i downto 0) := 
					result(result_msb downto 2**i);
		end if;
		result := temp;
	    end if;
	end loop;
	sv_result := result;
    end;

  ----------------------------------------------------------------
  -- sv_rol
  -- Rotate left
  ----------------------------------------------------------------
  PROCEDURE sv_rol (sv : IN std_logic_vector;
                    sv_result: OUT std_logic_vector;
       	            rotate_count : IN natural) IS

    CONSTANT sv_length : natural := sv'length;
    CONSTANT actual_rotate_count : natural := rotate_count MOD sv_length;
    VARIABLE sv_norm : std_logic_vector(1 TO sv_length);
    VARIABLE result : std_logic_vector(1 TO sv_length);

  BEGIN
    --synopsys synthesis_off
    ASSERT sv'length = sv_result'length
      REPORT "sv_rol: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv) = FALSE
      REPORT "sv_rol: operand contains unknown value !"
      SEVERITY WARNING; 
    
    --synopsys synthesis_on
    sv_norm := sv;
    result(1 TO sv_length - actual_rotate_count)
      := sv_norm(actual_rotate_count + 1 TO sv_length);
    result(sv_length - actual_rotate_count + 1 TO sv_length)
      := sv_norm(1 TO actual_rotate_count);
    sv_result := result;
  END sv_rol;

  ----------------------------------------------------------------
  -- sv_ror
  -- Rotate right
  ----------------------------------------------------------------
  PROCEDURE sv_ror (sv : IN std_logic_vector;
                    sv_result: OUT std_logic_vector;
                    rotate_count : IN natural) IS
    
    CONSTANT sv_length : natural := sv'length;
    CONSTANT actual_rotate_count : natural := rotate_count MOD sv_length;
    VARIABLE sv_norm : std_logic_vector(1 TO sv_length);
    VARIABLE result : std_logic_vector(1 TO sv_length);

  BEGIN
    --synopsys synthesis_off
    ASSERT sv'length = sv_result'length
      REPORT "sv_ror: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv) = FALSE
      REPORT "sv_ror: operand contains unknown value !"
      SEVERITY WARNING; 
    
    --synopsys synthesis_on
    sv_norm := sv;
    result(actual_rotate_count + 1 TO sv_length)
      := sv_norm(1 TO sv_length - actual_rotate_count);
    result(1 TO actual_rotate_count)
      := sv_norm(sv_length - actual_rotate_count + 1 TO sv_length);
    sv_result := result;
  END sv_ror;

  ----------------------------------------------------------------
  --  ***** Arithmetic comparison operators. *****
  --  Perform comparisons on bit vector encoded signed integers.
  ----------------------------------------------------------------

  ----------------------------------------------------------------
  -- sv_lt
  -- Signed less than comparison
  ----------------------------------------------------------------
  PROCEDURE sv_lt (sv1, sv2 : IN std_logic_vector;
                   sv_result : OUT std_logic) IS

    VARIABLE tmp1 : std_logic_vector(sv1'RANGE);
    VARIABLE tmp2 : std_logic_vector(sv2'RANGE);

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length
      REPORT "sv_lt: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_lt: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_lt: second operand contains unknown value !"
      SEVERITY WARNING;

    --synopsys synthesis_on
    tmp1 := sv1;
    tmp2 := sv2;
    tmp1(tmp1'left) := NOT tmp1(tmp1'left);
    tmp2(tmp2'left) := NOT tmp2(tmp2'left);
    IF (tmp1 < tmp2) THEN sv_result := '1'; ELSE sv_result := '0'; END IF;
  END sv_lt;

  ----------------------------------------------------------------
  -- sv_ltu
  -- Unsigned less than comparison
  ----------------------------------------------------------------
  PROCEDURE sv_ltu (sv1, sv2 : IN std_logic_vector;
                    sv_result : OUT std_logic) IS

    VARIABLE tmp1 : std_logic_vector(sv1'RANGE);
    VARIABLE tmp2 : std_logic_vector(sv2'RANGE);

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length
      REPORT "sv_ltu: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_ltu: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_ltu: second operand contains unknown value !"
      SEVERITY WARNING;

    --synopsys synthesis_on
    tmp1 := sv1;
    tmp2 := sv2;
    IF (tmp1 < tmp2) THEN sv_result := '1'; ELSE sv_result := '0'; END IF;
  END sv_ltu;
    
  ----------------------------------------------------------------
  -- sv_le
  -- Signed less than or equal comparison
  ----------------------------------------------------------------
  PROCEDURE sv_le (sv1, sv2 : IN std_logic_vector;
                   sv_result : OUT std_logic) IS

    VARIABLE tmp1 : std_logic_vector(sv1'RANGE);
    VARIABLE tmp2 : std_logic_vector(sv2'RANGE);

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length
      REPORT "sv_le: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_le: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_le: second operand contains unknown value !"
      SEVERITY WARNING;

    --synopsys synthesis_on
    tmp1 := sv1;
    tmp2 := sv2;
    tmp1(tmp1'left) := NOT tmp1(tmp1'left);
    tmp2(tmp2'left) := NOT tmp2(tmp2'left);
    IF (tmp1 <= tmp2) THEN sv_result := '1'; ELSE sv_result := '0'; END IF;
  END sv_le;

  ----------------------------------------------------------------
  -- sv_leu
  -- Unsigned less than or equal comparison
  ----------------------------------------------------------------
  PROCEDURE sv_leu (sv1, sv2 : IN std_logic_vector;
                    sv_result : OUT std_logic) IS

    VARIABLE tmp1 : std_logic_vector(sv1'RANGE);
    VARIABLE tmp2 : std_logic_vector(sv2'RANGE);

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length
      REPORT "sv_leu operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_leu first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_leu second operand contains unknown value !"
      SEVERITY WARNING;

    --synopsys synthesis_on
    tmp1 := sv1;
    tmp2 := sv2;
    IF (tmp1 <= tmp2) THEN sv_result := '1'; ELSE sv_result := '0'; END IF;
  END sv_leu;

  ----------------------------------------------------------------
  -- sv_gt
  -- Signed greater than comparison
  ----------------------------------------------------------------
  PROCEDURE sv_gt (sv1, sv2 : IN std_logic_vector;
                   sv_result : OUT std_logic) IS

    VARIABLE tmp1 : std_logic_vector(sv1'RANGE);
    VARIABLE tmp2 : std_logic_vector(sv2'RANGE);

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length
      REPORT "sv_gt: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_gt: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_gt: second operand contains unknown value !"
      SEVERITY WARNING;

    --synopsys synthesis_on
    tmp1 := sv1;
    tmp2 := sv2;
    tmp1(tmp1'left) := NOT tmp1(tmp1'left);
    tmp2(tmp2'left) := NOT tmp2(tmp2'left);
    IF (tmp1 > tmp2) THEN sv_result := '1'; ELSE sv_result := '0'; END IF;
  END sv_gt;

  ----------------------------------------------------------------
  -- sv_gtu
  -- Unsigned greater than comparison
  ----------------------------------------------------------------
  PROCEDURE sv_gtu (sv1, sv2 : IN std_logic_vector;
                    sv_result : OUT std_logic) IS

    VARIABLE tmp1 : std_logic_vector(sv1'RANGE);
    VARIABLE tmp2 : std_logic_vector(sv2'RANGE);

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length
      REPORT "sv_gtu: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_gtu: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_gtu: second operand contains unknown value !"
      SEVERITY WARNING;

    --synopsys synthesis_on
    tmp1 := sv1;
    tmp2 := sv2;
    IF (tmp1 > tmp2) THEN sv_result := '1'; ELSE sv_result := '0'; END IF;
  END sv_gtu;
  
  ----------------------------------------------------------------
  -- sv_ge
  -- Signed greater than or equal comparison
  ----------------------------------------------------------------
  PROCEDURE sv_ge (sv1, sv2 : IN std_logic_vector;
                   sv_result : OUT std_logic) IS

    VARIABLE tmp1 : std_logic_vector(sv1'RANGE);
    VARIABLE tmp2 : std_logic_vector(sv2'RANGE);

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length
      REPORT "sv_ged: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_ge: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_ge: second operand contains unknown value !"
      SEVERITY WARNING;

    --synopsys synthesis_on
    tmp1 := sv1;
    tmp2 := sv2;
    tmp1(tmp1'left) := NOT tmp1(tmp1'left);
    tmp2(tmp2'left) := NOT tmp2(tmp2'left);
    IF (tmp1 >= tmp2) THEN sv_result := '1'; ELSE sv_result := '0'; END IF;
  END sv_ge;

  ----------------------------------------------------------------
  -- sv_geu
  -- Unsigned greater or equeal comparison
  ----------------------------------------------------------------
  PROCEDURE sv_geu (sv1, sv2 : IN std_logic_vector;
                    sv_result : OUT std_logic) IS

    VARIABLE tmp1 : std_logic_vector(sv1'RANGE);
    VARIABLE tmp2 : std_logic_vector(sv2'RANGE);

  BEGIN
    --synopsys synthesis_off
    ASSERT sv1'length = sv2'length
      REPORT "sv_geu: operands of different lengths"
      SEVERITY FAILURE;
    ASSERT IS_X(sv1) = FALSE
      REPORT "sv_geu: first operand contains unknown value !"
      SEVERITY WARNING; 
    ASSERT IS_X(sv2) = FALSE
      REPORT "sv_geu: second operand contains unknown value !"
      SEVERITY WARNING;

    --synopsys synthesis_on
    tmp1 := sv1;
    tmp2 := sv2;
    IF (tmp1 >= tmp2) THEN sv_result := '1'; ELSE sv_result := '0'; END IF;
  END sv_geu;
  
  ----------------------------------------------------------------
  --  ***** Extension operators *****
  ----------------------------------------------------------------

  ----------------------------------------------------------------
  -- sv_sext
  -- Sign extension - replicate the sign bit of the operand into
  -- the most significant bits of the result.  Length parameter
  -- determines size of result.  If length < sv'length, result is
  -- rightmost length bits of sv.
  ----------------------------------------------------------------
  FUNCTION sv_sext (sv     : IN std_logic_vector;
      	      	    length : IN natural) RETURN std_logic_vector IS

    VARIABLE sv_norm : std_logic_vector(1 TO sv'length);
    VARIABLE result : std_logic_vector(1 TO length);
    VARIABLE src_length : natural;

  BEGIN
    --synopsys synthesis_off
    ASSERT IS_X(sv) = FALSE
      REPORT "sv_sext: operand contains unknown value !"
      SEVERITY WARNING; 

    --synopsys synthesis_on
    sv_norm := sv;
    result := (OTHERS => sv(sv'left));
    src_length := sv'length;
    IF src_length > length THEN
      src_length := length;
    END IF;
    result(length - src_length + 1 TO length)
      := sv_norm(sv'length - src_length + 1 TO sv'length);
    RETURN result;
  END sv_sext;

  ----------------------------------------------------------------
  -- sv_zext
  -- Zero extension - replicate zero bits into the most significant
  -- bits of the result.  Length parameter determines size of result.
  -- If length < sv'length, result is rightmost length bits of sv.
  ----------------------------------------------------------------
  FUNCTION sv_zext (sv     : IN std_logic_vector;
      	      	    length : IN natural) RETURN std_logic_vector IS

    VARIABLE sv_norm : std_logic_vector(1 TO sv'length);
    VARIABLE result : std_logic_vector(1 TO length);
    VARIABLE src_length : natural;

  BEGIN
    --synopsys synthesis_off
    ASSERT IS_X(sv) = FALSE
      REPORT "sv_zext: operand contains unknown value !"
      SEVERITY WARNING;

    --synopsys synthesis_on
    sv_norm := sv;
    result := (OTHERS => '0');
    src_length := sv'length;
    IF src_length > length THEN
      src_length := length;
    END IF;
    result(length - src_length + 1 TO length)
      := sv_norm(sv'length - src_length + 1 TO sv'length);
    RETURN result;
  END sv_zext;

  ----------------------------------------------------------------
  -- sv_expand
  -- expand std_logic value to std_logic_vector of length
  ----------------------------------------------------------------
  FUNCTION sv_expand (sv     : IN std_ulogic;
                      length : IN natural) RETURN std_logic_vector IS

    VARIABLE result : std_logic_vector(1 TO length);

  BEGIN
    --synopsys synthesis_off
    ASSERT length /= 0
      REPORT "sv_expand: vector length must must be greater than zero !"
      SEVERITY WARNING;
    --synopsys synthesis_on

    FOR i IN 1 TO length LOOP
      result(i) := sv;
    END LOOP;
    RETURN result;
  END sv_expand;


END stdl1164_vector_arithmetic;

